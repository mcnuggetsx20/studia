#include "Table.h"
#include <stdio.h>
#include <stdlib.h>

int CTABLE_MAX_SIZE(){return 100;}

void v_allocate_table(CTable* c_table){
    v_allocate_table_from_length(c_table, c_table ->i_length);
    return;
}

void v_allocate_table_from_length(CTable* c_table, uint8_t i_size){
    if(c_table -> pi_table!= NULL && i_size == c_table->i_length) return;
    v_ctable_destroy(c_table);
    c_table -> pi_table = (uint8_t*) malloc(sizeof(uint8_t) * i_size);
    c_table -> i_length = i_size;
    return;
}

void v_ctable_fill(CTable* c_table, uint8_t i_value){
    if(c_table -> pi_table == NULL) v_allocate_table(c_table);

    for(int i = 0; i < c_table -> i_length; ++i){
        c_table->pi_table[i] = i_value;
    }

    return;
}

CTable c_ctable_from(uint8_t* i_array, int i_size){
    CTable c_res = {
        .s_name = "copy",
    };
    v_allocate_table_from_length(&c_res, i_size);

    for(int i = 0 ; i < i_size; ++i){
        c_res.pi_table[i] = i_array[i];
    }

    return c_res;
}

void v_ctable_destroy(CTable* c_table){
    if(c_table -> pi_table != NULL)
        free(c_table->pi_table);
    return;
}

bool b_set_new_size(CTable* cp_table, uint8_t i_new_size){
    if(i_new_size > CTABLE_MAX_SIZE()) return false;
    v_allocate_table_from_length(cp_table, i_new_size);
    cp_table -> s_name = "resized";
    
    return true;
}

void v_print_table(CTable* c_table){
    printf("[%d", c_table->pi_table[0]);
    for(int i = 1 ; i < c_table->i_length; ++i){
        printf(", %d", c_table->pi_table[i]);
    }
    printf("]\n");
    return;
}
