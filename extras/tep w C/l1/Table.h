#ifndef TABLE_H
#define TABLE_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint8_t* pi_table;
    int i_length;
    char* s_name;

} CTable;

void v_ctable_fill(CTable* , uint8_t size);
CTable c_ctable_from(uint8_t* i_array, int i_size);
void v_ctable_destroy(CTable*);
bool b_set_new_size(CTable*, uint8_t size);

void v_allocate_table_from_length(CTable*, uint8_t size);
void v_allocate_table(CTable*);

void v_print_table(CTable*);

int CTABLE_MAX_SIZE();

#endif
