#include "Table.h"
#include <stdio.h>

int main(void){
    CTable table = {
        .i_length = 10,
        .s_name = "main",
    };
    v_ctable_fill(&table, 5);

    CTable temp = c_ctable_from((uint8_t[]){1,2,3}, 3);
    v_print_table(&temp);

    printf("ale sie nam udalo : %d\n", *table.pi_table);
    v_ctable_destroy(&table);
    return 0;
}
