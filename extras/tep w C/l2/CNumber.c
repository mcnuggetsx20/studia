#include "CNumber.h"
#include <stdlib.h>

CNumber cnumber_factory(int* p_table, int table_size, int base, bool sign){
    CNumber res ={
        .p_table = p_table,
        .table_size = table_size,
        .base = base,
        .sign = sign,
    };

    return res;
}

CNumber cnumber_from_int(int value, int base){
    int temp = value;
    int size = 0;
    while(temp){
        ++size;
        temp /= base;
    }

    CNumber res = {
        .p_table = (int*) malloc(sizeof(int) * size),
        .table_size = size,
        .base = base,
        .sign = (value >= 0) ? true : false,
    };

    for(int i= res.table_size-1; i >= 0; --i){
        res.p_table[i] = value%base;
        value /=base;
    }

    return res;
}

void cnumber_resize(CNumber* cnumber, int new_size){
    cnumber_destroy(cnumber);
    cnumber->p_table = (int*) malloc(sizeof(int) * new_size);
    cnumber->table_size = new_size;
    return;
}

void cnumber_destroy(CNumber* cnumber){
    if(cnumber->p_table != NULL)
        free(cnumber->p_table);
    return;
}
