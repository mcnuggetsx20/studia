#ifndef CNUMBER_H
#define CNUMBER_H

#include <stdbool.h>

typedef struct{
    int* p_table;
    int table_size;
    int base;
    bool sign;
} CNumber;

CNumber cnumber_factory(int*, int size, int base, bool sign);
CNumber cnumber_from_int(int value, int base);
void cnumber_resize(CNumber*, int size);

CNumber cadd(CNumber*, CNumber*);
CNumber csub(CNumber*, CNumber*);
CNumber cmul(CNumber*, CNumber*);
CNumber cdiv(CNumber*, CNumber*);

void cnumber_destroy(CNumber*);

#endif
