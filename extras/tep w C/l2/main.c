#include "CNumber.h"
#include <stdio.h>

int main(void){
    CNumber number = cnumber_from_int(12, 10);

    for(int i = 0; i < number.table_size; ++i){
        printf("%d, ", number.p_table[i]);
    }
    printf("\n");
}
