let rec accumulate (tab : float list) : float =
    if List.is_empty tab then 0.0
    else
        List.hd tab +. accumulate (List.tl tab)
