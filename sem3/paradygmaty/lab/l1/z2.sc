object main {
  def genlist(a: Int, b: Int): List[Int] ={
    if(a > b) List()
    else{
      a :: genlist(a+1, b)
    }
  }

  def main(args: Array[String]): Unit = {
    val result = genlist(4, 8)
    println(result)
  }
}
