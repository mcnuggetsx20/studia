let rec smaller (tab : int list) n : bool = 
    if List.is_empty tab then true
    else if List.hd tab >= n then false
    else 
        smaller (List.tl tab) n
