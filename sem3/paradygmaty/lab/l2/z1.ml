let rec merge tab sec =
    match tab, sec with
    | [], [] -> []
    | [], l2 -> l2
    | l1, [] -> l1
    | x1 :: xs1, x2::xs2 -> x1 :: x2 :: merge xs1 xs2

