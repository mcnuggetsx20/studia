object main {
  def size(tab : List[Any]) : Int = {
    tab match {
     case Nil => 0
     case _ => 1 + size(tab.tail)
    }
  }
}
