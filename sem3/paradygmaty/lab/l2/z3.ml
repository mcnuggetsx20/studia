let rec split tab =
    match tab with
    | [] -> ([], [])
    | x :: xs ->
            let (even, odd) = split xs in
            match (x mod 2) with
            | 0 -> (x:: even, odd)
            | _ -> (even, x :: odd)
            
