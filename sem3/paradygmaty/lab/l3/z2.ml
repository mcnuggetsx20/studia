let applyToRange foo a b= 
    let rec tailRec foo a b acc = 
        match a>b with
        | true -> acc
        (* | false -> foo a + applyToRange foo (a+1) b *)
        | false -> tailRec foo (a+1) b (acc+(foo a))
    in
    tailRec foo a b 0

let neutral n = n
let cube n = n*n*n

let fact n =
    let rec tailRec n acc =
        match n with
        | 0 -> acc
        | 1 -> acc
        | _ -> tailRec (n-1) (acc*n)

    in
    tailRec n 1

