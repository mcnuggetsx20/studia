object z3{
  def applyToRange(function : Int => Int, a : Int, b : Int) : Int = {

    @scala.annotation.tailrec
    def tailRec(function : Int => Int, a : Int, b : Int, acc : Int) : Int = {
      a <= b match{
        case false => acc
        case _ => tailRec(function, a+1, b, acc + function(a))
      }
    }
    tailRec(function, a, b, 0)
  }

  val neutral = (n: Int) => n
  val cube = (n: Int) => n*n*n

  val fact: Int => Int = (n : Int) => {

    @scala.annotation.tailrec
    def tailRec(n : Int, acc: Int) : Int ={
      n match{
        case 1 => 1
        case 0 => 1
        case _ => fact(n-1, n*acc)
      }
    }

    tailRec(n, 1)
  }
}
