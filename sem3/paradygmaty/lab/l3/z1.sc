object z1 {
  def transformList(tab : List[Float], function : Float => Float) : List[Float] = {

    @scala.annotation.tailrec
    def tailRec(tab : List[Float], function : Float => Float, acc : List[Float]) : List[Float] = {
      tab match{
        case Nil => acc
        case _ =>
          val res = function(tab.head)
          // res :: transformList(tab.tail, function)
          tailRec(tab.tail, function, acc ::: List(res))
      }
    }
    tailRec(tab, function, List())
  }

  def square(n : Float) : Float = n * n
  def exp(n : Float) : Float = scala.math.pow(scala.math.E, n).toFloat - 1

  def abs(n : Float): Float = {
    n < 0 match{
      case true => n*(-1)
      case false => n
    }
  }
}
