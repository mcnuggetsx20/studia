object z2 {
  sealed trait BT[A]
  case class Empty[A]() extends BT[A]
  case class Node[A] (value: A, left: BT[A], right: BT[A]) extends BT[A]

  def sum_tree[A: Numeric](tt : BT[A]) : A ={
    val numeric = implicitly[Numeric[A]]

    @scala.annotation.tailrec
    def tailRec(tt: List[BT[A]], acc: A) : A ={
      tt match{
        case Nil => acc
        case Empty[A]() :: tree_tail => tailRec(tree_tail, acc)
        case Node(value, left, right) :: tree_tail => tailRec(left :: right :: tree_tail, numeric.plus(acc, value))
      }
    }

    tailRec(List(tt), numeric.zero)
  }
}
