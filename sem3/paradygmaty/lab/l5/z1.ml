type eval = Num of float | Add | Sub | Mul | Div;;

exception InvalidExpression;;

let eval instr =

    let rec tailRec instr stack= 
        match(instr, stack) with
        | ([], [result]) -> result
        | (Num n :: instr_tail, _) -> tailRec instr_tail (n :: stack)
        | (operation :: instr_tail, a :: b :: stack_tail) ->
                let result = match operation with
                | Add -> a +. b
                | Sub -> a -. b
                | Mul -> a *. b
                | Div -> a /. b
                | _ -> raise InvalidExpression
                in
                tailRec instr_tail (result :: stack_tail)
        | (_, _) -> raise InvalidExpression
    in
    tailRec instr []
