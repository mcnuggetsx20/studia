module Quadratic = struct
    let delta a b c =(b *. b) -. (4.*.a*.c) 

    let solution a b c op= 
        let d = delta a b c in
        (( op (0. -. b) (sqrt d)) /. (2.*.a))

    let solve a b c = 
        match (compare (delta a b c) 0.0) with
        | -1 -> Printf.printf "Rownanie nie ma rozwiazan\n"
        | 1 -> Printf.printf "Rozwiazania: %f, %f\n" (solution a b c (+.)) (solution a b c (-.))
        | 0 -> Printf.printf "Rozwiazanie: %f\n" (solution a b c (+.))
        | _ -> Printf.printf "default"

end

module ListOperations = struct
    let empty = []
    let is_empty = function
        | [] -> true
        | _ -> false

    let length tab = 
        let rec tailrec tab acc =
            match tab with
            | [] -> acc
            | head::tail -> tailrec tail (acc+1)
        in
        tailrec tab 0

    let sum tab = 
        let rec tailrec tab acc = 
            match tab with 
            | [] -> acc
            | head :: tail -> tailrec tail (acc+head)
        in
        tailrec tab 0

    let rec filter_even = function
        | [] -> []
        | head :: tail ->
                match (head mod 2 = 0) with 
                | true -> head :: filter_even tail
                | false -> filter_even tail

    let rec split_into_pairs = function
        | [] -> ([], [])
        | head :: tail ->
                let (even, odd) = split_into_pairs tail in
                match (head mod 2 = 0) with
                | true -> (head :: even, odd)
                | false -> (even, head :: odd)
end

module TupleOperations = struct
    let element_count (a, b, c) = b
    let sum_element (a,b,c) = a+b+c
    let is_sorted (a,b,c) = (a <= b) && (b <= c)
    let min_max (a,b,c) = (min a (min b c), max a (max b c))
    let combine (a,b,c) (d,e,f) = ((a,d), (b,e), (c,f))
end

