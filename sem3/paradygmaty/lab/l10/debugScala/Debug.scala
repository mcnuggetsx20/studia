package debugScala

import java.lang.reflect.Field

trait Debug{
  def debugName() : Unit ={
    printf("Klasa: %s\n", getClass().getSimpleName())
  }

  def debugVars() : Unit ={

    def helper(arr : List[Field]) : Unit ={
      arr match{
        case Nil => ()
        case field :: tail =>
          field.setAccessible(true)
          printf("Pole: %s => %s, %s\n", 
            field.getName, 
            field.getType, 
            field.get(this)
          )

          helper(tail)
      }
    }

    val fields : List[Field] = getClass().getDeclaredFields().toList
    helper(fields)
  }
}
