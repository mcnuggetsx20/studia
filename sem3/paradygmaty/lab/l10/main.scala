import debugScala.Point
import debugScala.Animal

object Main extends App{
  new Animal("pies"). debugName(); println()
  new Point(1, 10). debugName(); println()
  new Point(4, -100). debugName(); println()

  new Animal("pies"). debugVars(); println()
  new Point(1, 10). debugVars(); println()
  new Point(4, -100). debugVars(); println()

}
