import debugJava.Point;

public class main{
    public static void main(String[] args) throws IllegalAccessException {
        Point a = new Point(3,4);
        Point b = new Point(3,100);
        Point c = new Point(-1, 351);
        a.fields(a); System.out.println();
        b.fields(b); System.out.println();
        c.fields(c); System.out.println();
    }
}
