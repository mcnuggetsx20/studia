package debugJava;

import java.lang.reflect.Field;
import java.lang.IllegalAccessException;

public class Debug{
    public void fields(Object obj) throws IllegalAccessException{

        for(Field field : getClass().getDeclaredFields()){
            field.setAccessible(true);
            System.out.printf("Pole: %s => %s, %s\n", 
                    field.getName(), 
                    field.getType(),
                    field.get(obj)
            );

        }
    }
}
