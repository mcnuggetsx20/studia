let sum tab =
    let rec tailRec tab acc =
        match tab with
        | [] -> acc
        | head :: tail -> tailRec tail head+acc
    in
    tailRec tab 0
        

let fltr tab key =
    let rec tailRec tab acc=
        match tab with
        | [] -> acc
        | head::tail ->
                match (sum head = key) with
                | true-> tailRec tail (acc @ [head])
                | false -> tailRec tail acc

    in
    tailRec tab []

let toHex n =
    let n_abs = abs n in

    let rec tailRec n acc =
        match n with
        | 0 -> acc
        | _ -> tailRec (n/16) ((n mod 16) :: acc)
    in
    tailRec n_abs []

let merge tuple_list =
    let rec tailRec tuple_list acc =
        match tuple_list with
        | [] -> acc
        | head::tail ->
                match head with
                | (a, b, c, d) -> tailRec tail ((a^b^c^d) :: acc)
    in
    List.rev (tailRec tuple_list [])



