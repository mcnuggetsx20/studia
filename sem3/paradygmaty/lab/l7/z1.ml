let rec insert tab n =
    match tab with
    | [] -> [n]
    | head :: tail ->
            match (n <= head) with
            | true -> n :: tab
            | false -> head :: insert tail n
