public class z1 {

    public static int[] insert(int[] tab, int n){
        int[] ans = new int[tab.length+1];
        if(tab.length == 0){
            ans[0] = n;
            return ans;
        }

        boolean ok = true;
        int ans_index = 0;

        for(int i=0 ; i < tab.length; ++i){
            if(n <= tab[i] && ok){
                ans[ans_index++] = n;
                ok = false;
            }
            ans[ans_index++] = tab[i];
        }
        if(ok) ans[ans_index++] = n;
        return ans;
    }

    public static void test(int[][] tabs, int[] to_insert){
        for(int i = 0; i < Math.min(tabs.length, to_insert.length); ++i){
            int[] ans = insert(tabs[i], to_insert[i]);
            
            System.out.printf("insert [");
            for(int j = 0; j < tabs[i].length; ++j) System.out.printf("%d%s", tabs[i][j], (j==tabs[i].length-1) ? "" : " ");
            System.out.printf("] %d -> ", to_insert[i]);

            System.out.printf("[");
            for(int j = 0; j < ans.length; ++j) System.out.printf("%d%s", ans[j], j==ans.length-1 ? "" : " ");
            System.out.printf("]\n");
        }
    }

    public static void main(String[] args){
        int[] to_insert = {4, 6, 1, 2, 12, 5};
        int[][] tabs = {
            {1, 3, 5, 7},
            {},
            {0},
            {5},
            {13,13,13,13},
            {-4, 0, 0, 4},
        };

        test(tabs, to_insert);


    }
}
