type 'a nlist = Nil | Cons of 'a * ('a nlist);;
type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;

let rec to_nlist tab = 
    match tab with
    | [] -> Nil
    | head :: tail -> Cons(head, to_nlist tail)

let rec to_llist tab = 
    match tab with
    | [] -> LNil
    | head :: tail -> LCons(head, fun () -> to_llist tail)

let rec from_llist l_tab =
    match l_tab with
    | LNil -> []
    | LCons(head, tail) -> head :: from_llist (tail())

let rec nlist_concat tab sec =
    match tab with
    |Nil -> sec
    |Cons(head, tail) -> Cons(head, nlist_concat tail sec)

let rec llist_concat tab sec =
    match tab with
    |LNil -> sec
    |LCons(head, tail) -> LCons(head, fun () -> llist_concat (tail()) sec)

let rec powiel_helper element n =
    match n with
    | 0 -> Nil
    | _ -> Cons(element, powiel_helper element (n-1))

let rec lpowiel_helper element n =
    match n with
    | 0 -> LNil
    | _ -> LCons(element, fun () -> (lpowiel_helper element (n-1)) )

let powiel tab =
    let rec inner tab =
        match tab with
        | Nil -> Nil
        | Cons(head, tail) -> 
                match head >=0 with
                | false -> failwith "liczby musza byc calkowite i nieujemne"
                | true ->
                    let temp = powiel_helper head head in
                    nlist_concat temp (inner tail)
    in
    inner (to_nlist tab)

let lpowiel tab =
    let rec inner tab =
        match tab with
        | LNil -> LNil
        | LCons(head, tail) -> 
                match head >= 0 with
                |false -> failwith "liczby musza byc calkowite i nieujemne"
                | true ->
                    llist_concat (lpowiel_helper head head) (inner (tail()))
    in
    inner (to_llist tab)

