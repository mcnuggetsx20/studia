import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.ArrayList;
import java.util.HashMap;

public class SCC<W, S>{
    private Graf<W,S> graph;
    private Map<W, Boolean> visited;

    public SCC(Graf<W,S>graph){
        this.graph = graph;
        this.visited = new HashMap<>();
    }

    private void dfs(W vertex, Stack<W> stack){
        visited.putIfAbsent(vertex, true);

        for(W neighbor : this.graph.krawedzie(vertex)){
            if(!visited.containsKey(neighbor)) dfs(neighbor, stack);
        }
        stack.push(vertex);
        return;
    }

    private Graf<W,S> transposeGraph(){
        List<Graf.Edge<W,S>> edges_transposed = new ArrayList<>();

        for(W vertex : this.graph.wierzcholki()){
            for(W neighbor : this.graph.krawedzie(vertex)){

                S label = this.graph.krawedz(vertex, neighbor);
                edges_transposed.add(new Graf.Edge<W,S>(neighbor, vertex, label));
            }
        }

        return new Graf<W,S>(edges_transposed);
    }

    public List<List<W>> findSCC(){
        Stack<W> stack = new Stack<>();
        this.visited = new HashMap<>();
        
        //dfs1
        for(W vertex : this.graph.wierzcholki()){
            if(!this.visited.containsKey(vertex)) dfs(vertex, stack);
        }

        Graf<W,S> transposed = transposeGraph();
        this.graph = transposed;
        
        //dfs2
        this.visited.clear();

        Stack<W> new_stack = new Stack<>();
        List<List<W>> answer = new ArrayList<>();

        while(!stack.isEmpty()){
            W current_vertex = stack.pop();
            if(!this.visited.containsKey(current_vertex)) dfs(current_vertex, new_stack);

            if(!new_stack.isEmpty()) answer.add(new ArrayList<>(new_stack));
            new_stack.clear();
        }

        return answer;

    }
}
