import  java.util.ArrayList;
import java.util.Arrays;
import  java.util.List;


public class Main{
    public static void main(String[] args){

        List<Graf.Edge<Integer, String>> edges = new ArrayList<>();
        edges.add(new Graf.Edge<Integer,String>(1, 2, "a"));
        edges.add(new Graf.Edge<Integer,String>(2, 3, "b"));
        edges.add(new Graf.Edge<Integer,String>(2, 4, "b"));
        edges.add(new Graf.Edge<Integer,String>(3, 4, "c"));
        edges.add(new Graf.Edge<Integer,String>(3, 6, "c"));
        edges.add(new Graf.Edge<Integer,String>(4, 1, "c"));
        edges.add(new Graf.Edge<Integer,String>(4, 5, "c"));
        edges.add(new Graf.Edge<Integer,String>(5, 6, "c"));
        edges.add(new Graf.Edge<Integer,String>(6, 7, "c"));
        edges.add(new Graf.Edge<Integer,String>(7, 5, "c"));

        List<Graf.Edge<Integer, String>> edges1 = new ArrayList<>();
        edges1.add(new Graf.Edge<>(1, 2, "a"));
        edges1.add(new Graf.Edge<>(2, 3, "b"));
        edges1.add(new Graf.Edge<>(3, 1, "c"));
        edges1.add(new Graf.Edge<>(4, 3, "d"));

        List<Graf.Edge<Integer, String>> edges2 = new ArrayList<>();
        edges2.add(new Graf.Edge<>(1, 2, "a"));
        edges2.add(new Graf.Edge<>(2, 1, "b"));
        edges2.add(new Graf.Edge<>(3, 4, "c"));
        edges2.add(new Graf.Edge<>(4, 3, "d"));
        edges2.add(new Graf.Edge<>(5, 6, "e"));
        edges2.add(new Graf.Edge<>(6, 5, "f"));

        List<Graf.Edge<Integer, String>> edges3 = new ArrayList<>();
        edges3.add(new Graf.Edge<>(1, 2, "a"));
        edges3.add(new Graf.Edge<>(2, 3, "b"));
        edges3.add(new Graf.Edge<>(3, 1, "c"));
        edges3.add(new Graf.Edge<>(4, 5, "d"));

        Graf<Integer, String> test = new Graf<Integer, String>(edges);
        Graf<Integer, String> test1= new Graf<Integer, String>(edges1);
        Graf<Integer, String> test2 = new Graf<Integer, String>(edges2);
        Graf<Integer, String> test3 = new Graf<Integer, String>(edges3);

        System.out.println(test.wierzcholki());
        System.out.println(test.krawedz(2, 3));
        System.out.println(test.krawedz(3, 7));
        System.out.println(test.krawedzie(1));
        System.out.println(test.krawedzie(8));

        System.out.println(new SCC<Integer, String>(test).findSCC());
        System.out.println(new SCC<Integer, String>(test1).findSCC());
        System.out.println(new SCC<Integer, String>(test2).findSCC());
        System.out.println(new SCC<Integer, String>(test3).findSCC());
    }
}
