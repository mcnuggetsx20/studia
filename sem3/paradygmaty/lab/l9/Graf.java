import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class Graf<W,S> implements IGraf<W,S>{
    public static class Edge<W,S>{
        private final W first;
        private final W second;
        private final S label;

        public Edge(W first, W second, S label){
            this.first = first;
            this.second = second;
            this.label = label;
        }

    }
    private final Map<W, Map<W,S>> adj;

    public Graf(){
        adj = new HashMap<>();
        return;
    }

    public Graf(List<Edge<W,S>>edges){
        adj = new HashMap<>();

        for(Edge<W,S> edge : edges){
            adj.putIfAbsent(edge.first, new HashMap<>());
            adj.putIfAbsent(edge.second, new HashMap<>());

            adj.get(edge.first).put(edge.second, edge.label);
        }
        return;
    }

    @Override
    public List<W> wierzcholki(){
        return new ArrayList<W>(adj.keySet());
    }

    @Override
    public S krawedz(W w1, W w2){
        if(adj.containsKey(w1)) return adj.get(w1).getOrDefault(w2, null);
        return null;
    }

    @Override
    public List<W> krawedzie (W w){
        if(!adj.containsKey(w)) return null;
        return new ArrayList<W>(adj.getOrDefault(w, null).keySet());
    }
}
