object z2 {
  def ldzialanie(tab : Stream[Int], sec : Stream[Int], op : (Int, Int) => Int) : Stream[Int] ={
    (tab, sec) match{
      case (Stream.Empty, _) => sec
      case (_, Stream.Empty) => tab
      case (tab_head #:: tab_tail, sec_head #:: sec_tail) =>
        try{
          Stream.cons(op(tab_head, sec_head), ldzialanie(tab_tail, sec_tail, op))
        }
        catch {
          case e: ArithmeticException => 
            println("DOKONANO NIEDOZWOLONEJ OPERACJI")
            Stream()
        }
      case _ => Stream.Empty
    }
  }
}
