type 'a nlist = Nil | Cons of 'a * ('a nlist);;
type 'a llist = LNil | LCons of 'a * (unit -> 'a llist);;

let rec to_nlist tab = 
    match tab with
    | [] -> Nil
    | head :: tail -> Cons(head, to_nlist tail)

let rec to_llist tab = 
    match tab with
    | [] -> LNil
    | head :: tail -> LCons(head, fun () -> to_llist tail)

let rec from_nlist n_tab = 
    match n_tab with
    | Nil -> []
    | Cons(head, tail) -> head :: (from_nlist tail)



let rec from_llist l_tab =
    match l_tab with
    | LNil -> []
    | LCons(head, tail) -> head :: from_llist (tail())

let rec tuple_map (x,y) foo = (foo x, foo y)

let podziel tab =
    let rec helper tab n =
        match tab with
        | Nil -> (Nil, Nil)
        | Cons(head, tail) ->
                let (even, odd) = helper tail (n+1) in
                match (n mod 2) = 0 with
                | true -> (Cons(head, even), odd)
                | false -> (even, Cons(head, odd))

    in
    helper (to_nlist tab) 0

let lpodziel tab =
    let rec helper tab n =
        match tab with
        | LNil -> (LNil, LNil)
        | LCons(head, tail) -> 
                let (even, odd) = helper (tail()) (n+1) in

                match (n mod 2) = 0 with
                | true -> (LCons(head, fun () -> even), odd)
                | false -> (even, LCons(head, fun () -> odd))
    in
    (* let (even, odd) = helper (to_llist tab) 0
    in (from_llist even, from_llist odd) *)
    helper (to_llist tab) 0






