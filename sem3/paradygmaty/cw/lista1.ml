let rec flatten (tab: 'a list list) : 'a list=
    match tab with
    | [] -> []
    | x :: tab_rest -> x @ flatten tab_rest

let rec count (element: 'a) (tab: 'a list) : int=
    match tab with
    | [] -> 0
    | x :: tab_rest ->
            let ok = match (x = element) with
            | true -> 1
            | false -> 0
            in
            ok + (count element tab_rest)

let rec replicate obj counter =
    match counter with
    | 0 -> []
    | _ -> obj :: replicate obj (counter-1)

let rec sqrList (tab: int list): int list = 
    match tab with
    | [] -> []
    | element :: tab_rest -> (element*element) :: sqrList(tab_rest)

let rec palindrome tab =
    match tab with
    | [] -> true
    | x :: tail -> (List.rev tab) = tab
            
let rec listLength tab = 
    match tab with 
    | [] -> 0
    | x :: tail -> 1 + listLength tail
