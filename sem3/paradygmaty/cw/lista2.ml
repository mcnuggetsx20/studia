let rec fib (n : int): int =
    match n with
    | 0 -> 0
    | 1 -> 1
    | _ -> fib(n-1) + fib(n-2)

let fibTail (n: int) : int =
    let rec tailRec (n: int) (a : int) (b : int) : int =
        match n with
        | 0 -> 0
        | 1 -> b
        | _ -> tailRec (n-1) (b) (a+b)
    in
    tailRec n 0 1

let root3 (n: float): float =
    let rec tailRec (n : float) (x : float) : float =
        match abs_float ((x**3.) -. n) <= 1e-15 *. (abs_float n) with
        | true -> x
        | false -> tailRec n (x +. (n/.(x**2.) -. x)/. 3.)
    in
    match n>1. with
    | true -> tailRec n (n /. 3.)
    | false -> tailRec n n

let match0 (expr: 'a list) : bool =
    match expr with
    | [(a,b); (0,c)] -> true
    (* | [a;b; 0; c; d] -> true *)
    | _ -> false

let rec initSegment tab sec =
    match (List.length tab) > (List.length sec) with
    | true -> false
    | false ->
        match tab with
        | [] -> true
        | x :: tail -> 
                match( x = List.hd sec) with 
                | true -> initSegment tail (List.tl sec) 
                | false -> false

let rec replaceNth tab index element =
    match (tab, index) with
    | ([], _) -> []
    | (head::tail, 0) -> element :: tail
    | (head::tail, _) -> head :: (replaceNth tail (index-1) element)


