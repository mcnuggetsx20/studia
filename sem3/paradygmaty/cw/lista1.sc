import scala.annotation.tailrec
object lista1{

  val flatten: [A] => (List[List[A]]) => List[A] = [A] => (tab: List[List[A]]) =>{
    tab match{
      case Nil => Nil
      case _ => tab.head ::: flatten(tab.tail)
    }
  }

  val count: (Any, List[Any]) => Int =(element, tab) =>{
    tab match{
      case Nil => 0
      case _ => 
        val ok = (tab.head == element) match{
          case true => 1
          case _ => 0
        }
        ok + count(element, tab.tail)
    }
  }

  val replicate: (Any, Int) => List[Any] = (a, n) =>{
    n match{
      case 0 => List()
      case _ => a :: replicate(a, n-1)
    }
  }

  val sqrList: (List[Int]) => List[Int] = (tab) => {
    tab match{
      case Nil => List()
      case _ => tab.head * tab.head :: sqrList(tab.tail)
    }
  }

  val palindrome: (List[Any]) => Boolean = (tab) =>{
    tab match{
      case Nil => true
      case List(temp) => true
      case _ => 
        (tab.head == tab.last) match{
          case true => palindrome(tab.tail.init)
          case false => false
        }
    }
  }

  val listLength: (List[Any]) => Int = (tab) =>{
    @tailrec
    def helper(tab: List[Any], acc: Int) : Int ={
      tab match{
        case Nil => acc
        case _ => helper(tab.tail, acc+1)
      }
    }
    helper(tab, 0)
  }
}
