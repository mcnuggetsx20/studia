object lista4 {
  sealed trait BT[+A]
  case object Empty extends BT[Nothing]
  case class Node[+A](elem: A, left: BT[A], right: BT[A]) extends BT[A]

  sealed trait Graphs[A]
  case class Graph[A](f: A=>List[A]) extends Graphs[A]

  val tt = Node(1, Node(2, Node(4, Empty, Empty), Empty), Node(3, Node(5, Empty, Node(6, Empty, Empty)), Empty))
  val g = Graph((i: Int) =>
    i match
      case 0 => List(3)
      case 1 => List(0, 2, 4)
      case 2 => List(1)
      case 3 => Nil
      case 4 => List(0, 2)
      case n =>
        throw new Exception(s"Graph g: node $n doesn't exist")
  )

  def bfs[A](tree: BT[A]): List[A] = {

    @scala.annotation.tailrec
    def search[A](queue: List[BT[A]], vis : List[A]): List[A] = {
      queue match {
        case Nil => vis
        case Empty :: rest => search(rest, vis)
        case Node(value, left, right) :: rest =>
          val new_vis = vis ::: List(value)
          search(rest ::: List(left, right), new_vis)
      }
    }

    search(List(tree), List())
  }

  def inner_path[A](tree: BT[A]): Int = {
    def sum_depth[A](tree: BT[A], depth : Int): Int = {
      tree match {
        case Empty => 0
        case Node(value, left,right) => 
          depth + sum_depth(left, depth+1) + sum_depth(right, depth+1)
      }
    }
    sum_depth(tree, 0)
  }

  def outer_path[A](tree: BT[A]): Int = {
    def sum_depth[A](tree: BT[A], depth : Int): Int = {
      tree match {
        case Empty => depth
        case Node(value, left,right) => 
          sum_depth(left, depth+1) + sum_depth(right, depth+1)
      }
    }
    sum_depth(tree, 0)
  }

  def dfs[A](gr: Graph[A], start_node: A): List[A] = {
    def search(queue: List[A], vis : List[A]): List[A] = {
      queue match {
        case Nil => List()
        case head :: tail => 
          vis.contains(head) match {
            case true => search(tail, vis)
            case false => 
              val new_queue = (gr f head) ::: tail
              val new_vis = head :: vis
              head :: search(new_queue, new_vis)
          }
      }
    }
    search(List(start_node), List())
  }
}
