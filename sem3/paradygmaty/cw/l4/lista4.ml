type 'a bt = Empty | Node of 'a * 'a bt * 'a bt
type 'a graph = Graph of ('a -> 'a list)

let bfs tree = 
    let rec inner queue vis = 
        match queue with
        | [] -> vis
        | Empty :: rest -> inner rest vis
        | Node(value, left, right) :: rest ->
                let new_vis = vis @ [value] in
                inner (rest @ [left; right]) new_vis
    in
    inner [tree] []

let dfs (Graph gr) start_node=
    let rec inner queue vis = 
        match queue with
        | [] -> []
        | head :: tail ->
                match (List.mem head vis) with
                | true -> inner tail vis
                | false -> head :: (inner ((gr head) @ tail) (head :: vis))
    in
    inner [start_node] []

let inner_path tree = 
    let rec sum_depth tree depth = 
        match tree with
        | Empty -> 0
        | Node(value, left, right) ->
                depth + (sum_depth left (depth+1)) + (sum_depth right (depth+1))
    in
    sum_depth tree 0


let outer_path tree = 
    let rec sum_depth tree depth =
        match tree with
        | Empty -> depth
        | Node(value, left, right) ->
                (sum_depth left (depth+1)) + (sum_depth right (depth+1))
    in
    sum_depth tree 0










