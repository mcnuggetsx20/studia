(*zadanie 2*)
let sugar_uncurry3 curried_function = fun (a, b, c) -> curried_function a b c
let uncurry3 curried_function = 
    let foo (a, b, c) = curried_function a b c
    in foo

let sugar_curry3 uncurried_function (a, b, c) = fun a b c -> uncurried_function (a, b, c)
let curry3 uncurried_function (a, b, c) =
    let foo a b c = uncurried_function (a,b,c)
    in foo

let uncurr_add (a, b, c) = a + b + c
let curr_add a b c = a + b + c

(* zadanie 3 *)
let sumProd = function
    | [] -> (0, 1)
    | tab -> List.fold_left (fun (sum, prod) a -> (sum + a, prod * a)) (0, 1) tab

(* zadanie 5 *)
let rec insert cmp n = function
    | [] -> [n]
    | head :: tail as tab ->
        match cmp n head with
        | true -> n :: tab
        | false -> head :: (insert cmp n tail)

let rec insertion_sort cmp = function
    | [] -> []
    | head :: tail -> insert cmp head (insertion_sort cmp tail)


let rec split = function
    | [] -> ([], [])
    | [x] -> ([x],[])
    | h1 :: h2 :: tail ->
        let (a, b) = split tail in
        (h1 :: a, h2 :: b)

let rec merge cmp tab sec = 
    match (tab, sec) with
    | ([], a)  | (a, []) -> a
    | (h1 :: t1, h2 :: t2) ->
        match (cmp h1 h2) with
        | true -> h1 :: (merge cmp t1 sec)
        | false -> h2 :: (merge cmp t2 tab)

let rec merge_sort cmp = function
    | [] | [_] as tab -> tab
    | tab ->
        let (a,b) = split tab in
        merge cmp (merge_sort cmp a) (merge_sort cmp b)

let lst = [(1, "a"); (2, "b"); (1, "c"); (2, "d")]
let cmp (x, _) (y, _) = x <= y
