object lista3 {
  // zadanie 2
  def sugar_curry3[A,B,C,D](uncurried_function : (A,B,C) => D) : A => B => C => D ={
    a => b => c => uncurried_function(a, b, c)
  }

  def curry3[A, B, C, D](uncurried_function : (A,B,C) => D) : A => B => C => D ={
    (a : A) => (b : B) =>(c : C) => uncurried_function(a, b, c)
  }

  def sugar_uncurry3[A, B, C, D](curried_function : A => B => C => D) : (A,B,C) =>D ={
    (a,b,c) => curried_function(a)(b)(c)
  }
  def uncurry3[A, B, C, D](curried_function : A => B => C => D) : (A,B,C) =>D ={
    (a : A, b : B, c : C) => curried_function(a)(b)(c)
  }

  // zadanie 3
  def sumProd(tab : List[Int]) : (Int, Int) ={
    tab match {
      case Nil => (0, 1)
      case _ =>
        tab.foldLeft((0,1))((acc,element) => {
          val (sum,prod) = acc
          (sum+element, prod*element)
        })
    }
  }

  // zadanie 5

  def insert[A](cmp : (A, A) => Boolean, n : A, tab : List[A]) : List[A] ={
    tab match{
      case Nil => List(n)
      case _ =>
        cmp(n, tab.head) match{
          case true => n :: tab
          case false => tab.head :: insert(cmp, n, tab.tail)
        }
    }
  }

  def insertion_sort[A](cmp : (A,A) => Boolean, tab : List[A]): List[A] ={
    tab match{
      case Nil => List()
      case List(x) => List(x)
      case _ =>
        insert(cmp, tab.head, insertion_sort(cmp, tab.tail))
    }
  }
  
  def split[A](tab : List[A]) : (List[A], List[A]) = {
    tab match {
      case Nil => (Nil, Nil)
      case List(element) => (List(element), Nil)
      case _ =>
        val (left, right) = split(tab.tail.tail)
        (tab.head :: left, tab.tail.head :: right)
    }
  }

  def merge[A](cmp : (A,A) => Boolean, tab : List[A], sec : List[A]) : List[A] ={
    (tab, sec) match{
      case (Nil, a) => a
      case (a, Nil) => a
      case _ =>
        cmp(tab.head, sec.head) match{
          case true => tab.head :: merge(cmp, tab.tail, sec)
          case false => sec.head :: merge(cmp, tab, sec.tail)
        }
    }
  }

  def merge_sort[A](cmp : (A,A) => Boolean, tab : List[A]) : List[A] ={
    tab match{
      case Nil => List()
      case List(element) => List(element)
      case _ =>
        val (left, right) = split(tab)
        merge(cmp, merge_sort(cmp, left), merge_sort(cmp, right))
    }
  }
  val cmpAscending: (Int, Int) => Boolean = (a, b) => a <= b

}
