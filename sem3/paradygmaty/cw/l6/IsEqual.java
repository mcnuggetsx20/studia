public class IsEqual{
    static boolean isEqual1(int m, int n){return n==m;}
    static boolean isEqual2(Integer m, Integer n){return m == n;}

    public static void main(String[] args){
        int[] ints = {1,2,3};
        for(int i : ints) {
            System.out.println(i); i = 0;
        }

        for(int i : ints)
            System.out.println(i);

        int[] ints2 = ints;
        for(int i=0; i<ints2.length; i++) {
            System.out.println(ints2[i]); ints2[i] = -1;
        }

        for(int i : ints) System.out.println(i);

        int[] a = {1,2,3};
        int[] b = a;
        b[0] = 10;
        System.out.println(a[0]);
    }
}
