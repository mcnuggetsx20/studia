type 'a llist = LNil | LCons of 'a * 'a llist Lazy.t;;

let rec toLazyList = function
    [] -> LNil
    | x :: xs -> LCons(x, lazy (toLazyList xs))

let rec from_llist = function
    | LNil -> []
    | LCons(head, lazy tail) -> head :: from_llist (tail)

let lrepeat ltab n =
    let rec prop n k e =
        match k with
        | 0 -> e
        | _ -> LCons(n, lazy ( prop n (k-1) e))
    in

    let rec solve ltab = 
        match ltab with
        | LNil -> LNil
        | LCons(head, lazy tail) -> (prop head n (solve tail))
    in

    from_llist (solve ltab)

let list_rev tab = 
    let rec aux tab acc =
        match tab with 
        | [] -> acc
        | head :: tail -> aux tail (head :: acc)
    in 
    aux tab []

type 'a ibt = LEmpty | LNode of 'a * (unit -> 'a ibt) * (unit -> 'a ibt);;

let rec take tab n = 
    match(tab, n) with
    | (LNil, _) -> []
    | (_, 0) -> []
    | (LCons(head, lazy tail), _) -> head :: take tail (n-1)

let l_bfs tree = 
    let rec search queue = 
        match queue with
        | [] -> LNil
        | LEmpty :: tail -> search tail 
        | LNode(value, left, right) :: tail -> LCons(value, lazy (search (tail@[left(); right()])))
    in
    search [tree]

let rec l_tree n =
    LNode(n, (fun () -> l_tree (2*n)), (fun () -> l_tree (2*n+1)))

