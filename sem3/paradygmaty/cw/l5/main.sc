object main {
    sealed trait IBT[+A]
    case object LEmpty extends IBT[Nothing]
    case class LNode[+A](value : A, left : () => IBT[A] , right : () => IBT[A]) extends IBT[A]

    def toList[A](ltab : LazyList[A]) : List[A] ={
      val LNil = LazyList.empty
      ltab match{
        case LNil => List()
        case head #:: tail =>
          head :: toList(tail)
      }
    }

    def lrepeat[A](ltab : LazyList[A], k : Int) = {
      def prop(n : A, k : Int, last : LazyList[A]) : LazyList[A] ={
        k match{
          case 0 => last
          case _ =>
            LazyList.cons(n, prop(n, k-1, last))
        }
      }
      
      val LNil = LazyList.empty
      def solve(ltab : LazyList[A]) : LazyList[A] = {
        ltab match{
          case LNil => LazyList.empty[A]
          case head #:: tail =>
            prop(head, k, solve(tail))
        }
      }

      solve(ltab)
    }

    def lTree(n : Int) : IBT[Int] ={
      LNode(n, () => lTree(2*n), () => lTree(2*n+1))
    }

    def lBFS[A](tree : IBT[A]) : LazyList[A] = {
      def search(queue : List[IBT[A]]) : LazyList[A] = {
        queue match{
          case Nil => LazyList.empty
          case LNode[A](value, left, right) :: tail =>
            LazyList.cons(value, search(tail ::: List(left(), right())))
          case LEmpty :: tail => search(tail)
        }
      }
      search(List(tree))
    }
}
