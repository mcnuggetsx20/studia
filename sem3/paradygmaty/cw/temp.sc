object temp {

  def fact(n: Int): Int = {
    def aux(n : Int, acc: Int) : Int ={
      n match{
        case 0 => acc
        case _ => aux(n-1, n*acc)
      }
    }
    if (n <= 0) then 1
    else aux(n, 1)
    
  }
}
