object lista2 {
  val fib: Int => Int = (n) => {
    n match{
      case 0 => 0
      case 1 => 1
      case _ => fib(n-1) + fib(n-2)
    }
  }
  val fibTail: Int => Int = (n) => {
    @scala.annotation.tailrec()
    def tailRec(n: Int, a : Int, b: Int) : Int ={
      n match{
        case 0 => 0
        case 1 => b
        case _ => tailRec(n-1, b, a+b)
      }
    }
    tailRec(n, 0, 1)
  }

  val root3Fun: Double => Double = (n) => {

    @scala.annotation.tailrec
    def tailRec(n: Double, x: Double) : Double ={
      (scala.math.abs(scala.math.pow(x,3) - n) <= 1e-15 * scala.math.abs(n)) match{
        case true => x
        case false => tailRec(n, x+ (n/scala.math.pow(x,2) - x)/3)
      }
    }

    (n > 1) match{
      case true => tailRec(n, n/3)
      case false => tailRec(n, n)
    }
  }

  val List(_,_, x, _, _) = List(-1, -2, 0, 1, 2)
  val List((_,_),(y, _)) = List((1,2), (0, 1))

  @scala.annotation.tailrec
  def initSegment[A](tab : List[A], sec: List[A]) : Boolean = {

    if(tab.length > sec.length) false
    else if(tab.isEmpty) true
    else if(tab.head == sec.head) initSegment(tab.tail, sec.tail)
    else false
  }

  val replaceNth: [A] => (List[A], Int, A) => List[A] = [A] => (tab, n, element) =>{
    (tab, n) match{
      case (Nil, _) => Nil
      case (_, 0) => element :: tab.tail
      case (_, _) => tab.head :: replaceNth(tab.tail, n-1, element)
    }
  }
}
