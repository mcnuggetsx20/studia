#include "CNode.h"
#include <cstdlib>
#include <cstring>
#include <cmath>
// #include <algorithm>
#include <map>

const cnode_map_type CNode::V_OPERATOR_MAP = map_init_operators();
const std::string CNode::S_DEFAULT_CHILD = "1";

CNode::CNode(const std::string &asc_tokens){
    v_init_fields(0, i_find_num_children(asc_tokens), asc_tokens);
}

CNode::CNode(){
    std::string s_temp = "R";
    v_init_fields(0, I_ROOT_MAX_CHILDREN, s_temp);
}

CNode::CNode(const CNode &acp_other){
    v_init_fields(0, acp_other.i_max_children, acp_other.s_text);
    v_copy_all_children(this, &acp_other);
}

CNode::~CNode(){
    for(int i = 0; i < i_curr_children; ++i){
        delete ctp_children[i];
        ctp_children[i] = NULL;
    } 
    i_curr_children = 0;
    delete[] ctp_children;
}

void CNode::v_init_fields(int ai_curr_children, int ai_max_children, const std::string &as_text){
    i_curr_children = ai_curr_children;
    i_max_children = ai_max_children;
    ctp_children = new CNode*[i_max_children];
    
    //ustawienie wszystkich pointerow na null
    for(int i = 0; i < i_max_children; ++i) ctp_children[i] = NULL;

    s_text = as_text;
    if(!as_text.size() || (as_text.size() == 1 && (int)as_text[0] == 0)){
        s_text = S_DEFAULT_CHILD;
    }
    i_type = i_determine_type(as_text);
}

void CNode::v_copy_all_children(CNode* acp_parent, const CNode* acp_node){
    if(acp_node == NULL) return;
    for(int i = 0 ; i < acp_node->i_curr_children; ++i){
        acp_parent->v_add_child(acp_node->ctp_children[i]->s_text);
        int i_next_parent = acp_parent->i_curr_children-1;
        v_copy_all_children(acp_parent->ctp_children[i_next_parent], acp_node->ctp_children[i]);
    }
    return;
}

std::string CNode::s_to_string() const{
    return std::string(s_text.begin(), s_text.end());
}

void CNode::v_add_child(const std::string &asc_tokens){
    if(i_curr_children == i_max_children) return;
    ctp_children[i_curr_children++] = new CNode(asc_tokens);
}

int CNode::i_determine_type(const std::string &asc_tokens){
    cnode_map_type::const_iterator map_iter;
    map_iter = V_OPERATOR_MAP.find(asc_tokens);

    if(map_iter != V_OPERATOR_MAP.end()) return I_OPERATOR_CODE;

    for(unsigned int i = 0 ; i < asc_tokens.size(); ++i){
        char c = asc_tokens[i];
        if( (int(c) > 57 || int(c) < 48)){
            return I_VARIABLE_CODE;
        }
    }
    //jesli tu jestesmy to token moze byc juz tylko liczba,
    //bo na pewno mamy dobre znaki dzieki CTree
    return I_NUMBER_CODE;

}

char CNode::ch_get_valid_char(const char &ach_token){
    std::string s_temp = ""; s_temp += ach_token;
    cnode_map_type::const_iterator map_iter = V_OPERATOR_MAP.find(s_temp);
    if(map_iter != V_OPERATOR_MAP.end()) return ach_token;

    char ch_result = std::tolower(ach_token);
    int i_temp = int(ch_result);
    if ((i_temp >= 48 && i_temp < 57) || (i_temp >= 65 && i_temp <=90) || (i_temp >= 97 && i_temp <=122)){
        return ch_result;
    }
    return 0;
}

int CNode::i_find_num_children(const std::string &asc_tokens){
    cnode_map_type::const_iterator map_iter;
    map_iter = V_OPERATOR_MAP.find(asc_tokens);

    if(map_iter == V_OPERATOR_MAP.end()) return 0; //to znaczy ze nie ma takiego operatora
    return map_iter -> second.first;
}

bool CNode::b_is_operator() const{ return i_type == I_OPERATOR_CODE;}
bool CNode::b_is_variable()const{ return i_type == I_VARIABLE_CODE;}
bool CNode::b_is_number()const{ return i_type == I_NUMBER_CODE;}
bool CNode::b_is_true()const{ return true;}
bool CNode::b_is_leaf() const {return i_max_children ==0;}

double CNode::d_str_to_double(const std::string &as_text){
        char* ch_end;
        const char* ch_cstr = as_text.c_str();
        return std::strtod(ch_cstr, &ch_end);
}

cnode_map_type CNode::map_init_operators(){
    cnode_map_type m_result;
    m_result["/"] = std::make_pair(2, &CNode::d_divide);
    m_result["*"] = std::make_pair(2, &CNode::d_multiply);
    m_result["+"] = std::make_pair(2, &CNode::d_add);
    m_result["-"] = std::make_pair(2, &CNode::d_subtract);
    m_result["sin"] = std::make_pair(1, &CNode::d_sin);
    m_result["cos"] = std::make_pair(1, &CNode::d_cos);
    return m_result;
}

std::ostream& operator<<(std::ostream& os, const CNode& obj) {
    os << obj.s_to_string();
    return os;
}

double CNode::d_add(const std::vector<double> &avd_comps){ return avd_comps[0] + avd_comps[1];}
double CNode::d_subtract(const std::vector<double> &avd_comps){ return avd_comps[0] - avd_comps[1];}
double CNode::d_multiply(const std::vector<double> &avd_comps){ return avd_comps[0] * avd_comps[1];}
double CNode::d_divide(const std::vector<double> &avd_comps){ return avd_comps[0] / avd_comps[1];}
double CNode::d_sin(const std::vector<double> &avd_comps){ return std::sin(avd_comps[0]);}
double CNode::d_cos(const std::vector<double> &avd_comps){ return std::cos(avd_comps[0]);}
