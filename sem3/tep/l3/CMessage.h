#ifndef CMESSAGE_H
#define CMESSAGE_H

#include <iostream>
#include <vector>

class CMessage{
    public:
        CMessage();
        CMessage(const CMessage &amp_other);
        ~CMessage();

        std::string last;
        std::vector<std::string> sv_message_list;

        void add_message(std::string as_to_add);
        void upload(std::string as_to_add);
        std::string next_message();

        void new_message();
        void close_message();

        friend std::ostream& operator<<(std::ostream& os, CMessage& obj);
        CMessage& operator=(CMessage amp_other);

    private:
        std::string temp_message;
        void v_swap_with(CMessage &amp_other);

};

#endif
