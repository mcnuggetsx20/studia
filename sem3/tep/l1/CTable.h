#ifndef CTABLE_H
#define CTABLE_H

#include <string>

using namespace std;

class CTable {
    private:
        string s_name;
        int* itp_table;
        int i_table_size;

    public:
        //konstruktory
        CTable();
        CTable(string as_name, int ai_table_len);
        CTable(const CTable &apc_other);

        //destruktor
        ~CTable();

        //gettery
        string getName() const {return s_name;};
        int getTableSize() const {return i_table_size;};
        //int* getTable();

        //settery
        void vSetName(string as_name){s_name = as_name;};
        bool bSetNewSize(int ai_table_len);

        //reszta
        CTable* pcClone();
        void printTable();

        //modyfikacja
        bool bDoubleLength();
};

#endif
