#include <iostream>
#include "constants.h"
#include "CTable.h"

using namespace std;

void v_alloc_table_fill_34(int i_size){
    if(i_size <= 0) return;
    
    int *itp_tab = new int[i_size];
    for(int i = 0 ; i < i_size; ++i){
        itp_tab[i] = I_TABLE_DEFAULT_VALUE;
    }

    // wypisanie
    cout << "v_alloc_table_fill_34; size: " << i_size << '\n';
    for(int i = 0; i < i_size; ++i){
        cout << itp_tab[i] << " ";
    }
    cout <<'\n';

    delete[] itp_tab;
}

bool b_alloc_table_2_dim(int*** itp_2d_table, int i_size_x, int i_size_y){
    if(i_size_x <=0 || i_size_y <= 0) return false;

     *itp_2d_table = new int*[i_size_y];
     for(int i = 0; i < i_size_y; ++i){
        (*itp_2d_table)[i] = new int[i_size_x]();
    }

    return true;
}

//cleanup
//mozna by usunac size_x bo go nie uzywamy do cleanupu
bool b_dealloc_table_2_dim(int ***itp_2d_table, int i_size_x, int i_size_y){
    if(i_size_x <=0 || i_size_y <= 0 || itp_2d_table == NULL) return false;

    for(int i = 0; i < i_size_y; ++i){
        delete[] (*itp_2d_table)[i];
    }

    delete[] *itp_2d_table;
    *itp_2d_table = NULL;
    return true;
}

void v_mod_tab(CTable *apc_tab){
    apc_tab->vSetName("zmienilem nazwe z pointerem");
}

void v_mod_tab(CTable ac_tab){
    ac_tab.vSetName("raczej sie nie uda tak");
}
