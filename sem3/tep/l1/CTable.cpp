#include "constants.h"
#include "CTable.h"

using namespace std;

//konstruktory
CTable::CTable(){
    s_name = S_DEFAULT_CTABLE_NAME;
    itp_table = new int[I_DEFAULT_CTABLE_LENGTH]();
    i_table_size = I_DEFAULT_CTABLE_LENGTH;

    cout << "bezp: " << s_name <<'\n';
}

CTable::CTable(string as_name, int ai_table_len){
    if(ai_table_len < 0 ){
        ai_table_len = I_DEFAULT_CTABLE_LENGTH;
    }

    s_name = as_name;
    cout << "parametr: " << s_name << '\n';

    i_table_size = (ai_table_len ? ai_table_len : I_DEFAULT_CTABLE_LENGTH);
    itp_table = new int[i_table_size]();
}

CTable::CTable(const CTable &apc_other){
    s_name = apc_other.getName() + "_copy";

    //delete[] itp_table;
    i_table_size = apc_other.getTableSize();
    itp_table = new int[i_table_size]();

    for(int i = 0; i < i_table_size; ++i){
        itp_table[i] = apc_other.itp_table[i];
    }

    cout << "kopiuj: " << s_name << '\n';
}

CTable::~CTable(){
    cout << "usuwam " << s_name <<'\n';
    delete[] itp_table;
}

CTable* CTable::pcClone(){
    return new CTable(*this);
}

void CTable::printTable(){
    for(int i = 0; i < i_table_size; ++i){
        cout << itp_table[i] << ' ';
    }
    cout << '\n';
}

//getters
/* int* CTable::getTable(){
    int* it_temp_tab = new int[i_table_size];
    for(int i = 0; i < i_table_size; ++i){
        it_temp_tab[i] = itp_table[i];
    }
    return it_temp_tab;
} */

//setters
bool CTable::bSetNewSize(int ai_table_len){
    if(ai_table_len < 0 || ai_table_len > I_MAX_CTABLE_SIZE/2) return false;

    //przygotowujemy tablice tymczasowa
    int* itp_new_table = new int[ai_table_len]();
    for(int i = 0; i < min(ai_table_len, i_table_size); ++i){
        itp_new_table[i] = itp_table[i];
    }

    i_table_size = ai_table_len;
    //usuwanie starej wersji tablicy
    delete[] itp_table;

    //tworzymy nowa tablice i kopiujemy z tymczasowej
    //specjalnie nie dajemy () bo chcemy zeby tablica miala
    //losowe wartosci, co przeklada sie na szybsze dzialanie;
    //mozemy tak zrobic bo i tak nadpisujemy ja danymi z 
    //tablicy tymczasowej
    itp_table = new int[i_table_size];
    for(int i = 0 ; i< i_table_size; ++i) itp_table[i] = itp_new_table[i];

    //usuwamy tymczasowa tablice
    delete[] itp_new_table;

    return true;

}

bool CTable::bDoubleLength(){
    if(itp_table == NULL || i_table_size > I_MAX_CTABLE_SIZE/2){ 
        return false;
    }

    i_table_size*=2;

    delete[] itp_table;
    itp_table = new int[i_table_size]();

    return true;
}



