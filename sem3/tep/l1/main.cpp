#include "CTable.cpp"
#include "functions.cpp"

#define dbg(out) (cout << out << ' ')
#define dbgn(out) cout << out << '\n';

using namespace std;

int main(){

    //z1
    v_alloc_table_fill_34(10);
    dbgn("");

    //z2 + z3
    int **itpp_table_2d;
    int i_table_size_x = 5, i_table_size_y = 7;
    bool b_status = b_alloc_table_2_dim(&itpp_table_2d,i_table_size_x, i_table_size_y);

    dbgn("wypisanie zaalokowanej tablicy 2d")
    for(int i = 0 ; i < i_table_size_y; ++i){
        for(int j = 0; j < i_table_size_x; ++j){
            dbg((itpp_table_2d)[i][j]);
        }
        dbgn("");
    }
    dbgn("");
    b_status = b_dealloc_table_2_dim(&itpp_table_2d, i_table_size_x, i_table_size_y);

    cout << "stan pointera podwojnego: " << itpp_table_2d <<'\n';
    dbgn("");

    //z4
    CTable c_ctable_object("obiekt ctable", 0); //konstruktor z parametrami
    c_ctable_object.printTable();
    c_ctable_object.bSetNewSize(15);
    c_ctable_object.printTable();

    CTable c_copy_test(c_ctable_object); //konstruktor kopiujacy
    
    CTable benc("do podwojenia", 1000);
    cout << "status podwojenia: " << benc.bDoubleLength() << '\n';

    CTable *pc_new_tab; //klonowanie
    pc_new_tab = c_copy_test.pcClone();
    delete pc_new_tab;

    CTable* ct_ctable_array; //tablica obiektow
    ct_ctable_array = new CTable[I_CTABLE_TEST_SIZE];
    delete[] ct_ctable_array;

    v_mod_tab(&c_ctable_object); //procedury zmieniajace
    dbgn(c_ctable_object.getName());

    return 0;
}

