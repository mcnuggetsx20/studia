#ifndef CONSTANTS
#define CONSTANTS

#include <iostream>

//CTABLE
const std::string   S_DEFAULT_CTABLE_NAME = "default name";
const int           I_DEFAULT_CTABLE_LENGTH = 2;
const int           I_MAX_CTABLE_SIZE = 1e6;
const int           I_CTABLE_TEST_SIZE = 5;

const int           I_TABLE_DEFAULT_VALUE = 34;



#endif
