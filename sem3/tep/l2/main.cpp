#include <iostream>
#include "CNumber.cpp"
#include <climits>

using namespace std;

int main(){

    /* CNumber num1, num2, result;
    int i_int1=1, i_int2=0, cor;
    bool ok;

    while(i_int1!=i_int2){
        cout << "two numbers: ";
        cin >> i_int1 >> i_int2;

        num1 = i_int1; num2 = i_int2;

        cor = i_int1 / i_int2;
        result = num1 / num2;

        num1 = cor;
        ok = (result == num1);

        cout << "cor: " << cor << '\n';
        cout << "cnu: " << result << '\n';
        cout << (ok ? "PASSED" : "FAILED") << "\n-------\n";
    } */

    CNumber c_num1, c_num2;
    c_num1 ^= 123;
    cout << c_num1 << '\n';

    c_num1 = 379; c_num2 = 999;
    cout << c_num1 - c_num2 << '\n';

    c_num1 = 379; c_num2 = -999;
    cout << c_num1 + c_num2 << '\n';

    c_num1 = 1000; c_num2 = 22;
    cout << c_num1 - c_num2 << '\n';

    c_num1 = 999; c_num2 = 1;
    cout << c_num1 + c_num2 << '\n';

    c_num1 = 37; c_num2 = -11;
    cout << c_num1 * c_num2 << '\n';

    c_num1 = -99; c_num2 = -2;
    cout << c_num1 * c_num2 << '\n';

    c_num1 = 121; c_num2 = 3012;
    cout << c_num1 * c_num2 << '\n';

    c_num1 = INT_MAX; c_num2 = INT_MAX;
    cout << c_num1 * c_num2 << '\n';

    c_num1 = 364452; c_num2 = 13;
    cout << c_num1 / c_num2 << '\n';

    c_num1 = 3448182; c_num2 = -123;
    cout << c_num1 / c_num2 << '\n';

    c_num1 = -89998; c_num2 = -7777;
    cout << c_num1 / c_num2 << '\n';

    CNumber benc; int n = 20;
    benc = 124;
    while(n--) benc = benc * 2;
    cout << benc << '\n';
}


