#ifndef CNUMBER_H
#define CNUMBER_H

#include <iostream>

class CNumber{
    public:
        CNumber();
        CNumber(int i_base);
        CNumber(const CNumber &apc_other);
        ~CNumber();
        std::string sToString() const;

        void operator=(const CNumber &apc_other);
        void operator=(int i_value);

        CNumber operator+(const CNumber &apc_other) const;
        CNumber operator+(int i_value) const;

        CNumber operator-(const CNumber &apc_other) const;
        CNumber operator-(int i_value) const;

        CNumber operator*(const CNumber &apc_other) const;
        CNumber operator*(int i_value) const;

        CNumber operator/(const CNumber &apc_other) const;
        CNumber operator/(int ia_value) const;

        CNumber operator%(const CNumber &apc_other) const;
        CNumber operator%(int ia_value) const;

        void operator^=(const int ia_value);

        bool operator>(const CNumber &apc_other) const ;
        bool operator<(const CNumber &apc_other) const;
        bool operator>=(const CNumber &apc_other) const;
        bool operator<=(const CNumber &apc_other) const;
        bool operator==(const CNumber &apc_other) const;

        bool operator==(const int ia_value) const;

        CNumber abs(const CNumber &apc_other) const;

    private:
        int* itp_table;
        int i_table_size;
        bool b_sign;
        int i_sys_base;

        static const int I_DEFAULT_VALUE = 0;
        static const int I_DEFAULT_SIZE = 1;
        static const int I_HALVER = 2;

        void normalize();
        CNumber mult10(int i_exponent) const;
        CNumber halve(const CNumber &apc_other) const;
};

#endif
