#include "CNumber.h"
#include <vector>
#include "functions.cpp"

CNumber::CNumber(){
    i_sys_base = 10;
    i_table_size = I_DEFAULT_SIZE;
    itp_table = new int[i_table_size]();
    b_sign = true;
}

CNumber::CNumber(const CNumber &apc_other){
    itp_table = new int[apc_other.i_table_size];
    i_table_size = apc_other.i_table_size;
    b_sign = apc_other.b_sign;

    for(int i = 0; i < i_table_size; ++i){
        itp_table[i] = apc_other.itp_table[i];
    }
}

CNumber::~CNumber(){
    delete[] itp_table;
}

void CNumber::operator=(const CNumber &apc_other){
    if(this == &apc_other) return;
    
    delete[] itp_table;
    itp_table = new int[apc_other.i_table_size];
    for(int i = 0; i < apc_other.i_table_size; ++i){
        itp_table[i] = apc_other.itp_table[i];
    }

    i_table_size = apc_other.i_table_size;
    b_sign = apc_other.b_sign;
}

void CNumber::operator=(int ia_value){
    b_sign = (ia_value >=0);
    if(ia_value <0) ia_value*=-1;

    int i_new_size = 0, i_value = ia_value;
    while(i_value > 0){
        i_value/=i_sys_base;
        i_new_size++;
    }
    i_table_size = std::max(1, i_new_size);
    //std::cout << "utworzono " << ia_value << " dlugosc: " << i_table_size << " znak " << b_sign << '\n';

    delete[] itp_table;
    itp_table = new int[i_table_size];

    int i_index = i_table_size -1;
    if(!ia_value) itp_table[0] = 0;
    while(ia_value > 0){
        itp_table[i_index--] = ia_value%i_sys_base;
        ia_value/=i_sys_base;
    }

}

CNumber CNumber::operator+(const CNumber &apc_other) const{
    CNumber c_result;
    //std::cout << this->sToString() << ' ' <<apc_other.sToString() << '\n';

    //-a + b = b - a
    if(!b_sign && apc_other.b_sign){
        c_result = *this;
        c_result.b_sign = true;
        c_result = apc_other - c_result;
        return c_result;
    }
    //a + (-b) = a - b;
    else if(b_sign && !apc_other.b_sign){
        c_result = apc_other;
        c_result.b_sign = true;
        c_result = *this - c_result;
        return c_result;
    }
    int i_this_index = i_table_size,
        i_other_index = apc_other.i_table_size,
        i_carry = 0;

    int i_res_size = std::max(i_table_size, apc_other.i_table_size) + 1; //res ma miec wiecej miejsc
    int i_res_index = i_res_size,
        i_sum = 0;

    //c_result = std::max(i_table_size, apc_other.i_table_size) * 10; //zawsze o jedno miejsce wiecej niz max
    delete[] c_result.itp_table;

    //dzieki zabezpieczeniom na gorze tej metod sprowadzamy dodawanie tylko do:
    //1) dodawania dwoch liczb dodatnich
    //2) dodawania dwoch liczb ujemnych
    //w zwiazku z tym znak wyniku bedzie na pewno taki sam jak znak ktoregokolwiek z
    //*this i apc_other, wiec wybralem losowo akurat *this->b_sign ale mogloby rownie
    //dobrze byc apc_other.b_sign
    c_result.itp_table = new int[i_res_size]();
    c_result.i_table_size = i_res_size;
    c_result.b_sign = b_sign;
    
    while(i_res_index--){
        i_this_index--;
        i_other_index--;

        i_sum = (i_this_index >= 0 ? itp_table[i_this_index] : 0) + 
                (i_other_index >= 0 ? apc_other.itp_table[i_other_index] : 0) + 
                i_carry;
        /* std::cout << i_sum << "sum\n";
        std::cout << i_this_index << ' ' << i_other_index << "indexy\n";
        std::cout << (i_this_index >= 0 ? itp_table[i_this_index] : 0) << "\n"; 
        std::cout << (i_other_index >= 0 ? apc_other.itp_table[i_other_index] : 0)  <<'\n'; */

        c_result.itp_table[i_res_index] = i_sum % i_sys_base;
        i_carry = i_sum / i_sys_base;

    }
    // std::cout << c_result.sToString() << '\n';

    //poprawiamy wynik jesli jednak nie potrzeba kolejnego miejsca
    c_result.normalize();

    return c_result;
}

CNumber CNumber::operator+(int ia_value) const {
    CNumber c_temp;
    c_temp = ia_value;
    return *this + c_temp;
}

CNumber CNumber::operator-(const CNumber &apc_other) const {
    CNumber c_bigger, c_smaller, c_result;

    //-a -b  = -a + (-b) ---> obslugiwane w dodawaniu
    if(!b_sign && apc_other.b_sign){
        c_result = apc_other;
        c_result.b_sign = false;
        c_result = *this + c_result;
        return c_result;
    }

    //a - (-b) = a+b
    else if(b_sign && !apc_other.b_sign){
        c_result = apc_other;
        c_result.b_sign = true;
        c_result = *this + c_result;
        return c_result;
    }

    c_bigger = std::max( abs(*this), abs(apc_other));
    c_smaller = std::min( abs(*this), abs(apc_other));

    //tak jak w dodawaniu zakladamy wiekszy rozmiar wyniku
    int i_res_size = std::max(i_table_size, apc_other.i_table_size),
        i_bigger_index = c_bigger.i_table_size,
        i_smaller_index = c_smaller.i_table_size,
        i_res_index = i_res_size,
        i_debt = 0,
        i_top_digit,
        i_bottom_digit;
    
    delete[] c_result.itp_table;
    c_result.itp_table = new int[i_res_size];
    c_result.i_table_size = i_res_size;
    c_result.b_sign = (*this >= apc_other ? true:false);

    while(i_res_index--){
        i_bigger_index--;
        i_smaller_index--;

        i_top_digit = c_bigger.itp_table[i_bigger_index];
        i_bottom_digit = i_smaller_index >= 0 ? c_smaller.itp_table[i_smaller_index] : 0;

        if((i_bottom_digit + i_debt > i_top_digit) || (i_top_digit < i_bottom_digit) ){
            i_top_digit += i_sys_base - i_debt;
            i_debt = 1;
        }
        else if(i_bottom_digit + i_debt <= i_top_digit){
            i_top_digit -= i_debt;
            i_debt = 0;
        }

        c_result.itp_table[i_res_index] = i_top_digit - i_bottom_digit;
        //std::cout << i_res_index;
    }

    c_result.normalize();

    return c_result;
}

CNumber CNumber::operator-(int ia_value) const {
    CNumber c_temp;
    c_temp = ia_value;
    return *this - c_temp;
}

bool CNumber::operator>(const CNumber &apc_other) const {
    if(*this==apc_other) return false;

    bool b_res = true;
    if(!b_sign && !apc_other.b_sign){
        b_res = false;
    }

    if(i_table_size > apc_other.i_table_size) return b_res;
    else if(i_table_size < apc_other.i_table_size) return !b_res;
    else if(b_sign && !apc_other.b_sign) return true;
    else if(!b_sign && apc_other.b_sign) return false;

    for(int i = 0; i < i_table_size; ++i){
        if(itp_table[i] > apc_other.itp_table[i]) return b_res;
        else if(itp_table[i] < apc_other.itp_table[i]) return !b_res;
    }
    return !b_res;
}

CNumber CNumber::operator*(const CNumber &apc_other) const {
    CNumber c_result;
    std::vector<CNumber> cv_to_sum;

    int i_carry,
        i_temp,
        i_res_size = 2*std::max(i_table_size, apc_other.i_table_size),
        i_res_index = i_res_size;

    for(int i = i_table_size-1; i >-1; --i){
        i_carry = 0;

        delete[] c_result.itp_table;
        c_result.itp_table = new int[i_res_size]();
        c_result.i_table_size = i_res_size;
        i_res_index = i_res_size;

        for(int j = apc_other.i_table_size-1; j > -1; --j){
            i_res_index--;

            i_temp = itp_table[i] * apc_other.itp_table[j] + i_carry;
            c_result.itp_table[i_res_index] = i_temp % i_sys_base;
            i_carry = i_temp / i_sys_base;
        }
        if(i_res_index--) c_result.itp_table[i_res_index] = i_carry;
        cv_to_sum.push_back(c_result);
    }

    delete[] c_result.itp_table;
    c_result.itp_table = new int[i_res_size]();

    for(long unsigned int i = 0; i < cv_to_sum.size(); ++i){
        //std::cout<<c_result.sToString() << ' ' << cv_to_sum[i].sToString() << " elo\n";
        c_result = c_result + cv_to_sum[i].mult10(i);
    }
    c_result.b_sign = !(b_sign^apc_other.b_sign);
    c_result.normalize();
    return c_result;
}

CNumber CNumber::operator*(int ia_value) const{
    CNumber c_temp;
    c_temp = ia_value;
    return *this * c_temp;
}

CNumber CNumber::operator/(const CNumber &apc_other) const {
    CNumber mid;
    if(abs(*this) < abs(apc_other)){
        mid = 0;
        return mid;
    }
    else if(*this == apc_other){
        mid = 1;
        return mid;
    }

    CNumber left, right, c_temp;
    left = 1;
    right = abs(*this);
    mid = 0;

    //tylko dlatego to robie bo zabraniaja breaka uzywac(???)
    //normalnie daje sie breaka w petli gdy mid == target
    bool b_guard = true;

    while(left < right-1 && b_guard){
        mid = halve(right + left);
        c_temp = mid*abs(apc_other);
        if(c_temp > abs(*this)) right = mid-1;
        else if(c_temp < abs(*this)) left = mid+1;
        else{
            right = mid;
            b_guard = false;
        }
    }
    //korekta
    if(right * abs(apc_other) > abs(*this)){
        c_temp = right * abs(apc_other);
        while( c_temp > abs(*this)){
            right = right -1;
            c_temp = right * abs(apc_other);
        }
    }
    right.b_sign = !(b_sign ^ apc_other.b_sign);
    return right;
}

CNumber CNumber::operator/(int ia_value) const{
    CNumber c_temp;
    c_temp = ia_value;
    return *this / c_temp;
}

CNumber CNumber::operator%(const CNumber &apc_other) const{
    CNumber c_result;
    c_result = *this/apc_other;
    return *this - (c_result * apc_other);
}

CNumber CNumber::operator%(int ia_value) const{
    CNumber c_temp;
    c_temp = ia_value;
    return *this % c_temp;
}

void CNumber::operator^=(const int ia_value){
    CNumber c_result;
    c_result.i_sys_base = 4;
    c_result = ia_value;
    *this = c_result;
}

bool CNumber::operator<(const CNumber &apc_other) const {
    if(*this == apc_other) return false;
    return !(*this > apc_other);
}

bool CNumber::operator>=(const CNumber &apc_other) const {
    return *this == apc_other || *this > apc_other;
}

bool CNumber::operator<=(const CNumber &apc_other) const {
    return *this == apc_other || *this < apc_other;
}

bool CNumber::operator==(const CNumber &apc_other) const {
    if(i_table_size != apc_other.i_table_size) return false;
    else if(b_sign != apc_other.b_sign) return false;

    for(int i = 0; i < i_table_size; ++i){
        if(itp_table[i] != apc_other.itp_table[i]) return false;
    }
    return true;
}

bool CNumber::operator==(const int ia_value) const{
    CNumber c_temp;
    c_temp = ia_value;
    return *this == c_temp;
}

CNumber CNumber::abs(const CNumber &apc_other) const {
    CNumber c_result;
    c_result = apc_other;
    c_result.b_sign = true;
    return c_result;
}

std::string CNumber::sToString() const{
    std::string s_result = "";
    if(!b_sign) s_result = "-";
    for(int i = 0; i < i_table_size; ++i){
        s_result += (char)itp_table[i] + 48;
        // std::cout << itp_table[i] << '\n';
    }
    return s_result;
}

void CNumber::normalize(){
    if(itp_table[0] || i_table_size == 1) return;

    int *itp_temp_table = new int[i_table_size -1];
    for(int i = 0; i < i_table_size-1; ++i){
        itp_temp_table[i] = itp_table[i+1];
    }
    delete[] itp_table;
    itp_table = itp_temp_table;
    i_table_size -= 1;

    itp_temp_table = NULL;
    normalize();
}

//to jest tak naprawde dodawanie ilus zer na koniec
CNumber CNumber::mult10(int i_exponent) const {
    CNumber c_result;
    c_result = *this;
    if(!i_exponent) return c_result;

    int *itp_temp_table = new int[i_table_size + i_exponent]();
    for(int i = 0 ; i < i_table_size; ++i) itp_temp_table[i] = c_result.itp_table[i];

    delete[] c_result.itp_table;
    c_result.itp_table = itp_temp_table;
    c_result.i_table_size += i_exponent;

    itp_temp_table = NULL;
    return c_result;
}

CNumber CNumber::halve(const CNumber &apc_other) const{
    CNumber c_result;
    c_result.itp_table = new int[apc_other.i_table_size]();
    c_result.i_table_size = apc_other.i_table_size;

    int i_carry=0,
        i_sum = 0,
        i_res_index = 0;

    for(int i = 0; i < apc_other.i_table_size; ++i){
        i_sum = i_carry + apc_other.itp_table[i];
        c_result.itp_table[i_res_index] = i_sum / I_HALVER;
        i_carry = (i_sum % I_HALVER) * i_sys_base;
        i_res_index++;
    }

    i_carry/=i_sys_base;
    c_result.normalize();
    return c_result;
}
