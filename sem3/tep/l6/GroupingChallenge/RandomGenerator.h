#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#include <random>

template<typename T>
class CRandomGenerator{
    public:
        CRandomGenerator(const T i_lower, const T i_upper);
        ~CRandomGenerator();
        static std::mt19937 c_random_engine;
};

template<>
class CRandomGenerator<int>{
    public:
        CRandomGenerator(const int i_lower, const int i_upper);
        int i_get_next();

    private:
        static std::mt19937 c_random_engine;
        std::uniform_int_distribution<int>c_distribution;
        
};

template<>
class CRandomGenerator<double> {
public:
    CRandomGenerator(const double d_lower, const double d_upper);
    double d_get_next();

private:
    static std::mt19937 c_random_engine;
    std::uniform_real_distribution<double> c_distribution;
};

#endif
