#include "RandomGenerator.h"

template<typename T>
std::mt19937 CRandomGenerator<T>::c_random_engine(std::random_device{}());

std::mt19937 CRandomGenerator<int>::c_random_engine(std::random_device{}());
CRandomGenerator<int>::CRandomGenerator(const int i_lower, const int i_upper)
    : c_distribution(i_lower, i_upper)
{
}

int CRandomGenerator<int>::i_get_next() {
    return c_distribution(c_random_engine);
}

std::mt19937 CRandomGenerator<double>::c_random_engine(std::random_device{}());
CRandomGenerator<double>::CRandomGenerator(const double d_lower, const double d_upper)
    : c_distribution(d_lower, d_upper)
{
    c_random_engine.seed(std::random_device{}());
}

double CRandomGenerator<double>::d_get_next() {
    return c_distribution(c_random_engine);
}

