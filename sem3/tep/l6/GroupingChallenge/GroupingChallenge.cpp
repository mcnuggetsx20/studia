#include "GaussianGroupingEvaluatorFactory.h"
#include "GeneticAlgorithm.h"
#include "GroupingEvaluator.h"
#include <iostream>

using namespace NGroupingChallenge;

int main()
{
	CGaussianGroupingEvaluatorFactory c_evaluator_factory(5, 100, 1);

	c_evaluator_factory
		/* .cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0) */
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0)
		.cAddDimension(-100, 100, 1.0, 1.0);

	CGroupingEvaluator* pc_evaluator = c_evaluator_factory.pcCreateEvaluator(0);

    CGeneticAlgorithm c_genetic_algorithm_instance(*pc_evaluator);
    c_genetic_algorithm_instance.v_set_number_of_iterations(50);
    c_genetic_algorithm_instance.v_set_mut_prob(0.15);
    c_genetic_algorithm_instance.v_set_cross_prob(0.6);
    c_genetic_algorithm_instance.v_set_pop_size(100);

    c_genetic_algorithm_instance.v_iterate();
    cout << c_genetic_algorithm_instance.c_get_best_individual().d_get_evaluation();

	delete pc_evaluator;

	return 0;
}
