#ifndef GENETIC_ALGORITHM_H
#define GENETIC_ALGORITHM_H

#include "GroupingEvaluator.h"
#include "Individual.h"
#include "OptimizationResult.h"

namespace NGroupingChallenge{

    class CGeneticAlgorithm{
        public:
            CGeneticAlgorithm(CGroupingEvaluator& ac_evaluator);
            ~CGeneticAlgorithm();

            void v_next_generation();

            void v_iterate();

            COptimizationResult c_get_best_individual() const;

            void v_set_pop_size(int i_new_pop_size);
            void v_set_cross_prob(double d_new_cross_prob);
            void v_set_mut_prob(double d_new_mut_prob);
            void v_set_number_of_iterations(int i_iterations);
            
        private:
            double d_current_best_fitness;
            CIndividual c_current_best;
            std::vector<CIndividual> vc_individuals;
            CGroupingEvaluator& c_evaluator;

            void v_initialize_individuals();
            void v_initialize_mod();
            void v_update_best_fitness();

            void v_mutate_population(std::vector<CIndividual>& vc_population);
            int i_choose_random_parent_index();

            int i_POP_SIZE;
            double d_CROSS_PROB;
            double d_MUT_PROB;
            int i_NUMBER_OF_ITERATIONS;
            
            static const int i_MIN_POP_SIZE;
            static const double d_MIN_CROSS_PROB;
            static const double d_MIN_MUT_PROB;
            static const int i_MIN_NUMBER_OF_ITERATIONS;
            static const int i_INITIAL_CANDIDATES = 3;

    };
}

#endif
