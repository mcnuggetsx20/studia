#include "GeneticAlgorithm.h"
#include "Individual.h"
#include "RandomGenerator.h"
#include <cfloat>
#include <iostream>

using namespace NGroupingChallenge;
#define dbgn(n) std::cout << n << '\n';

const int CGeneticAlgorithm::i_MIN_POP_SIZE = 1;
const double CGeneticAlgorithm::d_MIN_CROSS_PROB = 0.2;
const double CGeneticAlgorithm::d_MIN_MUT_PROB = 0.02;
const int CGeneticAlgorithm::i_MIN_NUMBER_OF_ITERATIONS = 10;

CGeneticAlgorithm::CGeneticAlgorithm(CGroupingEvaluator& ac_evaluator) 
    :c_evaluator(ac_evaluator)
{
    c_current_best = CIndividual();
    d_current_best_fitness = std::numeric_limits<double>::max();

    v_set_pop_size(i_MIN_POP_SIZE);
    v_set_number_of_iterations(i_MIN_NUMBER_OF_ITERATIONS);
    v_set_mut_prob(d_MIN_MUT_PROB);
    v_set_cross_prob(d_CROSS_PROB);

    return;
}

CGeneticAlgorithm::~CGeneticAlgorithm(){
    vc_individuals.clear();
    return;
}

void CGeneticAlgorithm::v_next_generation(){

    CRandomGenerator<double> c_chance_generator(0.0, 1.0);
    CRandomGenerator<int> c_random_cut_generator(1, c_evaluator.iGetNumberOfPoints()-1);

    std::vector<CIndividual> vc_next_generation;

    int i_parent1,
        i_parent2,
        i_number_of_parents,
        i_gene_cut_position;
    
    double d_fitness1, d_fitness2;

    while(vc_next_generation.size() < vc_individuals.size()){

        i_number_of_parents = 2;
        i_gene_cut_position = c_random_cut_generator.i_get_next();

        // WYBOR RODZICOW
        i_parent1 = i_choose_random_parent_index();
        i_parent2 = i_choose_random_parent_index();
        ////////////////
        
        //KRZYZOWANIE
        if(c_chance_generator.d_get_next() <= d_CROSS_PROB){ //zachodzi krzyzowanie
            vc_next_generation.push_back(CIndividual(
                        vc_individuals[i_parent1], 
                        vc_individuals[i_parent2], 
                        i_gene_cut_position
            ));

            vc_next_generation.push_back(CIndividual(
                    vc_individuals[i_parent2], 
                    vc_individuals[i_parent1], 
                    i_gene_cut_position
            ));
        }
        else{ //do nastepnego pokolenia przechodza kopie rodzicow
            vc_next_generation.push_back(CIndividual(vc_individuals[i_parent1]));
            vc_next_generation.push_back(CIndividual(vc_individuals[i_parent2]));
        }
        /////////////////
    }

    v_mutate_population(vc_next_generation);

    vc_individuals = vc_next_generation;
    v_update_best_fitness();
    
    return;
}

void CGeneticAlgorithm::v_iterate(){
    // v_initialize_individuals();
    v_initialize_mod();
    for(int i = 0 ; i < i_NUMBER_OF_ITERATIONS; ++i){
        // int i_percent = (i + 1) * 100 / i_NUMBER_OF_ITERATIONS;
        // std::cout << "\rGenerations evolving: " << i_percent << "%" << std::flush;
        std::cout << "\rGenerations evolving: " << i+1 << "/" << i_NUMBER_OF_ITERATIONS <<  std::flush;
        v_next_generation();
    }
    std::cout << "\r" << std::string(50, ' ') << "\rDone!\n" << std::flush;
    // v_print_best_fitness();
}

void CGeneticAlgorithm::v_initialize_individuals(){
    for(int i = 0; i < i_POP_SIZE; ++i){
        vc_individuals.push_back(
            CIndividual(
                c_evaluator.vGetPoints(), 
                c_evaluator.iGetNumberOfGroups()
            )
        );
    }
    return;
}

void CGeneticAlgorithm::v_initialize_mod(){
    CIndividual c_temp, c_best;
    double d_best_fit;

    c_current_best = CIndividual();
    d_current_best_fitness = std::numeric_limits<double>::max();

    for(int i = 0; i < i_POP_SIZE; ++i){
        d_best_fit = std::numeric_limits<double>::max();
        for(int j = 0; j < i_INITIAL_CANDIDATES; ++j){

            c_temp  =
                CIndividual(
                    c_evaluator.vGetPoints(), 
                    c_evaluator.iGetNumberOfGroups()
            );
            if(c_temp.d_get_evaluation(c_evaluator) < d_best_fit){
                c_best = c_temp;
                d_best_fit = c_temp.d_get_evaluation(c_evaluator);
            }
        }
        vc_individuals.push_back(c_best);
        // std::cout << c_best << endl;
    }
    return;

}

void CGeneticAlgorithm::v_update_best_fitness(){
    //ZBIERAMY OSOBNIKA O NAJLEPSZY DOPASOWANIU
    double d_temp_fitnes;

    for(unsigned int i =0 ; i < vc_individuals.size(); ++i){
        d_temp_fitnes = vc_individuals[i].d_get_evaluation(c_evaluator);
        if(d_temp_fitnes < d_current_best_fitness){
            d_current_best_fitness = d_temp_fitnes;
            c_current_best = vc_individuals[i];
        }
    }
    ////////////////
}

int CGeneticAlgorithm::i_choose_random_parent_index(){
    CRandomGenerator<int> c_generator(0, (int)vc_individuals.size()-1);

    int i_candidate1 = c_generator.i_get_next();
    int i_candidate2 = c_generator.i_get_next();

    double d_fitness1 = vc_individuals[i_candidate1].d_get_evaluation(c_evaluator);
    double d_fitness2 = vc_individuals[i_candidate2].d_get_evaluation(c_evaluator);

    return d_fitness1 < d_fitness2 ? i_candidate1 : i_candidate2;
}

void CGeneticAlgorithm::v_mutate_population(std::vector<CIndividual>& vc_population){
    CRandomGenerator<double> c_chance_generator(0.0, 1.0);
    CRandomGenerator<int> c_random_group_generator(1, c_evaluator.iGetNumberOfGroups());

    for(unsigned int i = 0; i < vc_population.size(); ++i){
        for(int j = 0 ; j < c_evaluator.iGetNumberOfPoints(); ++j){
            if(c_chance_generator.d_get_next() <= d_MUT_PROB){
                vc_population[i].v_mutate(j, c_random_group_generator.i_get_next());
            }
        }
    }
    return;
}

void CGeneticAlgorithm::v_set_number_of_iterations(int i_iterations){
    i_NUMBER_OF_ITERATIONS = std::max(i_MIN_NUMBER_OF_ITERATIONS, i_iterations);
}
void CGeneticAlgorithm::v_set_pop_size(int i_new_pop_size){
    i_POP_SIZE = std::max(i_new_pop_size, i_MIN_POP_SIZE);
}
void CGeneticAlgorithm::v_set_cross_prob(double d_new_cross_prob){
    d_CROSS_PROB = std::max(d_MIN_CROSS_PROB, d_new_cross_prob);
}
void CGeneticAlgorithm::v_set_mut_prob(double d_new_mut_prob){
    d_MUT_PROB = std::max(d_MIN_MUT_PROB, d_new_mut_prob);
}

COptimizationResult CGeneticAlgorithm::c_get_best_individual() const{
    return COptimizationResult(c_current_best, c_evaluator);
}


