#include "OptimizationResult.h"

COptimizationResult::COptimizationResult(CIndividual ac_ind, CGroupingEvaluator& ac_evaluator){
    itp_table = new int[ac_evaluator.iGetNumberOfPoints()];
    for(int i = 0; i< ac_evaluator.iGetNumberOfPoints(); ++i){
        itp_table[i] = ac_ind.itp_get_group_table()[i];
    }

    d_fitness = ac_ind.d_get_evaluation(ac_evaluator);
    return;
}

COptimizationResult::~COptimizationResult(){
    if(itp_table != NULL) delete itp_table;
    return;
}

double COptimizationResult::d_get_evaluation() const{
    return d_fitness;
}

