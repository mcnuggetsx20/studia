#include "Individual.h"
#include "GroupingEvaluator.h"
#include "Point.h"
#include <iostream>
#include <string>
#include <vector>

#include "RandomGenerator.h"

using namespace NGroupingChallenge;

CIndividual::CIndividual(){
    itp_group_assignment = new int[1];
    i_number_of_points = 1;
}

CIndividual::CIndividual(const CIndividual &ac_other){
    i_number_of_points = ac_other.i_number_of_points;
    itp_group_assignment = new int[i_number_of_points];

    for(int i = 0 ; i < i_number_of_points; ++i){
        itp_group_assignment[i] = ac_other.itp_group_assignment[i];
    }
}

CIndividual::CIndividual(const std::vector <CPoint>& av_points, const int ai_number_of_groups){
    CRandomGenerator<int> c_generator(1, ai_number_of_groups);

    itp_group_assignment = new int[av_points.size()];
    i_number_of_points = av_points.size();

    for(int i = 0; i < i_number_of_points; ++i){
        itp_group_assignment[i] = c_generator.i_get_next();
    }

    return;
}

CIndividual::CIndividual(const CIndividual& c_parent1, const CIndividual& c_parent2, const int& ai_gene_cut_position){
    if(c_parent1.i_number_of_points != c_parent2.i_number_of_points){
        itp_group_assignment = new int[1];
        i_number_of_points = 1;
        return;
    }

    i_number_of_points = c_parent1.i_number_of_points;
    itp_group_assignment = new int[i_number_of_points];

    //pierwszy segment rodzica 1
    for(int i = 0; i < ai_gene_cut_position; ++i){
        itp_group_assignment[i] = c_parent1.itp_group_assignment[i];
    }

    //drugi segment rodzica 2
    for(int i = ai_gene_cut_position; i < i_number_of_points; ++i){
        itp_group_assignment[i] = c_parent2.itp_group_assignment[i];
    }

    return;
}

CIndividual::~CIndividual(){
    delete[] itp_group_assignment;
    itp_group_assignment = NULL;
    return;
}

double CIndividual::d_get_evaluation(CGroupingEvaluator &ac_evaluator) const{
    return ac_evaluator.dEvaluate(itp_group_assignment);
}

void CIndividual::v_mutate(const int i_index, const int i_new_value){
    itp_group_assignment[i_index] = i_new_value;
    return;
}

CIndividual& CIndividual::operator=(CIndividual ac_other){
    v_swap_with(ac_other);
    return *this;
}

void CIndividual::v_swap_with(CIndividual& ac_other){
    std::swap(itp_group_assignment, ac_other.itp_group_assignment);
    std::swap(i_number_of_points, ac_other.i_number_of_points);
    return;
}

namespace NGroupingChallenge {
    std::ostream& operator<<(std::ostream& os, const CIndividual& obj) {
        os << obj.s_to_string();
        return os;
    }
}

std::string CIndividual::s_to_string() const {
    std::string res = "";
    for(int i = 0; i < i_number_of_points; ++i){
        res += to_string(itp_group_assignment[i]) + ' ';
    }
    return res;
}
