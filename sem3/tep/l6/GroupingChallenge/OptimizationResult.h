#ifndef OPTIMIZATIONRESULT_H
#define OPTIMIZATIONRESULT_H

#include "Individual.h"
#include "GroupingEvaluator.h"

using namespace NGroupingChallenge;
class COptimizationResult{
    public:
        COptimizationResult(CIndividual ac_ind, CGroupingEvaluator& ac_evaluator);
        ~COptimizationResult();

        const int* i_get_table() const{return itp_table;};
        double d_get_evaluation() const;



    private:
        int* itp_table;
        double d_fitness;
};

#endif
