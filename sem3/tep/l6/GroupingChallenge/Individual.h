#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include "Point.h"
#include "GroupingEvaluator.h"
#include <ostream>
#include <vector>

namespace NGroupingChallenge{
    class CIndividual {
    public:
        CIndividual();
        CIndividual(const std::vector<CPoint>& av_points, const int ai_number_of_groups);
        CIndividual(const CIndividual& c_parent1, const CIndividual& c_parent2, const int& i_gene_cut_pos);
        CIndividual(const CIndividual &ac_other);
        CIndividual &operator=(CIndividual c_other);
        /* CIndividual(CIndividual &&) = default;
        CIndividual &operator=(CIndividual &&) = default; */
        ~CIndividual();

        std::string s_to_string() const;
        const int* itp_get_group_table() const {return itp_group_assignment;}
        double d_get_evaluation(CGroupingEvaluator &ac_evaluator) const;
        void v_mutate(const int i_index, const int i_new_value);

    private:
        int* itp_group_assignment;
        int i_number_of_points;

        void v_swap_with(CIndividual& ac_other);
    };
    std::ostream& operator<<(std::ostream& os, const CIndividual& obj);
}

#endif // ifndef INDIVIDUAL
