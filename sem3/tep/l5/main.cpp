#include <iostream>
#include <string>
#include <vector>
#include "CMySmartPointer.h"
#include "CTree/CTree.h"

using namespace std;

int main(){
    /* CTree t1(vector<string>{"+", "a", "b"}), 
          t2(vector<string>{"*", "c", "10"});

    CTree t3 = t1+t2;
    cout << t3 << '\n';

    CTree t4(vector<string>{"+", "a", "b"}), 
          t5(vector<string>{"*", "c", "10"});

    CTree t6 = t4+t5;
    cout << t6 << '\n';

    cout << t6 + t1 << '\n';
    cout << t1 << '\n';

    printf("Licznik kopii: %d\nLicznik przeniesien: %d\n", 
        *CTree::i_copy_counter, 
        *CTree::i_move_counter
    ); */

    CMySmartPointer<int> c_ptr(new int(12));
    cout << *c_ptr << endl;
    c_ptr = 30;
    cout << *c_ptr << endl;
    int i_val = 20;
    c_ptr = i_val;
    cout << *c_ptr << endl;

    CMySmartPointer<int> c_copy(c_ptr);
    c_ptr = 21;
    cout << *c_ptr << endl;
    c_ptr = i_val;
    cout << *c_ptr << endl;


    return 0;
}


