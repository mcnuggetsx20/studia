#ifndef CREFCOUNTER_H
#define CREFCOUNTER_H

class CRefCounter{
    public:
        CRefCounter() { i_count = 0; }

        int i_inc(){return ++i_count;}
        int i_dec(){return --i_count;}

        int i_get(){return i_count;}
    private:
        int i_count;
};

#endif // !CREFCOUNTER_H
