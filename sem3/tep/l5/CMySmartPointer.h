#ifndef CMYSMARTPOINTER_H
#define CMYSMARTPOINTER_H

#include "CRefCounter.h"
#include <iostream>

template<typename T>
class CMySmartPointer{
    public:
        CMySmartPointer(T *apc_pointer);
        CMySmartPointer(const CMySmartPointer<T> &apc_other);
         ~CMySmartPointer();
        T& operator*(){ return *pc_pointer; }
        T* operator->(){ return pc_pointer; }

        CMySmartPointer<T>& operator=(const T& ac_other);
        CMySmartPointer<T>& operator=(T&& appc_other);
        CMySmartPointer<T>& operator=(const CMySmartPointer<T>& ac_other);

    private:
        T* pc_pointer;
        CRefCounter* pc_counter;

        void v_copy_from(const CMySmartPointer<T> &ac_other);
        void v_check_and_delete();
};

template<typename T>
CMySmartPointer<T>::CMySmartPointer(T* apc_pointer){
    pc_pointer = apc_pointer;
    pc_counter = new CRefCounter();
    pc_counter -> i_inc();
    return;
}

template<typename T>
CMySmartPointer<T>::CMySmartPointer(const CMySmartPointer<T> &apc_other){
    pc_pointer = NULL;
    pc_counter = NULL;
    v_copy_from(apc_other);
    return;
}

template<typename T>
CMySmartPointer<T>::~CMySmartPointer(){
    v_check_and_delete();
    return;
}

template<typename T>
CMySmartPointer<T>& CMySmartPointer<T>::operator=(const CMySmartPointer<T>& ac_other){
    v_copy_from(ac_other);
    return *this;
}

template<typename T>
CMySmartPointer<T>& CMySmartPointer<T>::operator=(const T& ac_other){
    if(pc_counter -> i_get() %2 == 1) return *this;
   *pc_pointer = ac_other;
    return *this;
}

template<typename T>
CMySmartPointer<T>& CMySmartPointer<T>::operator=(T&& appc_other){
    if(pc_counter->i_get() % 2 ==0 ) return *this;
    *pc_pointer = appc_other;
    return *this;
}

//////////////////////////////////////////////////////
//                      PRIVATE
//////////////////////////////////////////////////////
template<typename T>
void CMySmartPointer<T>::v_copy_from(const CMySmartPointer<T> &ac_other){
    v_check_and_delete();
    pc_pointer = ac_other.pc_pointer;
    pc_counter = ac_other.pc_counter;
    pc_counter -> i_inc();
    return;
}

template<typename T>
void CMySmartPointer<T>::v_check_and_delete(){
    if(pc_counter == NULL) return;
    else if(pc_counter -> i_dec() == 0){
        delete pc_pointer;
        delete pc_counter;
    }
    return;
}


#endif
