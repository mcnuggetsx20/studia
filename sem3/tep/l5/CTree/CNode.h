#ifndef CNODE_H
#define CNODE_H

#include <iostream>
#include <vector>
#include <map>

typedef std::map<std::string, std::pair<int, double (*)(const std::vector<double>&) > > cnode_map_type;

class CNode{
    public:
        CNode(const std::string &asc_tokens);
        CNode(const CNode &apc_other);
        ~CNode();

        CNode& operator=(CNode& ac_other);

        friend class CTree;
        friend class CTreeCLI;
        friend std::ostream& operator<<(std::ostream& os, const CNode& obj);
        std::string s_to_string() const;

    private:
        int i_max_children,
            i_curr_children,
            i_type;

        std::string s_text;
        CNode **ctp_children;

        CNode();

        void v_add_child(const std::string &asc_tokens);
        void v_init_fields(int ai_curr_children, int ai_max_children, const std::string &as_text);

        int i_find_num_children(const std::string &asc_tokens);

        bool b_is_true() const;
        bool b_is_number() const;
        bool b_is_operator() const;
        bool b_is_variable() const;
        bool b_is_leaf() const;

        static cnode_map_type map_init_operators();
        static double d_str_to_double(const std::string &as_text);
        static char ch_get_valid_char(const char &ach_token);
        static int i_determine_type(const std::string &asc_tokens);
        static void v_copy_all_children(CNode *acp_parent, const CNode *acp_node);

        static double d_add(const std::vector<double> &avd_comps);
        static double d_subtract(const std::vector<double> &avd_comps);
        static double d_multiply(const std::vector<double> &avd_comps);
        static double d_divide(const std::vector<double> &avd_comps);
        static double d_sin(const std::vector<double> &avd_comps);
        static double d_cos(const std::vector<double> &avd_comps);

        static const int I_OPERATOR_CODE = 2;
        static const int I_VARIABLE_CODE = 1;
        static const int I_NUMBER_CODE = 0;
        static const int I_OPERATOR_NOT_FOUND = -712;
        static const int I_ROOT_MAX_CHILDREN = 1;

        static const cnode_map_type V_OPERATOR_MAP;
        static const std::string S_DEFAULT_CHILD;
};

#endif
