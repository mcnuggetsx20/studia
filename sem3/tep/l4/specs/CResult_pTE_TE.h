#ifndef CRESULT_PTE_TE_H
#define CRESULT_PTE_TE_H

#include <iostream>

#include "../CResult.h"

template <typename TE>
class CResult<TE*, TE>{
    public:
        CResult(const CResult<TE*, TE>& ac_other);
        CResult(const TE* ac_other, bool ok);

        ~CResult();

        static CResult<TE*, TE> c_ok(TE* apc_value);
        static CResult<TE*, TE> c_fail(TE* apc_error);
        static CResult<TE*, TE> c_fail(std::vector<TE*>& av_errors);

        CResult<TE*, TE>& operator=(const CResult<TE*, TE>& ac_ther);

        bool b_is_success() const;

        std::vector<TE*>& v_get_errors();

        void v_evaluate();

    private:
        std::vector<TE*> v_errors;
        TE *pc_value;
};

template <typename TE>
CResult<TE*, TE>::CResult(const CResult<TE*, TE> &ac_other){
    pc_value = new TE(*ac_other.pc_value);
    for(unsigned int i = 0; i < ac_other.v_errors.size(); ++i){
        v_errors.push_back(new TE(*ac_other.v_errors[i]));
    }
}

template <typename TE>
CResult<TE*, TE>::CResult(const TE* ac_other, bool ok){
    if(ok){
        pc_value = new TE(*ac_other);
        return;
    }
    pc_value = NULL;
    v_errors.push_back(new TE(*ac_other));
}

template<typename TE>
CResult<TE*, TE>::~CResult(){
    pc_value = NULL;
    for(unsigned int i = 0 ; i < v_errors.size(); ++i){
        if(v_errors[i] != NULL) delete v_errors[i];
    }
    v_errors.clear();
    return;
}

template <typename TE>
CResult<TE*, TE>& CResult<TE*, TE>::operator=(const CResult<TE*, TE>& ac_other){
    CResult<TE*, TE> cr_temp(ac_other);
    return cr_temp;
}

template <typename TE>
bool CResult<TE*, TE>::b_is_success() const{
    return !v_errors.size();
}

template <typename TE>
std::vector<TE*>& CResult<TE*, TE>::v_get_errors(){
    return v_errors;
}

template<typename TE>
CResult<TE*, TE> CResult<TE*, TE>::c_ok(TE* ac_value){
    return CResult<TE*, TE>(ac_value, true);
}

template<typename TE>
CResult<TE*, TE> CResult<TE*,TE>::c_fail(TE* pc_error){
    return CResult<TE*, TE>(pc_error, false);
}

template<typename TE>
CResult<TE*, TE> CResult<TE*,TE>::c_fail(std::vector<TE*>& v_errors){
    return CResult<TE*,TE>(v_errors, false);
}

template<typename TE>
void CResult<TE*,TE>::v_evaluate(){
    if(b_is_success()){
        std::cout << *pc_value << '\n';
    }
    else{
        for(unsigned int i = 0; i < v_get_errors().size(); ++i){
            std::cout << *(v_get_errors()[i]) << '\n';
        }
        return;
    }
    return;
}

#endif
