#ifndef CRESULT_VOID_E_H
#define CRESULT_VOID_E_H

#include <iostream>
#include <vector>
#include "../CResult.h"

template <typename E>
class CResult<void, E>{
    public:
        CResult();
        CResult(E *pcError);
        CResult(std::vector<E*>& vErrors);
        CResult(const CResult<void, E>& cOther);

        ~CResult();

        static CResult<void, E> cOk();
        static CResult<void, E> cFail(E* pcError);
        static CResult<void, E> cFail(std::vector<E*>& vErrors);

        CResult<void, E>& operator=(const CResult<void, E>& cOther);

        bool b_is_success() const;

        std::vector<E*>& v_get_errors();

    private:
        std::vector<E*> v_errors;
};

//success
template<typename E> //wymagany kopiujacy w T
CResult<void, E>::CResult(){
    return;
}

//error
template<typename E> //wymagany kopiujacy w E
CResult<void, E>::CResult(E* apc_error){
    v_errors.push_back(new E(*(apc_error)));
    return;
}

//multiple errors
template<typename E> //wymagany kopiujacy w E
CResult<void, E>::CResult(std::vector<E*> &avErrors){
    for(int i = 0 ; i < avErrors.size(); ++i){
        v_errors.push_back(new E(*(avErrors[i])));
    }
    return;
}

//copy constructor
template<typename E> //wymagany copy w E
CResult<void, E>::CResult(const CResult<void, E>& ac_other){
    for(int i = 0 ; i < ac_other.v_errors.size(); ++i){
        v_errors.push_back(new E(*(ac_other.v_errors[i])));
    }
    return;
}

//destructor
template<typename E>
CResult<void, E>::~CResult(){
    v_errors.clear();
    return;
}

template<typename E>
static CResult<void, E> cOk(){
    return CResult<void, E>();
}

template<typename E>
static CResult<void, E> cFail(E* pc_error){
    return CResult<void, E>(*pc_error);
}

template<typename E>
static CResult<void, E> cFail(std::vector<E*>& vErrors){
    return CResult<void, E>(vErrors);
}

template<typename E>
CResult<void, E>& CResult<void,E>::operator=(const CResult<void, E> &ac_other){
    return CResult<void,E>(ac_other);
}

template<typename E>
std::vector<E*>& CResult<void,E>::v_get_errors(){
    return v_errors;
}

template<typename E>
bool CResult<void,E>::b_is_success()const{
    return  !v_errors.size();
}

#endif
