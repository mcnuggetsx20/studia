#include "CError.h"

CError::CError(const std::string &as_error_message){
    s_error_message = as_error_message;
    return;
}

CError::CError(const CError &apc_other){
    s_error_message = apc_other.s_error_message;
}

std::ostream& operator<<(std::ostream& os, const CError& obj) {
    os << obj.s_error_message;
    return os;
}

CError::~CError(){
    return;
}
