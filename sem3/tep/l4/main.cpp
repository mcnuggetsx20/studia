#include <iostream>
#include <vector>

#include "CTree/CTree.h"
#include "CResultDumper.h"
#include "specs/CResult_pTE_TE.h"

using namespace std;

CResult<int*, int> c_test(int iX, int iY){
    if((iX + iY) % 2 == 0){
        return CResult<int*, int> :: c_fail(new int(21));
    }
    return CResult<int*, int> :: c_ok(new int(12));
}

int main(){

    CResult<CTree, CError> cr_res = CTree::cr_create_with_result("+.$");
    cr_res.v_evaluate();

    CResultDumper<CTree> cr_dumper;
    CResult<CResultDumper<CTree>, CError> cr_dump_res = cr_dumper.cr_dump(cr_res, "test");
    cr_dump_res.v_evaluate();


    CResult<int*, int> i_res = c_test(2, 3);
    i_res.v_evaluate();


    return 0;
}


