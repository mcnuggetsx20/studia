#ifndef CRESULTDUMPER_H
#define CRESULTDUMPER_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "CResult.h"
#include "CError.h"
#include "CTree/CTree.h"

template<typename T>
class CResultDumper{
    public:
        CResultDumper();
        ~CResultDumper();

        template<typename A>
        CResult<CResultDumper<T>, CError> cr_dump(CResult<A, CError>, const std::string &s_file_name);

        template<typename A>
        friend std::ostream& operator<<(std::ostream& os, const CResultDumper<A> &cr_obj);

        std::string s_to_string() const;

    private:
        int i_status;
        static const int I_STATUS_OPENED = 0;
        static const int I_STATUS_FAILED = 1;
        static const int I_STATUS_NEUTRAL = -1;

};

template<typename T>
CResultDumper<T>::CResultDumper(){
    i_status = I_STATUS_NEUTRAL;
    return;
}

template<typename T>
CResultDumper<T>::~CResultDumper(){
    return;
}

template<typename T>
template<typename A>
CResult<CResultDumper<T>, CError> CResultDumper<T>::cr_dump(const CResult<A, CError> cr_result, const std::string &as_file_name){
    const char *char_file_name = as_file_name.c_str();
    std::ofstream of_output_file(char_file_name, std::ios::out);

    if(!of_output_file.is_open()){
        i_status = I_STATUS_FAILED;
        return CResult<CResultDumper<T>, CError>::c_fail(new CError("nie udalo sie otworzyc pliku!"));
    }

    if(!cr_result.b_is_success()){
        for(unsigned int i = 0; i < cr_result.v_get_errors().size(); ++i){
            of_output_file << *(cr_result.v_get_errors()[i]) << '\n';
        }
    }

    return CResult<CResultDumper<T>, CError>::c_ok(*this);
}

template<>
template<typename A>
CResult<CResultDumper<CTree>, CError> CResultDumper<CTree>::cr_dump(CResult<A,CError> cr_result, const std::string &as_file_name){
    const char *char_file_name = as_file_name.c_str();
    std::ofstream of_output_file(char_file_name, std::ios::out);

    if(!of_output_file.is_open()){
        i_status = I_STATUS_FAILED;
        return CResult<CResultDumper<CTree>, CError>::c_fail(new CError("nie udalo sie otworzyc pliku!"));
    }

    if(!cr_result.b_is_success()){
        for(unsigned int i = 0; i < cr_result.v_get_errors().size(); ++i){
            of_output_file << *(cr_result.v_get_errors()[i]) << '\n';
        }
    }
    else{
        of_output_file << cr_result.c_get_value();
        of_output_file.close();
    }
    i_status = I_STATUS_OPENED;
    return CResult<CResultDumper<CTree>, CError>::c_ok(*this);
}

template<typename T>
std::string CResultDumper<T>::s_to_string()const{
    std::stringstream ss; 
    ss << "Dumper: " << i_status;
    return ss.str();
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const CResultDumper<T>& cr_obj) {
    os << cr_obj.s_to_string();
    return os;
}

#endif
