#ifndef CERROR_H
#define CERROR_H

#include<iostream>

class CError{
    public:
        CError(const std::string &as_error_message);
        CError(const CError &apc_other);
        ~CError();

        friend std::ostream& operator<<(std::ostream& os, const CError& obj);

        std::string s_error_message;
};

#endif
