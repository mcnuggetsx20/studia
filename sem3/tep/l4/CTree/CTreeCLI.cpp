#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <vector>

#include "CTreeCLI.h"
#include "CNode.h"
#include "CMessage.h"

const cli_map_type CTreeCLI::MAP_COMMANDS = v_init_map_commands();
const char* CTreeCLI::st_help_table[8][2] = {
    {"print", "wypisuje stan drzewa"},
    {"leaves", "wypisuje liczbe lisci"},
    {"vars", "wypisuje zmienne z drzewa"},
    {"enter", "<expr> tworzy drzewo z podanego wyrazenia"},
    {"join", "<expr> podlacza podane wyrazenie do istniejacego drzewa"},
    {"comp", "<vals> podaje wartosci dla zmiennych w kolejnosci wypisania vars"},
    {"exit", "konczy program"},
    {"help", "wyswietla pomoc"},
};
const std::string CTreeCLI::S_TREE_NOT_CREATED_MSG = "Nie utworzono jeszcze drzewa";

CTreeCLI::CTreeCLI(){
    ct_tree = NULL;
    b_running = true;
    return;
}

CTreeCLI::~CTreeCLI(){
    if(ct_tree==NULL) return;
    delete ct_tree;
}

void CTreeCLI::start(){
    std::string s_query;
    while(b_running){
        std::cout << "-> ";
        std::getline(std::cin, s_query);
        v_parse_query(s_query);
    }
}

void CTreeCLI::v_parse_query(std::string &as_query){
    unsigned int i_index = 0;
    sv_parsed_queries.clear();
    sv_parsed_queries.push_back("");

    for(; i_index < as_query.size(); ++i_index){
        if(as_query[i_index] == ' ') sv_parsed_queries.push_back("");
        while(i_index < as_query.size() && as_query[i_index] == ' ') ++i_index;
        sv_parsed_queries[sv_parsed_queries.size() -1] += as_query[i_index];
    }

    if(!sv_parsed_queries.size()){
        std::cout << "brak polecen\n";
        return;
    }

    cli_map_type::const_iterator map_iter = MAP_COMMANDS.find(sv_parsed_queries[0]);
    if(map_iter == MAP_COMMANDS.end()){
        std::cout << "error: niepoprawna komenda\n";
        return;
    }
    
    int i_target_arguments = map_iter -> second.first;
    if(i_target_arguments != (int)sv_parsed_queries.size()-1 && i_target_arguments != I_ARGUMENTS_UNDEFINED){
        std::cout << "error: zla liczba argumentow funkcji " << sv_parsed_queries[0] << '\n';
        return;
    }
    /* else if(i_target_arguments== I_ARGUMENTS_UNDEFINED && (int)sv_parsed_queries.size()-1 == 0){
        std::cout << "nie podano argumentow\n";
        return;
    } */
    (this ->* (map_iter -> second.second))();
    if(ct_tree != NULL) std::cout << ct_tree->msg_messages;
}

cli_map_type CTreeCLI::v_init_map_commands(){
    cli_map_type map_result;
    map_result["print"] = std::make_pair(0, &CTreeCLI::v_print);
    map_result["leaves"] = std::make_pair(0, &CTreeCLI::v_print_leaves);
    map_result["vars"] = std::make_pair(0, &CTreeCLI::v_print_vars);
    map_result["comp"] = std::make_pair(I_ARGUMENTS_UNDEFINED, &CTreeCLI::v_comp);
    map_result["enter"] = std::make_pair(I_ARGUMENTS_UNDEFINED, &CTreeCLI::v_enter);
    map_result["join"] = std::make_pair(I_ARGUMENTS_UNDEFINED, &CTreeCLI::v_join_with);
    map_result["exit"] = std::make_pair(0, &CTreeCLI::v_exit_cli);
    map_result["help"] = std::make_pair(0, &CTreeCLI::v_print_help);
    return map_result;
}

void CTreeCLI::v_print(){
    if(ct_tree == NULL){
        std::cout << S_TREE_NOT_CREATED_MSG << "\n";
        return;
    }
    std::cout << *ct_tree << '\n';
}

void CTreeCLI::v_print_vars(){
    if(ct_tree == NULL){
        std::cout << S_TREE_NOT_CREATED_MSG << "\n";
        return;
    }
    std::vector<const CNode*> cv_temp;
    ct_tree -> v_get_variables(cv_temp);
    std::cout << "Vars: ";
    for(unsigned int i = 0 ; i < cv_temp.size(); ++i){
        std::cout << *cv_temp[i] << ' ';
    }
    std::cout << '\n';
}

void CTreeCLI::v_print_leaves() {
    if(ct_tree == NULL){
        std::cout << S_TREE_NOT_CREATED_MSG << "\n";
        return;
    }
    std::vector<const CNode*> cv_temp;
    ct_tree -> v_to_vec_rec(ct_tree ->cnp_root -> ctp_children[0], cv_temp, &CNode::b_is_leaf);
    std::cout<< "Leaves: " << cv_temp.size() << '\n';
}

void CTreeCLI::v_join_with(){
    std::vector<std::string> v_temp(sv_parsed_queries.begin()+1, sv_parsed_queries.end());
    if(ct_tree == NULL){
        ct_tree = new CTree(v_temp);
        return;
    }
    CTree ct_temp(v_temp);
    *ct_tree = *ct_tree + ct_temp;
    return;
}
void CTreeCLI::v_enter(){
    if(ct_tree != NULL){
        delete ct_tree;
        ct_tree = NULL;
    }
    std::vector<std::string> v_temp(sv_parsed_queries.begin()+1, sv_parsed_queries.end());
    ct_tree = new CTree(v_temp);
}
void CTreeCLI::v_comp(){
    if(ct_tree == NULL){
        std::cout << S_TREE_NOT_CREATED_MSG << "\n";
    }
    std::vector<double> v_temp;
    for(unsigned int i = 1; i < sv_parsed_queries.size(); ++i){
        v_temp.push_back(CNode::d_str_to_double(sv_parsed_queries[i]));
    }

    bool b_status = ct_tree -> b_comp(v_temp);
    if(!b_status){
        std::cout << "comp error: zla liczba parametrow\n";
        return;
    }
    std::cout << "Wartosc wyrazenia -> " << ct_tree -> d_calculate() << '\n';
}

void CTreeCLI::v_exit_cli(){
    std::cout << "koniec programu\n";
    b_running = false;
}

void CTreeCLI::v_print_help(){
    std::cout << "DOSTEPNE KOMENDY:\n";
    for(int i = 0; i < 7; ++i){
        printf("%7s%5s %-100s\n", st_help_table[i][0], " -> ", st_help_table[i][1]);
    }
}


