#include "CTree.h"
#include "CNode.h"
#include "../CResult.h"
#include "../CError.h"

#include <algorithm>
#include <cstdlib>
#include <string>

CTree::CTree(std::vector<std::string> &av_expression){
    cnp_root = new CNode();
    i_size = 0;
    std::string s_temp;
    std::vector<std::string> v_expression;
    for(unsigned int i = 0; i < av_expression.size(); ++i){
        s_temp = "";
        for(unsigned int j = 0; j < av_expression[i].size(); ++j){
            char ch_valid = CNode::ch_get_valid_char(av_expression[i][j]);
            if((int)ch_valid != 0) s_temp += ch_valid;
            else msg_messages.add_message("ominieto bledne znaki");
        }
        if(s_temp.size()) v_expression.push_back(s_temp);
    }
    unsigned int i_temp = i_find_and_add_to_parent(cnp_root, v_expression, 0);
    v_fix_tree(cnp_root->ctp_children[0]);

    // sprawdzamy czy nie podano czegos co sie nie wczytalo,
    // np za duzo argumentow
    msg_messages.new_message();
    if(i_temp < av_expression.size()){
        msg_messages.upload("info: ominieto -> ");
        for(; i_temp < av_expression.size(); ++i_temp){
            msg_messages.upload(av_expression[i_temp] + " ");
        }
        msg_messages.upload( "(za duzo argumentow)");
    }
    msg_messages.close_message();
}


CTree::CTree(const CTree &acp_other){
    cnp_root = new CNode(*acp_other.cnp_root);
    i_size = acp_other.i_size;
}

CResult<CTree, CError> CTree::cr_create_with_result(const std::string &as_expression){
    //ogarniecie spacji
    unsigned int i_index = 0;
    std::vector<std::string> v_temp;
    v_temp.push_back("");
    for(; i_index < as_expression.size(); ++i_index){
        if(as_expression[i_index] == ' ') v_temp.push_back("");
        while(i_index < as_expression.size() && as_expression[i_index] == ' ') ++i_index;
        v_temp[v_temp.size() -1] += as_expression[i_index];
    }
    
    //tworzenie drzewa
    CTree ct_temp(v_temp);
    if(ct_temp.b_has_messages()){
        std::vector<CError*> cv_errors;
        for(unsigned int i = 0; i < ct_temp.msg_messages.sv_message_list.size(); ++i){
            cv_errors.push_back(new CError(ct_temp.msg_messages.sv_message_list[i]));
        }
        return CResult<CTree, CError> :: c_fail(cv_errors);
    }
    return CResult<CTree, CError> :: c_ok(ct_temp);
}

CTree::~CTree(){
    delete cnp_root;
}

bool CTree::b_comp(std::vector<double> &avd_args){
    std::vector<const CNode*> cv_vars;
    // v_to_vec_rec(cnp_root -> ctp_children[0], cv_vars, &CNode::b_is_variable);
    v_get_variables(cv_vars);

    //init variable map
    dm_variable_map.clear();
    for(unsigned int i = 0; i < std::min(avd_args.size(), cv_vars.size()); ++i){
        dm_variable_map[cv_vars[i] -> s_to_string()] = avd_args[i];
    }

    if(dm_variable_map.size() != avd_args.size()){
        return false;
    }
    return true;
}

void CTree::v_get_variables(std::vector<const CNode*> &avs_vec) const{
    std::vector<const CNode*> cv_temp;
    avs_vec.clear();
    v_to_vec_rec(cnp_root -> ctp_children[0], cv_temp, &CNode::b_is_variable);

    std::map<std::string, bool> map_visited;
    for(unsigned int i = 0 ; i < cv_temp.size(); ++i){
        if(map_visited.find(cv_temp[i]->s_to_string()) == map_visited.end()){
            avs_vec.push_back(cv_temp[i]);
            map_visited[cv_temp[i] -> s_to_string() ] = true;
        }
    }
    return;

}

unsigned int CTree::i_find_and_add_to_parent(CNode *acp_parent, const std::vector<std::string> &av_expression, const int start){
    if(start >= (int)av_expression.size()) return start;

    unsigned int i_expression_index = start;
    for(int i = 0; i < acp_parent->i_max_children && i_expression_index < av_expression.size(); ++i){
        acp_parent->v_add_child(av_expression[i_expression_index]);
        ++i_expression_index;
        i_expression_index = i_find_and_add_to_parent(acp_parent->ctp_children[acp_parent->i_curr_children-1], av_expression, i_expression_index);
        i_size++;
    }
    return i_expression_index;
}

void CTree::v_fix_tree(CNode *acp_node){
    if(acp_node == NULL) return;
    
    for(int i = acp_node->i_curr_children; i < acp_node->i_max_children; ++i){
        acp_node->v_add_child(acp_node->S_DEFAULT_CHILD);
        msg_messages.add_message("info: uzupelniono argument dla '" + acp_node->s_to_string() + "' -> " + acp_node->S_DEFAULT_CHILD);
    }
    for(int i = 0; i < acp_node->i_curr_children; ++i) v_fix_tree(acp_node->ctp_children[i]); 
    return;
}

void CTree::v_swap_with(CTree &acp_other){
    i_size = acp_other.i_size;
    std::swap(cnp_root, acp_other.cnp_root);
}

std::ostream& CTree::o_get_ostream_text(std::ostream &os) const{
    os << s_to_string();
    return os;
}

void CTree::v_to_vec_rec(const CNode* apc_node, std::vector<const CNode*> &avc_buffer, bool (CNode::*abp_check)() const){
    if(apc_node == NULL) {
        return;
    }
    if((apc_node ->* abp_check)()) avc_buffer.push_back(apc_node);
    for(int i = 0 ; i < apc_node->i_curr_children; ++i){
        v_to_vec_rec(apc_node->ctp_children[i], avc_buffer, abp_check);
    }
    return;
}

double CTree::d_calculate()const{
    return d_calculate_helper(cnp_root -> ctp_children[0]);
}

double CTree::d_calculate_helper(const CNode* acp_node)const{
    if(acp_node == NULL) return 0.0;

    std::string s_text = acp_node -> s_to_string();
    std::map<std::string, double>::const_iterator map_iter = dm_variable_map.find(s_text);

    if(acp_node -> b_is_variable()) {
        if(map_iter == dm_variable_map.end()){
            return -1.0;
        }
        return dm_variable_map.at(s_text);
    }

    else if(acp_node -> b_is_number()){
        return CNode::d_str_to_double(s_text);
    }

    std::vector<double> dv_children;
    double (*d_fptr)(const std::vector<double>&);
    d_fptr = (acp_node -> V_OPERATOR_MAP.at(s_text)).second;
    for(int i = 0 ; i < acp_node -> i_curr_children; ++i){
        dv_children.push_back(d_calculate_helper(acp_node -> ctp_children[i]));
    }
    return (*d_fptr)(dv_children);
}

std::string CTree::s_to_string() const{
    return s_to_string(&CNode::b_is_true);
}

std::string CTree::s_to_string(bool (CNode::*abp_check)() const) const{
    std::vector<const CNode*> v_buffer;
    std::string s_result;
    v_to_vec_rec(cnp_root->ctp_children[0], v_buffer, abp_check);

    for(unsigned int i = 0; i < v_buffer.size(); ++i){
        s_result += v_buffer[i]->s_to_string() + ' ';
    }
    return s_result;
}

bool CTree::b_has_messages() const{
    return msg_messages.sv_message_list.size() != 0;
}

std::string CTree::s_get_last_msg() const{
    return msg_messages.last;
}

CTree& CTree::operator=(CTree acp_other){
    v_swap_with(acp_other);
    return *this;
}

CTree CTree::operator+(const CTree &acp_other) const{
    CTree c_this_copy(*this);

    if(!acp_other.i_size) return c_this_copy;
    else if(!c_this_copy.i_size) return acp_other;

    const CNode* c_first_leaf = c_find_first_leaf(c_this_copy.cnp_root);

    delete c_first_leaf;
    c_first_leaf = new CNode(*acp_other.cnp_root->ctp_children[0]);
    c_this_copy.i_size += acp_other.i_size -1;

    return c_this_copy;
}

std::ostream& operator<<(std::ostream &os, const CTree &acp_other){
    os << "CTree: ";
    return acp_other.i_size ? acp_other.o_get_ostream_text(os) : os << "Empty";
}

const CNode* CTree::c_find_first_leaf(const CNode* acp_parent){
    if(!acp_parent->i_max_children || !acp_parent->i_curr_children) return acp_parent;
    return c_find_first_leaf(acp_parent->ctp_children[0]);
}

