#include <iostream>
#include "CMessage.h"

CMessage::CMessage(){
    last = "";
    temp_message= "";
    sv_message_list.clear();
    return;
}

CMessage::CMessage(const CMessage &amp_other){
    last = amp_other.last;
    sv_message_list = amp_other.sv_message_list;
}

CMessage::~CMessage(){
    return;
}

void CMessage::add_message(std::string as_to_add){
    sv_message_list.push_back(as_to_add);
    last = as_to_add;
    return;
}

void CMessage::new_message(){
    temp_message = "";
    return;
}

void CMessage::close_message(){
    if(temp_message.size()) sv_message_list.push_back(temp_message);
    temp_message = "";
    return;
}

void CMessage::upload(std::string as_to_add){
    temp_message += as_to_add;
    return;
}

std::string CMessage::next_message(){
    last = sv_message_list[sv_message_list.size()-1];
    sv_message_list.pop_back();
    return last;
}

std::ostream& operator<<(std::ostream& os, CMessage &cm_obj){
    while(cm_obj.sv_message_list.size()){
        os << "CMessage: " << cm_obj.next_message() << '\n';
    }
    return os;
}

CMessage& CMessage::operator=(CMessage amp_other){
    std::swap(*this, amp_other);
    return *this;
}
