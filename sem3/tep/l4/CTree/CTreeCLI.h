#ifndef CTREECLI
#define CTREECLI

#include <iostream>
#include <map>
#include <vector>
#include "CTree.h"
#include "CNode.h"

class CTreeCLI;
typedef std::map<std::string, std::pair<int, void (CTreeCLI::*)() > > cli_map_type;

class CTreeCLI{
    public:
        CTreeCLI();
        ~CTreeCLI();

        void start();
        void v_parse_query(std::string &as_query);

    private:
        CTree* ct_tree;
        bool b_running;
        std::vector<std::string> sv_parsed_queries;

        void v_print();
        void v_print_leaves();
        void v_print_vars();
        void v_join_with();
        void v_enter();
        void v_comp();
        void v_exit_cli();
        void v_print_help();

        static cli_map_type v_init_map_commands();

        static const int I_NUMBER_OF_COMMANDS = 5;
        static const cli_map_type MAP_COMMANDS;
        static const int I_ARGUMENTS_UNDEFINED = -123;
        static const int I_HELP_TABLE_SIZE_Y = 8;
        static const int I_HELP_TABLE_SIZE_X = 2;
        static const char* st_help_table[I_HELP_TABLE_SIZE_Y][I_HELP_TABLE_SIZE_X];

        static const std::string S_TREE_NOT_CREATED_MSG;
};

#endif
