#ifndef CTREE_H
#define CTREE_H

#include "CNode.h"
#include "CMessage.h"
#include "../CResult.h"
#include "../CError.h"

#include <iostream>
#include <vector>
#include <map>

class CTree{
    public:
        CTree(std::vector<std::string> &as_expression);
        CTree(const CTree &acp_other);
        ~CTree();

        friend std::ostream& operator<<(std::ostream& os, const CTree& obj);
        CTree& operator=(CTree acp_other);
        CTree operator+(const CTree &acp_other) const;

        std::string s_to_string() const;
        void v_get_variables(std::vector<const CNode*> &avs_vec) const;
        bool b_comp(std::vector<double> &avd_args);
        bool b_has_messages() const;
        double d_calculate() const;

        friend class CTreeCLI;

        std::string s_get_last_msg() const;
        CMessage msg_messages;
        static CResult<CTree, CError> cr_create_with_result(const std::string &as_expression);

    private:
        CNode *cnp_root;
        unsigned int i_size;
        std::map<std::string, double> dm_variable_map;

        std::ostream& o_get_ostream_text(std::ostream& os) const;

        void v_swap_with(CTree &acp_other);
        void v_fix_tree(CNode *acp_node);
        static void v_to_vec_rec(const CNode *apc_node, std::vector<const CNode*> &avc_buffer, bool (CNode::*abp_check)() const);

        unsigned int i_find_and_add_to_parent(CNode *acp_parent, const std::vector<std::string> &as_expression, const int index);

        std::string s_to_string(bool (CNode::*abp_check)() const) const;
        double d_calculate_helper(const CNode* acp_node) const;

        static const CNode* c_find_first_leaf(const CNode* acp_parent);
};

#endif
