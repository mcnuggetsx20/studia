#ifndef CRESULT_H
#define CRESULT_H

#include <iostream>
#include <vector>

template <typename T, typename E>
class CResult{
    public:
        CResult(const T& ac_value);
        CResult(E* apc_error);
        CResult(std::vector<E*> &v_errors);
        CResult(const CResult<T, E> &ac_other);

        ~CResult();

        static CResult<T, E> c_ok(const T &ac_value);
        static CResult<T, E> c_fail(E* apc_error);
        static CResult<T, E> c_fail(std::vector<E*> &v_errors);

        CResult<T, E>& operator=(const CResult<T, E> ac_other);

        bool b_is_success() const;

        //getters
        T c_get_value() const;
        std::vector<E*>& v_get_errors();

        void v_evaluate();

    private:
        T *pc_value;
        std::vector<E*> v_errors;
};

//success
template<typename T, typename E> //wymagany kopiujacy w T
CResult<T,E>::CResult(const T& ac_value){
    pc_value = new T(ac_value);
    return;
}

//error
template<typename T, typename E> //wymagany kopiujacy w E
CResult<T, E>::CResult(E* apc_error){
    pc_value = NULL;
    v_errors.push_back(new E(*apc_error));
    return;
}

//multiple errors
template<typename T, typename E> //wymagany kopiujacy w E
CResult<T, E>::CResult(std::vector<E*> &av_errors){
    pc_value = NULL;
    for(unsigned int i = 0 ; i < av_errors.size(); ++i){
        if(av_errors[i] != NULL) v_errors.push_back(new E(*av_errors[i]));
    }
    return;
}

//copy constructor
template<typename T, typename E> //wymagany copy w E
CResult<T, E>::CResult(const CResult<T, E>& ac_other){
    if(ac_other.pc_value == NULL) pc_value =NULL;
    else pc_value = new T(*(ac_other.pc_value));

    for(unsigned int i = 0 ; i < ac_other.v_errors.size(); ++i){
        v_errors.push_back(new E(*(ac_other.v_errors[i])));
    }
    return;
}

//destructor
template<typename T, typename E>
CResult<T, E>::~CResult(){
    if(pc_value != NULL) delete pc_value;
    for(unsigned int i = 0 ; i < v_errors.size(); ++i){
        if(v_errors[i] != NULL) delete v_errors[i];
    }
    v_errors.clear();
    return;
}

template<typename T, typename E>
CResult<T, E> CResult<T, E>::c_ok(const T& ac_value){
    return CResult<T, E>(ac_value);
}

template<typename T, typename E>
CResult<T, E> CResult<T,E>::c_fail(E* pc_error){
    return CResult<T, E>(pc_error);
}

template<typename T, typename E>
CResult<T, E> CResult<T,E>::c_fail(std::vector<E*>& v_errors){
    return CResult<T,E>(v_errors);
}

template<typename T, typename E>
bool CResult<T,E>::b_is_success()const{
    return  !v_errors.size();
}

//copy and swap
template<typename T, typename E>
CResult<T, E>& CResult<T,E>::operator=(const CResult<T, E> ac_other){
    std::swap(pc_value, ac_other.pc_value);
    std::swap(v_errors, ac_other.v_errors);
    return *this;
}
template<typename T, typename E>
T CResult<T,E>::c_get_value() const{
    return *pc_value;
}

template<typename T, typename E>
std::vector<E*>& CResult<T,E>::v_get_errors(){
    return v_errors;
}


template<typename T, typename E>
void CResult<T,E>::v_evaluate(){
    if(b_is_success()){
        std::cout << c_get_value() << '\n';
    }
    else{
        for(unsigned int i = 0; i < v_get_errors().size(); ++i){
            std::cout << *(v_get_errors()[i]) << '\n';
        }
        return;
    }
    return;
}



#endif
