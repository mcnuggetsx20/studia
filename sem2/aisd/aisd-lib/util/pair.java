package aisd.util;
public class pair<T, E>{
    public T first;
    public E second;

    public pair(T a, E b){
        this.first = a;
        this.second = b;
    }

    @Override 
    public String toString(){
        return String.format("(%s, %s)", first, second);
    }
}
