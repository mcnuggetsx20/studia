package aisd.util;
public class Util{
    public static void dbg(Object... args){
        String delimiter = " ";
        for(Object i: args){
            delimiter = String.valueOf(i).equals("\n") ? "" : " ";
            System.out.printf("%s%s", String.valueOf(i), delimiter);
        }
        System.out.println();
    }
}
