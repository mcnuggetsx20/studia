import java.util.ArrayList;

public class BST<T extends Comparable<T>>{

    private class Node<T extends Comparable<T>>{
        T value = null;
        public Node<T> small = null;
        public Node<T> big = null;

        public Node(T val){
            this.value = val;
        }

        public int compareTo(Node<T> node){
            return this.value.compareTo(node.value);
        }
        public int compareTo(T val){
            return this.value.compareTo(val);
        }

        public String toString(){return String.format("%s", String.valueOf(this.value));}
    }

    int size = 0;
    public Node<T> root = null;

    public String _toString(Node<T> node){
        if(node==null) return "";
        return String.format("%s: %s, %s\n%s%s", node, node.small, node.big, _toString(node.small), _toString(node.big));
    }
    public String toString(){
        String res = _toString(this.root);
        return res.substring(0, res.length()-1);
    }

    public BST(T[] t){
        for(T i : t) insert(this.root, i);
        this.root = insert(this.root, t[0]);
        for(int i = 1; i < t.length; ++i) insert(this.root, t[i]);
        return;
    }

    public Node<T> insert(Node<T> node, T ele){
        Node<T> new_element = new Node<T>(ele);
        if(node == null){
            node = new_element;
            return node;
        }
        if(node.compareTo(new_element) > 0) node.small = insert(node.small, ele);
        else node.big = insert(node.big, ele);
        return node;
    }

    public Node<T> search(Node<T> node, T val){
        if(node==null) return null;
        if(node.value.equals(val)) return new Node<T>(val);
        return search (node.compareTo(val) > 0 ? node.small : node.big, val);
    }

    public boolean contains(T val){return (search(this.root, val) != null);}
}
