import java.util.Iterator;
public class FibbonaciIterator implements Iterator<Integer>{
    private int a = 1,
                b = 0;

    @Override
    public boolean hasNext(){return true;}

    @Override
    public Integer next(){
        int res = a + b;
        a = b;
        b=res;
        return res;
    }

    public Integer checkNext(){return a+b;}

    public static void main(String[] args){
        FibbonaciIterator iter = new FibbonaciIterator();
        while(iter.checkNext() <= 1000) System.out.println(iter.next());

    }
}
