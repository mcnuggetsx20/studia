import java.util.Iterator;
public class ArrayFromK implements Iterator<Integer>{
    int k;
    public ArrayFromK(int k){
        this.k = k;
    }

    @Override
    public boolean hasNext(){
        return true;
    }

    @Override
    public Integer next(){
        return ++k;
    }

    public static void main(String[] args){
        ArrayFromK iter = new ArrayFromK(5);
        while(iter.next() != 100){
            System.out.println(iter.next());
        }
        return;
    }
}
