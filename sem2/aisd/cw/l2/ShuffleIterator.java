import java.util.Iterator;
import java.util.ArrayList;
public class ShuffleIterator<T> implements Iterator<T>{
    private Iterator<T> it1, it2;
    private int cnt = 0;

    public ShuffleIterator(Iterator<T> it1, Iterator<T> it2){
        this.it1 = it1;
        this.it2 = it2;
    }

    @Override
    public boolean hasNext(){
        return it2.hasNext() || it1.hasNext();
    }

    @Override
    public T next(){
        ++cnt;
        if( (cnt%2 == 1 && it1.hasNext()) || (cnt%2==0 && !it2.hasNext())) return it1.next();
        return it2.next();
    }

    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<>();
        ArrayList<Integer> arr2 = new ArrayList<>();

        for(int i =1; i < 15; ++i){
            if(i%2==1) arr1.add(i);
            else arr2.add(i);
        }

        arr1.removeLast();

        ShuffleIterator<Integer> iter = new ShuffleIterator<>(arr1.iterator(), arr2.iterator());
        
        while(iter.hasNext()) System.out.println(iter.next());
    }
}
