import java.util.Iterator;
import java.util.NoSuchElementException;
public class Iterator2d<T> implements Iterator<T>{

    private T[][] array;
    private int n = -1,
                m = 0;

    public Iterator2d(T[][] arr){
        array = arr;
    }

    @Override
    public boolean hasNext(){
        return n!=array[array.length-1].length-1 || m!=array.length-1;
    }

    @Override
    public T next(){
        if(!hasNext()) throw new NoSuchElementException();

        if(n == array[m].length-1){
            n = -1;
            m++;
        }

        return array[m][++n];
    }

    public static void main(String[] args){
        Integer A[][] = {
            {1, 2, 4}, 
            {6, 2, 6}, 
            {7, 10, 1}
        };
        Iterator2d<Integer> iter = new Iterator2d<>(A);

        while(iter.hasNext()) System.out.println(iter.next());
    }
}
