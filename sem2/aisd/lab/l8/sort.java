import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
public class sort{
    int[] arr;
    ArrayList<Integer> gaps = new ArrayList<>();

    @SuppressWarnings("unchecked")
    private ArrayList<Integer>[] gps = new ArrayList[5];

    public static void swap(int[] N, int i1, int i2){
        int temp = N[i1];
        N[i1] = N[i2];
        N[i2] = temp;
        return;
    }

    public void genData(int N){
        Random rand = new Random();
        arr= new int[N];
        for(int i = 0 ; i< N; ++i){
            arr[i] = rand.nextInt(1, 1000000);
        }
    }

    public void genGaps(int limit){
        //ciag a
        for(int i =0; i < gps.length; ++i) this.gps[i] = new ArrayList<>();
        this.gps[0].add(1);
        int ind = 1, temp = 0;

        while(temp < limit){
            temp = 3*gps[0].get(ind-1) + 1;
            gps[0].add(temp);
            ++ind;
        }

        //ciag b
        temp = 0;
        for(int i =1; temp < limit; ++i){
            temp = (int)Math.pow(2,i) -1;
            this.gps[1].add(temp);
        }

        //ciag c
        temp = 0;
        for(int i = 1; temp < limit; ++i){
            temp = (int)Math.pow(2,i) +1;
            this.gps[2].add(temp);
        }

        //ciag d
        this.gps[3].add(1);
        this.gps[3].add(1);
        temp = 0;
        while(temp < limit){
            int len = gps[3].size();
            temp = gps[3].get(len-1) + gps[3].get(len-2);
            gps[3].add(temp);
        }

        //ciag e
        temp = limit/2;
        this.gps[4].add(temp);
        while(temp > 1){
            temp *= 0.75;
            gps[4].add(temp);
        }
        
        //podwojny reverse bo nie chce mi sie myslec nad tym
        this.gps[4] = new ArrayList<>(gps[4].reversed());

        for(int i = 0; i < this.gps.length; ++i){
            this.gps[i] = new ArrayList<>(gps[i].reversed());
            //System.out.println(this.gps[i]);
        }
    }

    public void shellsortA(){
        int[] N = arr.clone();

        for(Integer gap: gaps){
            for(int i =gap; i < N.length; ++i){
                int curr = N[i], j;
                for(j = i; j >= gap && curr < N[j-gap]; j-=gap) swap(N, j, j-gap);
            }
        }
        //System.out.println(Arrays.toString(N));
    }

    public void shellsortB(){
        int[] N = arr.clone();

        for(Integer gap : gaps.subList(1, gaps.size())){
            for(int i =gap; i < N.length; ++i){
                int curr = N[i], j;
                for(j = i; j >= gap && curr < N[j-gap]; j-=gap) swap(N, j, j-gap);
            }
        }
        boolean swapped = true;

        while(swapped){
            swapped=false;
            for(int i = 0 ;i < N.length-1;++i){
                if(N[i] > N[i+1]) {
                    swap(N, i, i+1);
                    swapped=true;
                }
            }
        }
        //System.out.println(Arrays.toString(N));
    }

    public void shellsortC(){
        int[] N = arr.clone();
        boolean swapped;

        for(Integer gap : gaps.subList(0, gaps.size())){
            swapped = true;
            while(swapped){
                swapped = false;
                for(int i =gap; i < N.length; ++i){
                    if(N[i] > N[i-gap]){
                        swap(N, i, i-gap);
                        swapped=true;
                    }
                }
            }
        }

        int gap = 1;
        for(int i =1; i < N.length; ++i){
            int curr = N[i], j;
            for(j = i; j >= gap && curr < N[j-gap]; j-=gap) swap(N, j, j-gap);
        }
        //System.out.println(Arrays.toString(N));
    }

    public void runAll(){
        int[] sample_sizes = {5000, 10000, 50000, 100000};
        long start;


        genGaps(10);
        genData(10);
        this.gaps = this.gps[0];
        start = System.nanoTime(); shellsortA(); //dummy
        start = System.nanoTime(); shellsortB(); //dummy
        start = System.nanoTime(); shellsortC(); //dummy
        
        for(int sample_size : sample_sizes){
            genGaps(sample_size);
            genData(sample_size);
            
            System.out.printf("Rozmiar paczki: %d:\n", sample_size);
            for(int i = 0; i < this.gps.length; ++i){
                this.gaps = this.gps[i];
                System.out.printf("Ciag %c\n", (char)(97 + i));

                start = System.nanoTime(); shellsortA(); System.out.printf("Wersja 1%40d\n", System.nanoTime()-start);
                start = System.nanoTime(); shellsortB(); System.out.printf("Wersja 2%40d\n", System.nanoTime()-start);
                start = System.nanoTime(); shellsortC(); System.out.printf("Wersja 3%40d\n", System.nanoTime()-start);
            System.out.println();
            }
            System.out.println();
        }
    }
}
