import static aisd.util.Util.*;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.File;

public class main{
    public static void main(String[] args) throws FileNotFoundException, EmptyQueueException, FullQueueException{
        skorowidz a = new skorowidz("aforyzm.txt");
        dbg("Drzewo wypisane alfabetycznie: ");
        a.print();
        a.remove("chocby");

        dbg();
        dbg("Drzewo po usunieciu elementu");
        a.print();
        dbg();

        dbg("Przejscie BFS");
        a.printBFS();
    }
}
