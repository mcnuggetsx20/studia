import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import aisd.queue.ListQueue;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;
import static aisd.util.Util.*;

public class BST<T extends Comparable<T>>{

    private class Node<T extends Comparable<T>>{
        public T value = null;
        public Node<T> small = null;
        public Node<T> big = null;

        public Node(T val){
            this.value = val;
        }

        public int compareTo(Node<T> node){
            return this.value.compareTo(node.value);
        }

        public int compareTo(T val){
            return this.value.compareTo(val);
        }

        public String toString(){return String.format("%s", String.valueOf(this.value));}
    }

    public static <T extends Comparable<? super T>> T min(T a, T b) {
        return (a.compareTo(b) <= 0) ? a : b;
    }
    
    int size = 0;
    public Node<T> root = null;
    public HashMap<T, Boolean> visited = new HashMap<>();

    public String _toString(Node<T> node){
        if(node==null) return "";
        return String.format("%s: %s, %s\n%s%s", node, node.small, node.big, _toString(node.small), _toString(node.big));
    }
    public String toString(){
        String res = _toString(this.root);
        return res.substring(0, res.length()-1);
    }

    public BST(){
        return;
    }

    public BST(T[] t){
        //for(T i : t) insert(this.root, i);
        this.root = insert(this.root, t[0]);
        for(int i = 1; i < t.length; ++i){
            if( this.contains(t[i])) continue;
            insert(this.root, t[i]);
        }
        return;
    }

    public Node<T> insert(Node<T> node, T ele){
        Node<T> new_element = new Node<T>(ele);
        if(node == null){
            node = new_element;
            return node;
        }
        if(node.compareTo(new_element) > 0) node.small = insert(node.small, ele);
        else node.big = insert(node.big, ele);
        return node;
    }

    public Node<T> remove(Node<T> node, T ele){
        if(node==null) return null;

        if(node.compareTo(ele) < 0) node.big = remove(node.big, ele);
        else if(node.compareTo(ele) > 0) node.small = remove(node.small, ele);
        else{
            if(node.small == null && node.big == null) return null;
            else if(node.small == null && node.big != null){
                Node<T> temp = new Node<T>(node.big.value);
                node.big = null;
                return temp;
            }
            else if(node.small != null && node.big == null){
                Node<T> temp = new Node<T>(node.small.value);
                node.small = null;
                return temp;
            }
            else{
                T successor = minValue(node.big);
                dbg(successor, node);
                node.big = remove(node.big, successor);
                node.value = successor;
            }
        }
        return node;
    }

    public Node<T> search(Node<T> node, T val){
        if(node==null) return null;
        if(node.value.equals(val)) return new Node<T>(val);
        return search (node.compareTo(val) > 0 ? node.small : node.big, val);
    }

    public T minValue(Node<T> node){
        T mn = node.value;
        while(node.small != null){
            mn = min(mn, node.small.value);
            node = node.small;
        }
        return mn;
    }

    public boolean contains(T val){return (search(this.root, val) != null);}

    private void _printInorder(Node<T> node){
        if(node==null) return;
        _printInorder(node.small);
        System.out.println(node);
        _printInorder(node.big);
    }
    public void printInorder(){
        _printInorder(this.root);
    }

    private void _getInorder(Node<T> node, ArrayList<T> arr){
        if(node==null) return;
        _getInorder(node.small, arr);
        arr.add(node.value);
        _getInorder(node.big, arr);
    }

    public void getInorder(ArrayList<T> arr){
        _getInorder(this.root, arr);
    }

    public void getBFS(ArrayList<T> arr) throws FullQueueException, EmptyQueueException{
        ListQueue<Node<T>> nodes = new ListQueue<>();
        nodes.enqueue(this.root);

        while(!nodes.isEmpty()){
            Node<T> temp = nodes.dequeue();

            arr.add(temp.value);
            if(temp.small != null) nodes.enqueue(temp.small);
            if(temp.big != null) nodes.enqueue(temp.big);
        }
    }
}
