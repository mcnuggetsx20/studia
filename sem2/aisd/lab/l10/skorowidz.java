import static aisd.util.Util.*;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.File;
import java.util.HashMap;

public class skorowidz{
    public BST<String> mainTree;
    HashMap<String, ArrayList<Integer>> occurances = new HashMap<>();

    public skorowidz(String filename) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(filename));
        ArrayList<String> arr = new ArrayList<>();
        int line = 0;
        while(scanner.hasNextLine()){
            ++line;
            for(String i: scanner.nextLine().split("\\s+|\\p{Punct}")) {
                arr.add(i);
                if(!occurances.containsKey(i)) occurances.put(i, new ArrayList<Integer>());
                occurances.get(i).add(line);
            }
        }
        scanner.close();

        this.mainTree = new BST<String>(arr.toArray(new String[0]));
    }

    public void print(){
        ArrayList<String> arr = new ArrayList<>();
        this.mainTree.getInorder(arr);
        for(String i : arr){
            dbg(i, this.occurances.get(i));
        }
        return;
    }

    public void remove(String word){
        this.mainTree.remove(this.mainTree.root, word);
        occurances.remove(word);
    }

    public void printBFS() throws FullQueueException, EmptyQueueException{
        ArrayList<String> arr = new ArrayList<>();
        this.mainTree.getBFS(arr);
        for(String i : arr){
            dbg(i, this.occurances.get(i));
        }
        return;
    }
}
