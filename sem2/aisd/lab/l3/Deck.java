import java.util.Random;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class Deck{
    boolean[] bs = new boolean[52];
    LinkedList <Card> ll = new LinkedList<>();
    int covered = 0;

    public Deck(){
        covered = 0;

        Random rand = new Random();

        //tu zaczynamy losowanie kart do talii
        int curr_val = rand.nextInt(14 +1),
            curr_col = rand.nextInt(3 +1),
            index;

        while(curr_val != 0){
            if(curr_val == 14){
                covered++;
            }

            //obliczamy index na ktory wstawimy nowa karte; w ten sposob beda one od razu posortowane
            else{
                index = (curr_val -1)*4 + curr_col;

                if(ll.size() ==0){
                    ll.add(new Card(curr_val, curr_col, true, 1));
                    continue;
                }

                ListIterator<Card> iter = ll.listIterator();
                int c = 0;
                while(iter.hasNext() && !bs[index]){
                    Card temp = iter.next();
                    int tempind = (temp.getValue()-1)*4 + temp.getColor();
                    if(tempind > index){
                        ll.add(c, new Card(curr_val, curr_col, true, 1));
                        break;
                    }
                    c++;
                }
                bs[index] = true;

            }

            //generujemy parametry dla nastepnej karty
            curr_val = rand.nextInt(14 +1); 
            curr_col = rand.nextInt(3 +1);
        }
        if(covered>0){
            ll.add(new Card(null, null, false, covered));
        }
    }

    //public void addCard(int val, int col){
    //    int index = (val -1)*4 + col;
    //    arr.get(index).add(new Card(val, col));
    //}

    public void showCards(){
        for(Card card : ll){
            System.out.printf("%s", card.toString());
            if(!card.getZnacznik()){
                for(int i =0; i < card.getQuantity()-1; ++i){
                    System.out.printf("%s", card.toString());
                }
            }
        }

    }

    public void cardsBy(String s){
        for(Card card : ll){
            if(!card.getZnacznik()){continue;}
            if( card.valueIs(s) || card.colorIs(s) ){
                System.out.printf("%s", card.toString());
            }
        }
    }

    public void removeCovered(){
        ll.remove(ll.size()-1);
        covered = 0;
    }

    public int getSize(){return ll.size() + Math.max(0, covered-1);}
    public int getCovered(){return covered;}
}
