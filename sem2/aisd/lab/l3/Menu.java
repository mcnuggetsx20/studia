import java.util.Scanner;
import java.lang.NumberFormatException;

public class Menu{
    static Scanner scanner = new Scanner(System.in);
    String[] OPTIONS;

    public Menu(){
        String[] arr = {
            "1. Utworzenie listy",
            "2. Wyświetlanie listy (karty zakryte drukuj jako parę nawiasów \"( )\"",
            "3. Wyświetlanie liczby elementów listy oraz ile jest kart zakrytych i ile odkrytych",
            "4. Wyświetlanie kart o podanej wartości",
            "5. Wyświetlanie kart o podanym kolorze",
            "6. Usuwanie kart zakrytych",
        };

        OPTIONS = new String[arr.length];
        System.arraycopy(arr, 0, OPTIONS, 0, arr.length);
    }

    public void show(){
        System.out.println("         __  __ _____ _   _ _   _");
        System.out.println("        |  \\/  | ____| \\ | | | | |");
        System.out.println("        | |\\/| |  _| |  \\| | | | |");
        System.out.println("        | |  | | |___| |\\  | |_| |");
        System.out.println("        |_|  |_|_____|_| \\_|\\___/ ");

        System.out.println("Opcje:");
        for(String option : OPTIONS){System.out.println(option);}
    }

    public void listen() throws NumberFormatException{
        System.out.printf("-> ");
        String input;

        String exception = "Nie stworzyles jeszcze talii!";

        Deck deck = null;
        while(!(input=scanner.nextLine()).equals("-1")){
            switch(input){

                case "1":
                    deck = new Deck();
                    System.out.printf("Stworzono %3d kart\n", deck.getSize());
                    break;

                default:
                    if(deck==null){System.out.println(exception); break;}
                    switch(input){
                        case "2":
                            deck.showCards();
                            break;

                        case "3":
                            System.out.printf("Jest %3d kart w talii w tym:\n%3d odkrytych\n%3d zakrytych\n",  deck.getSize(), deck.getSize()-deck.getCovered(), deck.getCovered());
                            break;

                        case "4":
                        case "5":
                            deck.cardsBy(listenArgument());
                            break;
                        
                        case "6":
                            deck.removeCovered();
                            System.out.printf("Usunieto karty zakryte\n");
                            break;

                        default:
                            System.out.println("Nie ma takiej opcji!");


                     }
            }
            System.out.printf("-> ");
        }
        System.out.println("Opuszczanie...");
        System.exit(0);

    }

    public static String listenArgument(){
        System.out.printf("++ ");
        String input = scanner.nextLine();
        return input;
    }
}
