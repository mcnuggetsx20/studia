import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import static aisd.util.Util.*;

public class DSU{
    public int[] parents; 
    public int[] ranks;
    public ArrayList<ArrayList<Integer>> edges = new ArrayList<ArrayList<Integer>>();
    ArrayList<String[]> read_data = new ArrayList<String[]>();

    public DSU() throws FileNotFoundException{
        this.read("in");
        int size = Integer.valueOf(this.read_data.get(0)[0]) + 1;

        parents = new int[size];
        ranks = new int[size];

        for(int i =0; i < size; ++i){
            parents[i] = i;
            ranks[i] = 0;
        }

        for(int i = 1; i < this.read_data.size(); ++i){
            String[] temp = read_data.get(i);
            this.insert_edge(Integer.valueOf(temp[0]),Integer.valueOf(temp[1]),Integer.valueOf(temp[2]));
        }
        return;
    }

    public void insert_edge(int a, int b, int w){
        ArrayList<Integer> temp = new ArrayList<>();
        temp.add(a);
        temp.add(b);
        temp.add(w);
        this.edges.add(temp);
    }

    public int find(int child){
        if(parents[child] == child) return child; 
        parents[child] = find(parents[child]);
        return parents[child];
    }

    public void union(int k, int v){
        int p1 = find(k);
        int p2 = find(v);
            dbg(p1, p2);

        if(p1==p2) return;
        if(ranks[p1] > ranks[p2]) parents[p2] = p1;
        else if(ranks[p1] < ranks[p2]) parents[p1] = p2;
        else{
            parents[p2] = p1;
            ranks[p1]++;
        }
        return;
    }

    public void union_parents(int p1, int p2){
        if(p1==p2) return;
        if(ranks[p1] > ranks[p2]) parents[p2] = p1;
        else if(ranks[p1] < ranks[p2]) parents[p1] = p2;
        else{
            parents[p2] = p1;
            ranks[p1]++;
        }
        return;
    }

    public int kruskal(){
        int ans = 0;
        this.edges.sort((a,b) -> {return a.get(2) >= b.get(2) ? 1 : -1;});
        for(ArrayList<Integer> i : this.edges){
            int p1 = this.find(i.get(0));
            int p2 = this.find(i.get(1));
            int w = i.get(2);

            if(p1 != p2){
                this.union_parents(p1, p2);
                ans += w;
            }
        }

        return ans;
    }

    private void read(String filename) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(filename));
        while (scanner.hasNextLine())
            this.read_data.add(scanner.nextLine().split("\\s+"));
        scanner.close();
    }
}
