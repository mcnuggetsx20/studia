import static aisd.util.Util.*;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;

import aisd.stack.FullStackException;
import aisd.stack.EmptyStackException;

import java.io.IOException;

public class wrapper {
    public onp expr;
    public String ifx;
    private tree tr;

    public wrapper(String ifx) throws IOException, FullStackException, EmptyStackException, FullQueueException, EmptyQueueException{
        this.ifx = ifx;
        this.expr = new onp(ifx);
    }

    public void makeTree() throws IOException, FullStackException, EmptyStackException, FullQueueException, EmptyQueueException{
        this.tr = new tree(this.expr.toString());
    }

    public Double calculate(){return this.tr.calculate(this.tr.head);}
    public int countLeaves(){return this.tr.countLeaves(this.tr.head);}
    public int countNodes(){return this.tr.countNodes(this.tr.head);}
    public int getHeight(){return this.tr.getHeight();}
    public String getIfx(){return this.ifx;}
    public String getRpn(){return this.expr.toString();}
    public void printTree(){System.out.println(this.tr);}
}
