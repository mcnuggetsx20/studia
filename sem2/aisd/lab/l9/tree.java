import static aisd.util.Util.*;

import aisd.queue.ListQueue;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;

import aisd.stack.ArrayStack;
import aisd.stack.EmptyStackException;
import aisd.stack.FullStackException;

import java.io.StreamTokenizer;
import java.io.IOException;
import java.io.StringReader;

public class tree{
    private class Node<T>{
        public T value;
        public Node left = null, right = null;
        public boolean isOperand;

        public Node(T val){
            this.value = val;
            this.isOperand = val.equals('+') || val.equals('-') || val.equals('*') || val.equals('/');
            //dbg(val, this.isOperand);

        }

        @Override
        public String toString(){return String.valueOf(this.value);}
    }

    public Node head = null;
    private int height = 0;

    public tree(String rpn_expression) throws EmptyStackException, FullStackException, IOException{
        ArrayStack<Node> stack = new ArrayStack<Node>(rpn_expression.length());
        StreamTokenizer arr = new StreamTokenizer(new StringReader(rpn_expression));
		arr.ordinaryChar('/');
        
        Node newNode = null;

        while(arr.nextToken() != arr.TT_EOF){
            if(arr.ttype != arr.TT_NUMBER){
                ++this.height;
                newNode = new Node<Character>((char)arr.ttype);
                newNode.right = stack.pop();
                newNode.left = stack.pop();
                stack.push(newNode);
                continue;
            }
            stack.push(new Node<String>(String.valueOf(arr.nval)));
        }

        this.head = newNode;
        return;
    }

    public Double calculate(Node node){
        if(node == null){return 0.0;}
        if(node.isOperand){
            Double ans = 0.0;

            if(node.value.equals('+')) ans = calculate(node.right) + calculate(node.left);
            else if(node.value.equals('-')) ans = calculate(node.left) - calculate(node.right);
            else if(node.value.equals('/')) ans = calculate(node.left) / calculate(node.right);
            else if(node.value.equals('*')) ans = calculate(node.right) * calculate(node.left);

            //dbg(node, ans);
            return ans;
        }
        return Double.valueOf(String.valueOf(node.value));

    }

    public int countLeaves(Node node){
        if(node ==null){return 0;}
        if(node.right==null && node.left==null){
            return 1;
        }
        int ans = 0;
        ans += countLeaves(node.right) + countLeaves(node.left);
        return ans;
    }

    public int countNodes(Node node){
        if(node ==null){return 0;}
        int ans = 1;
        ans += countNodes(node.right) + countNodes(node.left);
        return ans;
    }

    public int getHeight(){return this.height;}

    private String _print(Node node){
        if(node == null){return "";}
        String ans = "", r = "", l = "";
        ans = String.format("%s: %s, %s", node.value, node.left, node.right);
        ans += '\n';
        l += _print(node.left);
        r += _print(node.right);
        if(l.equals("null") && r.equals(null)){return "";}
        ans += l + r;
        return ans;
    }

    @Override
    public String toString(){
        return _print(head);
    }

}
