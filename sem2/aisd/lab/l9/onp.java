import static aisd.util.Util.*;
import aisd.queue.ListQueue;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;

import aisd.stack.ListStack;
import aisd.stack.FullStackException;
import aisd.stack.EmptyStackException;

import java.io.StreamTokenizer;
import java.io.IOException;
import java.io.StringReader;

public class onp{
    private class operand{
        public char value;
        public int priority = -1;
        public boolean isBracket;

        public operand(char v){
            this.value = v;
            this.priority = (v=='+' || v=='-') ? 0 : 1;
            this.isBracket = v=='(' || v==')';
        }

        public String toString(){
            return String.format("%c", this.value);
        }
    }

    ListQueue<Object> output = new ListQueue<>();
    ListStack<operand> stack = new ListStack<>();

    String stringVal = "";
    String normalStr = "";

    public onp(String str) throws IOException, FullStackException, EmptyStackException, FullQueueException, EmptyQueueException{
        StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(str));
        this.normalStr = str;

        tokenizer.ordinaryChar('(');
        tokenizer.ordinaryChar(')');
		tokenizer.ordinaryChar('/');
		tokenizer.ordinaryChar('-');

        while(tokenizer.nextToken() != tokenizer.TT_EOF) {
            //dbg(tokenizer.ttype);
            if(tokenizer.ttype=='(')
                stack.push(new operand((char)tokenizer.ttype));

            else if(tokenizer.ttype == tokenizer.TT_NUMBER)
                output.enqueue(tokenizer.nval);

            else if(tokenizer.ttype == ')'){
                while(stack.top().value != '('){
                    output.enqueue(stack.pop());
                }
                stack.pop();
            }

            //jesli nie pozostale to na pewno operator jakis
            else{
                operand op = new operand((char)tokenizer.ttype);
                while(!stack.isEmpty() && !stack.top().isBracket && stack.top().priority >= op.priority) 
                    output.enqueue(stack.pop());
                stack.push(op);
            }
        }
        while(!stack.isEmpty()) output.enqueue(stack.pop());
        while(!output.isEmpty()) this.stringVal += String.valueOf(output.dequeue()) + " ";
    }

    @Override
    public String toString(){
        return stringVal;
    }
}
