import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;

import aisd.stack.FullStackException;
import aisd.stack.EmptyStackException;

import java.io.IOException;

public class main{
    public static void dbg(Object... args){
        String delimiter = " ";
        for(Object i: args){
            delimiter = String.valueOf(i).equals("\n") ? "" : " ";
            System.out.printf("%s%s", String.valueOf(i), delimiter);
        }
        System.out.println();
        return;
    }

    public static void main(String args[]) throws IOException, FullStackException, EmptyStackException, FullQueueException, EmptyQueueException{
        wrapper w = new wrapper("((4+3)-(2+1)*2+3)/2");
        w.makeTree();
        dbg("Wartosc wyrazenia: ", w.calculate());
        dbg("Liczba lisci: ", w.countLeaves());
        dbg("Liczba wezlow: ", w.countNodes());
        dbg("Wysokosc: ", w.getHeight());
        dbg("Postac infiksowa", w.getIfx());
        dbg("Postac ONP: ", w.getRpn());
        //w.printTree();
    }
}
