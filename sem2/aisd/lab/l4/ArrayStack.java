import java.lang.reflect.Array;

public class ArrayStack<T> implements IStack<T>{
    T[] arr;
    private int index=-1;

    @SuppressWarnings("unchecked")
    public ArrayStack(int size){
        arr = (T[]) (new Object[size]);
    }

    @Override
    public boolean isEmpty(){return index == -1;}
    
    @Override
    public boolean isFull(){return index == arr.length-1;}

    @Override
    public void push(T item) throws StackFullException{
        if(isFull()){throw new StackFullException();}
        arr[++index] = item;
    }

    @Override
    public T pop() throws StackEmptyException{
        if(isEmpty()){throw new StackEmptyException();}
        return arr[index--];
    }

    @Override
    public T top() throws StackEmptyException{
        if(isEmpty()){throw new StackEmptyException();}
        return arr[index];
    }

    @Override
    public int size(){return index+1;}

    public void reverse(){
        for(int i = 0; i < size()/2; ++i){
            T temp = arr[i];
            arr[i] = arr[size() -i-1];
            arr[size() -i-1] = temp;
        }
    }

    public String toString(){
        String ans = "";
        for(int i = index; i > -1; --i){
            ans += String.format("%2s ", arr[i]);
        }
        return ans;
    }
}
