public interface IStack<T>{
    boolean isEmpty();
    boolean isFull();
    void push(T item) throws StackFullException;
    T pop() throws StackEmptyException;
    int size();
    T top() throws StackEmptyException;
}
