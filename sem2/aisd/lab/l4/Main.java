import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Main{
    public static void main(String[] args) throws StackEmptyException, StackFullException, FileNotFoundException{
        Nawiasy n = new Nawiasy();
        String test1 = "(w*[x+y]/z-[p/{r-q}])",
              test2 = "(w*[x+y)/z-[p/{r-q}])";

        //System.out.println(n.balanced(test1));
        //System.out.println(n.balanced(test2));

        File file = new File("in.txt");
        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            System.out.println(n.balanced(line));
        }
    }
}
