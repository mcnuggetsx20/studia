import java.util.Scanner;

public class ReverseStackExamples{

    public static boolean palindromeCheck() throws StackFullException, StackEmptyException{
        System.out.printf("Czy podane slowo jest palindromem? : ");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        scanner.close();

        char[] str = s.toCharArray();

        ArrayStack<Character> revstack = new ArrayStack<>(s.length() +1);
        ArrayStack<Character> stack = new ArrayStack<>(s.length() +1);

        for(char i: str){
            stack.push(i);
            revstack.push(i);
        }
        revstack.reverse();

        while(!stack.isEmpty() && !revstack.isEmpty()){
            if(stack.pop() != revstack.pop())
                return false;
        }
        return true;
    }

    public static void moveWithHelp() throws StackFullException, StackEmptyException{
        System.out.println("przeniesienie elementow stosu z pomoca");
        ArrayStack<Integer> stack1 = new ArrayStack<>(10);
        ArrayStack<Integer> stack2 = new ArrayStack<>(10);
        ArrayStack<Integer> help = new ArrayStack<>(10);

        stack1.push(1);
        stack1.push(4);
        stack1.push(5);
        stack1.push(9);

        while(!stack1.isEmpty()){
            Integer temp = stack1.pop();
            help.push(temp);
            System.out.println(temp);
        }
        System.out.println();

        while(!help.isEmpty())
            stack2.push(help.pop());

        //wypisywanie
        while(!stack2.isEmpty())
            System.out.println(stack2.pop());

    }

    public static void move() throws StackFullException, StackEmptyException{
        System.out.println("przeniesienie elementow stosu na samych zmiennych");
        ArrayStack<Integer> stack1 = new ArrayStack<>(10);
        ArrayStack<Integer> stack2 = new ArrayStack<>(10);

        stack1.push(1);
        stack1.push(4);
        stack1.push(5);
        stack1.push(9);

        while(!stack1.isEmpty()){
            Integer temp  = stack1.pop();
            stack2.push(temp);
            System.out.println(temp);
        }
        System.out.println();

        stack2.reverse();
        //wypisywanie
        while(!stack2.isEmpty())
            System.out.println(stack2.pop());

    }

    public static void main(String[] args) throws StackFullException, StackEmptyException{

        System.out.println(palindromeCheck());
        //moveWithHelp();
        System.out.println();
        //move();

        return;
    }
}
