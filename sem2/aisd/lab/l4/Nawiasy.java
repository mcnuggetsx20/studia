public class Nawiasy{

    boolean opening(char ch){return ch == '(' || ch == '{' || ch == '[';}
    boolean closing(char ch){return ch == ')' || ch == '}' || ch == ']';}

    public boolean balanced(String s) throws StackEmptyException, StackFullException{
        ArrayStack<Character> stack = new ArrayStack<>(100);
        char[] str = s.toCharArray();

        for(char ch : str){
            //System.out.println(stack);
            if(opening(ch))
                stack.push(ch);

            else if(closing(ch)){

                if(stack.isEmpty()){return false;}

                //System.out.printf("%d, %d\n", (int)ch, (int)stack.top());
                if(opening(stack.top()) && Math.abs((int)ch - (int)stack.top()) <=2)
                    stack.pop();

                else
                    break;
            }
        }

        if(stack.isEmpty())
            return true;
        return false;
    }
}
