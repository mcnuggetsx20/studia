import java.util.Random;

public class Produkt{
    String nazwa;
    int cena;

    String names[] = {
        "wiertarka",
        "laptop",
        "zestaw srubek",
        "mlotek",
        "rekawiczki ochronne",
        "latarka",
        "tasma klejaca",
        "zestaw gwozdzi",
        "kombinerki",
        "zestaw srubokretow",
    };

    public Produkt(){
        Random rand = new Random();
        
        this.nazwa = names[rand.nextInt(names.length)];
        this.cena = rand.nextInt(1000) + 1;
    }

    public String getNazwa(){return nazwa;}
    public int getCena(){return cena;}
}
