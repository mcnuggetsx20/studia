import aisd.queue.EmptyQueueException;
import aisd.queue.FullQueueException;

public class Main{
    public static void main(String [] args) throws FullQueueException, EmptyQueueException{

        Firma f = new Firma();

        Magazyn m1 = new Magazyn(1);
        Magazyn m2 = new Magazyn(2);
        Magazyn m3 = new Magazyn(3);

        m1.add( new Klient("John", "Smith", 5) );
        m1.add( new Klient("Alice", "Johnson", 7) );
        m1.add( new Klient("Bob", "Brown", 3) );

        m2.add( new Klient("Emily", "Davis", 6) );
        m2.add( new Klient("Michael", "Miller", 4) );
        m2.add( new Klient("Emma", "Wilson", 8) );

        m3.add( new Klient("David", "Taylor", 2) );
        m3.add( new Klient("Olivia", "Anderson", 9) );
        m3.add( new Klient("James", "Thomas", 5) );
        m3.add( new Klient("Sophia", "Martinez", 7) );

        f.add(m1); f.add(m2); f.add(m3);

        System.out.println(f.calculate());

        return;
    }
}
