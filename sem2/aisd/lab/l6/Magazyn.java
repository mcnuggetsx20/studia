import aisd.queue.OneWayListQueue;
import aisd.queue.EmptyQueueException;
import aisd.queue.FullQueueException;

public class Magazyn{
    OneWayListQueue<Klient> queue = new OneWayListQueue<>();

    int id;

    public Magazyn(int n){
        this.id = n;
    }

    public void add(Klient k) throws FullQueueException{
        queue.enqueue(k);
    }

    public int unload() throws EmptyQueueException{
        int ans = 0, ret = 0;
        while(!queue.isEmpty()){
            ans = 0;
            Klient temp = queue.dequeue();
            Produkt p = temp.placeOrder();
            while(p != null){
                ans += p.getCena();
                p = temp.placeOrder();
            }
            System.out.printf("\tZlecenie zrealizowane:\n%s\nKwota do zaplaty: %5d\n", temp.toString(), ans);
            ret += ans;
        }
        System.out.printf("Magazyn %d: %d", this.id, ret);
        return ret;
    }
}
