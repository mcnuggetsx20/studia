import aisd.queue.OneWayListQueue;
import aisd.queue.FullQueueException;
import aisd.queue.EmptyQueueException;
import java.util.Random;

public class Klient{
    OneWayListQueue<Produkt> queue = new OneWayListQueue<>();
    String imie,
           nazwisko;

    public Klient(String imie, String nazwisko, int n) throws FullQueueException{
        this.imie = imie;
        this.nazwisko = nazwisko;
        fill(n);
    }

    public String getImie(){return imie;}
    public String getNazwisko(){return nazwisko;}
    public String toString(){return String.format("%15s%15s", imie, nazwisko);}

    public void fill(int n) throws FullQueueException{
        while(n-- > 0) queue.enqueue(new Produkt());
    }

    public Produkt placeOrder() throws EmptyQueueException{
        if(queue.isEmpty()) return null;
        return queue.dequeue();
    }

}
