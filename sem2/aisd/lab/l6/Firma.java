import aisd.queue.OneWayListQueue;
import aisd.queue.EmptyQueueException;
import aisd.queue.FullQueueException;
import java.util.ArrayList;

public class Firma{
    OneWayListQueue<Magazyn> queue = new OneWayListQueue<>();

    public void add(Magazyn m) throws FullQueueException{
        queue.enqueue(m);
    }

    public int calculate() throws EmptyQueueException{
        ArrayList<Magazyn> arr = new ArrayList<>();

        int ans = 0;
        while(!queue.isEmpty()){
            Magazyn temp = queue.dequeue();
            ans += temp.unload();

            System.out.println("\n");
        }
        return ans;
    }
}

