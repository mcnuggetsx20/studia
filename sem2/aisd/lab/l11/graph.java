import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.TreeSet;
import java.util.stream.Stream;
import static aisd.util.Util.*;

public class graph{

    private class pair<T extends Comparable<T>, E extends Comparable<E>> implements Comparable<pair<T, E>>{
        public T first;
        public E second;

        public pair(T a, E b){
            this.first = a;
            this.second = b;
        }

        @Override 
        public String toString(){
            return String.format("(%s, %s)", first, second);
        }

        @Override 
        public int compareTo(pair<T, E> other_pair){
            int cmp2 = this.second.compareTo(other_pair.second);
            int cmp1 = this.first.compareTo(other_pair.first);
            if( (cmp1 == 0 && cmp2 ==0) || cmp1 == 0) return 0;
            if(cmp2 == 0) return cmp1;
            return cmp2;
        }
    }

    ArrayList<String[]> read_data = new ArrayList<String[]>();
    ArrayList<ArrayList<pair<Integer, Integer>>> adj = new ArrayList<ArrayList<pair<Integer, Integer>>>();
    public TreeSet<pair<Integer,Integer>> distances = new TreeSet<>();
    public boolean[] visited;

    private void read(String filename) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(filename));
        while (scanner.hasNextLine())
            this.read_data.add(scanner.nextLine().split("\\s+"));
        scanner.close();
    }

    public int source = 0;
    public int size;

    public graph(int size) throws FileNotFoundException{
        this.size = size;
        this.read("in");
        this.visited = new boolean[size];

        for(int i = 0; i < size; ++i){
            this.adj.add(new ArrayList<pair<Integer, Integer>>());
            this.visited[i] = false;
            if(i == this.source) continue;
            this.distances.add(new pair<Integer, Integer>(i, Integer.MAX_VALUE));
        }
        distances.add(new pair<Integer, Integer>(this.source, 0));

        for( String[] i : this.read_data){
            Integer root = Integer.valueOf(i[0]);
            Integer neigh = Integer.valueOf(i[1]);
            Integer dist = Integer.valueOf(i[2]);
            this.adj.get(root).add(new pair<Integer, Integer>(neigh, dist));
        }

        //dbg(adj);
    }

    public void dijkstra(){
        for(int i =0; i < this.size; ++i){
            //dbg(this.distances);

            pair<Integer, Integer> mn = this.distances.pollFirst();
            visited[mn.first] = true;

            printAns(mn);

            for(pair<Integer, Integer> j: adj.get(mn.first)){
                if(visited[j.first]) continue;
                int newDist = j.second + mn.second;
                Stream<pair<Integer, Integer>> pairStream = distances.stream().filter(pr -> pr.first.equals(j.first));
                pair<Integer, Integer> foundPair = pairStream.findFirst().orElse(null);
                if(foundPair.second.compareTo(newDist) > 0 && !mn.second.equals(Integer.MAX_VALUE)){
                    this.distances.remove(foundPair);
                    this.distances.add(new pair<Integer, Integer>(j.first, newDist));
                }
            }
        }
        this.visited = new boolean[size];
        return;
    }

    void dfs(Integer src, ArrayList<Integer> arr){
        if(visited[src] || src==null) return;
        visited[src] = true;
        for(pair<Integer, Integer> i : adj.get(src)) dfs(i.first, arr);
        arr.add(src);
        return;
    }

    private void printAns(pair<Integer, Integer> ans){
        if(ans.second.equals(Integer.MAX_VALUE)) return;
        String[] cities = {"Wroclaw", "Olawa", "Brzeg", "Nysa", "Opole"};
        System.out.printf("%-9s%4d\n", cities[ans.first], ans.second);
        return;
    }

    public void printAdj(){
        for(ArrayList<pair<Integer,Integer>> i : adj){
            System.out.println(i);
        }
    }
}
