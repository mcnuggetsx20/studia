import java.io.FileNotFoundException;

import java.util.ArrayList;

import static aisd.util.Util.*;

public class main{
    public static void main(String[] args) throws FileNotFoundException{
        graph a = new graph(7);
        a.dijkstra();
        ArrayList<Integer> ans = new ArrayList<>();
        a.dfs(a.source, ans);
        dbg("dfs: ", ans);

        a.printAdj();

    }
}
