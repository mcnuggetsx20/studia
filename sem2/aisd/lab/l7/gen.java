import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.Random;
import java.io.IOException;

public class gen{

    public gen(int N) throws IOException{
        int MAX = 10000000;
        Random rand = new Random();

        FileWriter fw = new FileWriter("in");
        BufferedWriter bw = new BufferedWriter(fw);
        while(N-- >0){
            bw.write(String.valueOf(rand.nextInt(MAX)));
            bw.newLine();
        }

        bw.close();
    }

    public static void main(String[] args) throws IOException{
        gen a = new gen(10);
    }
}
