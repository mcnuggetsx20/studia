from random import randint
MAX = 10**9
N = 10000
tab = [randint(0, MAX) for i in range(N)]
sec = list(map(str, tab))
sor = list(map(str, sorted(tab)))

with open('in', 'w') as file:
    file.write('\n'.join(sec))

with open('in_rosn', 'w') as file:
    file.write('\n'.join(sor))

with open('in_mal', 'w') as file:
    file.write('\n'.join(sor[::-1]))
