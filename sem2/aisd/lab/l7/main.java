import java.io.FileNotFoundException;
public class main{
    public static void main(String[] args) throws FileNotFoundException{
        Integer tab[] = {1 ,76 ,2 , 67, 3, 5, 10};

        sort srt = new sort("in");
        long start;

        System.out.printf("Paczka zawiera %d liczb:\nDane losowe:\n", srt.getSize());

        start = System.nanoTime(); srt.bubbleSort(); System.out.printf("%40s: %40d\n", "Bubble sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.insertSort(); System.out.printf("%40s: %40d\n", "Insertion sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.selectSort(); System.out.printf("%40s: %40d\n", "Selection sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.heapSort(); System.out.printf("%40s: %40d\n", "Heap sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.mergeSort(); System.out.printf("%40s: %40d\n", "Merge sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.quickSort(); System.out.printf("%40s: %40d\n", "Quick sort [O(nlogn)]", -start+System.nanoTime());

        srt = new sort("in_mal");

        System.out.printf("\nDane nierosnace:\n");

        start = System.nanoTime(); srt.bubbleSort(); System.out.printf("%40s: %40d\n", "Bubble sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.insertSort(); System.out.printf("%40s: %40d\n", "Insertion sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.selectSort(); System.out.printf("%40s: %40d\n", "Selection sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.heapSort(); System.out.printf("%40s: %40d\n", "Heap sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.mergeSort(); System.out.printf("%40s: %40d\n", "Merge sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.quickSort(); System.out.printf("%40s: %40d\n", "Quick sort [O(nlogn)]", -start+System.nanoTime());

        srt = new sort("in_rosn");

        System.out.printf("\nDane niemalejace:\n");

        start = System.nanoTime(); srt.bubbleSort(); System.out.printf("%40s: %40d\n", "Bubble sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.insertSort(); System.out.printf("%40s: %40d\n", "Insertion sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.selectSort(); System.out.printf("%40s: %40d\n", "Selection sort [O(n^2)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.heapSort(); System.out.printf("%40s: %40d\n", "Heap sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.mergeSort(); System.out.printf("%40s: %40d\n", "Merge sort [O(nlogn)]", -start+System.nanoTime());
        start = System.nanoTime(); srt.quickSort(); System.out.printf("%40s: %40d\n", "Quick sort [O(nlogn)]", -start+System.nanoTime());

    }
}
