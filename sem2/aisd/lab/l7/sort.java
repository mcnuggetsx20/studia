import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class sort{

    private class Heap{
        Integer N[];
        int max_index;

        int INF = 1000000000;

        //domyslnie tworzymy minHeap
        public Heap(Integer[] arr){
            double lg = Math.log( arr.length ) / Math.log(2);
            int size = (int)Math.ceil(lg) + (lg == (int)lg ? 1: 0);

            N = new Integer[Math.max( (int)Math.pow(2, size) -1 , 1)];
            Arrays.fill(N, INF);

            for(int i = 0; i < arr.length; ++i){
                N[i] = arr[i];
                fix(i);
            }

            max_index = arr.length -1;
        }

        public void fix(int index){
            int parent = (int)((index-1)/2);
            if(N[index].compareTo(N[parent]) >= 0) return;

            Integer temp = N[index];
            N[index]  = N[parent];
            N[parent] = temp;
            fix(parent);
        }

        public void fixAll(int parent){
            //System.out.println(Arrays.toString(N));

            if(N.length-1 <= parent*2+1) return;
            Integer mn = Math.min( N[ parent*2+1], N[ parent*2 + 2]);
            if(N[parent].compareTo(mn) <= 0) return;

            int index = parent*2+1 + (mn.equals(N[parent*2+1]) ? 0 : 1);

            Integer temp = N[parent];
            N[parent] = N[index];
            N[index] = temp;
            fixAll(index);
        }

        public boolean isEmpty(){
            return max_index < 0;
        }

        public int swap(){
            Integer res = N[0];
            N[0] = N[max_index];
            N[max_index--]=INF;
            return res;
        }
    }
    
    Integer N[];

    public sort(String filename) throws FileNotFoundException{
        this.N = read(filename);
    }

    public void printArr(Integer arr[], String message){
        System.out.printf("%20s%40s\n", message, Arrays.toString(arr));
    }

    private Integer[] read(String filename) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(filename));
        ArrayList<Integer> arr = new ArrayList<>();
        
        while(scanner.hasNextLine()){
            String temp = scanner.nextLine();
            arr.add(Integer.valueOf(temp));
        }

        scanner.close();
        return arr.toArray(new Integer[0]);
    }

    public Integer[] bubbleSort(){
        Integer[] arr = N.clone();
        boolean stop = false;

        while(!stop){
            stop = true;
            for(int i = 0; i < arr.length-1; ++i){
                if(arr[i].compareTo(arr[i+1]) > 0){
                    stop = false;
                    Integer temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
        return arr;
    }

    public Integer[] insertSort(){
        Integer[] arr = N.clone();

        for(int i = 1; i < arr.length; ++i){
            Integer curr = arr[i];
            int j;
            for(j = i-1; j >-1; --j){
                //System.out.println(j);
                if(arr[j].compareTo(curr) <= 0) break;
                arr[j+1] = arr[j];
            }
            arr[j+1] = curr;
        }
        return arr;
    }

    public Integer[] selectSort(){
        Integer[] arr = N.clone();
        Integer mn;
        int ind;
        for(int i =0; i < arr.length; ++i){
            mn = 1000000000;
            ind = 0;
            for(int j = i; j < arr.length; ++j){
                if(mn > arr[j]){
                    mn = arr[j];
                    ind = j;
                }
            }
            Integer temp = arr[ind];
            arr[ind] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }

    public Integer[] heapSort(){
        Integer[] arr = N.clone();
        Heap h = new Heap(arr);

        for(int i = 0; i < arr.length && !h.isEmpty(); ++i){
            arr[i] = h.swap();
            h.fixAll(0);
        }

        return arr;
    }

    private void merge(Integer[] arr, int l, int r){
        int m = (int)((l+r)/2);
        int i =l, j = m+1;
        int index = 0;

        Integer[] temp = new Integer[r+1-l];

        while(i < m+1 && j < r+1){
            if(arr[i] < arr[j]) temp[index] = arr[i++];
            else temp[index] = arr[j++];
            ++index;
        }
        //for(i; i < a1.length; ++i) res[i+j]=a1[i];
        //for(j; j < a2.length; ++j) res[i+j]=a2[j];
        while(i < m+1){
            temp[index] = arr[i];
            ++i; ++index;
        }

        while(j < r+1){
            temp[index] = arr[j];
            ++j; ++index;
        }
        for(i =0; i <temp.length; ++i) arr[i+l] = temp[i];

    }

    private void _mergeSort(Integer[] arr, int l, int r){
        //System.out.printf("%d, %d\n", l, r);
        if(l==r) return;
        int m = (int)((l+r)/2);
        _mergeSort(arr, l, m);
        _mergeSort(arr, m+1, r);
        //System.out.printf("bedzie mergowane %d, %d\n", l, r);
        merge(arr, l, r);
    }

    public Integer[] mergeSort(){
        Integer[] arr = N.clone();
        _mergeSort(arr, 0, arr.length-1);
        return arr;
    }

    public int partition(Integer arr[], int l, int r){
        Integer pivot = arr[r-1], temp;
        int k = l;
        for(int i = l; i < r; ++i){
            if(arr[i].compareTo(pivot) > 0) continue;

            temp = arr[i];
            arr[i] = arr[k];
            arr[k] = temp;

            ++k;
        }

        return Math.max(--k, 0);
    }
    public void _quickSort(Integer arr[], int l, int r){
        if(l>=r-1) return;

        int pivot = partition(arr, l, r);

        _quickSort(arr, l, pivot);
        _quickSort(arr, pivot, r);

        return;
    }
    
    public Integer[] quickSort(){
        Integer[] arr = N.clone();
        _quickSort(arr, 0, arr.length);

        return arr;
    }
    
    public int getSize(){return N.length;}
}
