import java.util.Iterator;
import java.util.ListIterator;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main{
    public static void main(String[] args) throws FileNotFoundException, IOException, NoSuchFieldException,IllegalAccessException {
        Menu menu = new Menu();
        menu.show();
        menu.listen();
    }
}
