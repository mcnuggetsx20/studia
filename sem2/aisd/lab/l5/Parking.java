import aisd.stack.*;

public class Parking{
    ListStack<Pracownik> mainStack = new ListStack<>();
    ListStack<Pracownik> helpStack = new ListStack<>();

    public void enqueue(Pracownik n) throws EmptyStackException, FullStackException{mainStack.push(n);}
    public void dequeue() throws EmptyStackException, FullStackException{
        while(mainStack.size() > 1){
            Pracownik temp = mainStack.pop();
            System.out.printf("Musi wyjechac: ");
            System.out.println(temp);
            helpStack.push(temp);
        }
        Pracownik h = mainStack.pop();
        System.out.printf("Wyjezdza: ");
        System.out.println(h);
        while(!helpStack.isEmpty()) mainStack.push(helpStack.pop());
        System.out.println();
    }
}
