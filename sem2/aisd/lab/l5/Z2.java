import aisd.stack.*;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Z2{
    public static void main(String [] args) throws EmptyStackException, FullStackException, IOException, FileNotFoundException{
        ArrayList<Pracownik> arr = new ArrayList<>();

        File file = new File("in.txt");
        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            String[] elems = line.split(";");

            //System.out.println(elems.length);
            arr.add(new Pracownik(elems[0], elems[1], elems[2], elems[3], Long.valueOf(elems[4]), Long.valueOf(elems[5]), Long.valueOf(elems[6])));
        }
        scanner.close();

        Parking p = new Parking();
        p.enqueue(arr.get(1));
        p.enqueue(arr.get(2));
        p.enqueue(arr.get(3));
        p.enqueue(arr.get(4));
        p.enqueue(arr.get(5));

        p.dequeue();
        p.enqueue(arr.get(7));
        p.dequeue();
        p.dequeue();
        return;
    }
}
