import java.lang.reflect.Field;
public class Pracownik{
    Long PESEL,
         pensja,
         staz;
    
    Double premia;

    String nazwisko,
           imie,
           data_urodzenia,
           stanowisko;

    public Pracownik(String imie, String nazwisko, String data_urodzenia, String stanowisko, Long pesel, Long staz, Long pensja){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.data_urodzenia = data_urodzenia;
        this.stanowisko = stanowisko;
        this.PESEL = pesel;
        this.staz = staz;
        this.pensja = pensja;
    }

    public Double obliczPremie(){
        Double ans = pensja * (staz>=20 ? 0.2 : 0.1) * (staz < 10 ? 0 : 1);
        premia = ans;
        return ans;
    }

    public String toString(){return String.format("%-15s%-15s%-20s%-20s%15d%8d%8d%15d", imie, nazwisko, data_urodzenia, stanowisko, PESEL, staz, pensja, premia);}
    public String toStringFile(){return String.format("%s;%s;%s;%s;%d;%d;%d", imie, nazwisko, data_urodzenia, stanowisko, PESEL, staz, pensja);}

    public Long getPensja(){return pensja;}
    public Long getPesel(){return PESEL;}
    public Long getStaz() { return staz; }
    public Double getPremia() { return premia; }
    public String getNazwisko() { return nazwisko; }
    public String getImie() { return imie; }
    public String getDataUrodzenia() { return data_urodzenia; }
    public String getStanowisko() { return stanowisko; }

    public void setFieldValue(String fieldName, String value) throws NoSuchFieldException, IllegalAccessException{
        try{
            Field field = Pracownik.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            if (field.getType() == Long.class) {
                field.set(this, Long.valueOf(value));
            } else if (field.getType() == Double.class) {
                field.set(this, Double.valueOf(value));
            } else if (field.getType() == String.class) {
                field.set(this, value);
            }
        } catch (NoSuchFieldException e) {
            System.out.println("zla nazwa pola!");
        }
        return;
    }
}
