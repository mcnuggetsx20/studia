import java.util.Iterator;
import java.util.ListIterator;
public class TwoWayCycledListWithSentinel<T>{
    Element sentinel = null;

    private class Element{
        private T value;
        private Element next, prev;

        Element(T data){value = data;}

        public T getValue(){return value;}
        public Element getNext(){return next;}
        public Element getPrev(){return prev;}

        public void setValue(T data){this.value = data;}
        public void setNext(Element data){this.next = data;}
        public void setPrev(Element data){this.prev = data;}

        public void insertAfter(Element item){
            item.setNext(this.getNext());
            item.setPrev(this);

            this.getNext().setPrev(item);
            this.setNext(item);
            return;
        }

        public void insertBefore(Element item){
            item.setPrev(this.getPrev());
            item.setNext(this);

            this.getPrev().setNext(item);
            this.setPrev(item);
            return;
        }

        public void remove(){
            this.getPrev().setNext(this.getNext());
            this.getNext().setPrev(this.getPrev());
            return;
        }
    }

    private class TWCIterator implements Iterator<T>{
        Element current = sentinel;
        public boolean hasNext(){return current.getNext() != sentinel;}
        public T next(){
            T ans = current.getNext().getValue();
            current = current.getNext();
            return ans;
        }
    }

    private class TWCListIterator implements ListIterator<T>{
        boolean lastDirection = false;
        Element current=  sentinel;

        public boolean hasNext(){return current.getNext()!=sentinel;}
        public boolean hasPrevious(){return current != sentinel;}
        
        public int nextIndex(){throw new UnsupportedOperationException();}
        public int previousIndex(){throw new UnsupportedOperationException();}

        public T next(){
            lastDirection = false;
            current = current.getNext();
            return current.getValue();
        }

        public T previous(){
            lastDirection = true;
            current = current.getPrev();
            return current.getValue();
        }

        public void remove(){
            System.out.println(current.getValue());
            Element temp;
            temp = lastDirection ? current.getNext() : current.getPrev();
            current.remove();
            current = temp;
            return;
        }

        public void add(T data){
            Element temp = new Element(data);
            current.insertAfter(temp);
            current = current.getNext();
            return;
        }

        public void set(T data){
            current.setValue(data);
            return;
        }
    }


    public TwoWayCycledListWithSentinel(){
        sentinel = new Element(null);
        sentinel.setNext(sentinel);
        sentinel.setPrev(sentinel);
        return;
    }

    public Element getElement(int index){
        if(index < 0) throw new IndexOutOfBoundsException();

        int counter =0;
        Element temp = sentinel.getNext();

        while(temp != sentinel && counter < index){
            temp = temp.getNext();
            ++counter;
        }

        if(temp==sentinel) throw new IndexOutOfBoundsException();
        return temp;
    }

    private Element getElement(T value){
        Element temp = sentinel.getNext();

        while(temp!=sentinel && !temp.getValue().equals(value)) temp = temp.getNext();
        if(temp==sentinel) return null;
        return temp;
    }

    public boolean isEmpty(){return sentinel.getNext() == sentinel;}

    public boolean contains(T value){return getElement(value) == null ? false : true;}

    public void clear(){
        sentinel.setNext(sentinel);
        sentinel.setPrev(sentinel);
        return;
    }

    public T get(int index){ return this.getElement(index).getValue();}

    public T set(int index, T value){
        Element temp = getElement(index);
        T res = temp.getValue();
        temp.setValue(value);
        return res;
    }

    public boolean add(T value){
        sentinel.insertBefore(new Element(value));
        return true;
    }

    public boolean add(int index, T value){
        Element temp = getElement(index);
        temp.insertBefore(new Element(value));
        return true;
    }

    public int indexOf(T value){
        Element temp = sentinel.getNext();
        int counter = 0;

        while(temp!=sentinel && !temp.getValue().equals(value)){
            ++counter;
            temp = temp.getNext();
        }

        if(temp==sentinel) return -1;
        return counter;
    }

    public T remove(int index){
        Element temp = getElement(index);
        temp.remove();
        return temp.getValue();
    }

    public boolean remove(T value){
        Element temp = getElement(value);
        if(temp == null) return false;
        temp.remove();
        return true;
    }

    public int size(){
        Element temp = sentinel.getNext();
        int counter = 0;
        while(temp != sentinel){
            ++counter;
            temp = temp.getNext();
        }
        return counter;
    }

    public Iterator<T> iterator(){return new TWCIterator();}
    public ListIterator<T> listIterator(){return new TWCListIterator();}
}
