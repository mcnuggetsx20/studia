import java.util.Scanner;
import java.lang.NumberFormatException;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Menu{
    static Scanner scanner = new Scanner(System.in);
    String[] OPTIONS;

    public Menu(){
        String[] arr = {
            "1. Utworzenie nowej bazy danych",
            "2. Odczyt bazy z pliku",
            "3. Wyświetlenie wszystkich rekordów",
            "4. Wyświetlenie danych jednego pracownika",
            "5. Dopisanie nowego pracownika",
            "6. Usunięcie pracownika z bazy",
            "7. Aktualizowanie danych pracownika",
            "8. Obliczanie średniej pensji w firmie",
            "9. Obliczanie ilu pracowników zarabia poniżej średniej",
            "10. Zapis bazy do pliku",
        };

        OPTIONS = new String[arr.length];
        System.arraycopy(arr, 0, OPTIONS, 0, arr.length);
    }

    public void show(){
        System.out.println("         __  __ _____ _   _ _   _");
        System.out.println("        |  \\/  | ____| \\ | | | | |");
        System.out.println("        | |\\/| |  _| |  \\| | | | |");
        System.out.println("        | |  | | |___| |\\  | |_| |");
        System.out.println("        |_|  |_|_____|_| \\_|\\___/ ");

        System.out.println("Opcje:");
        for(String option : OPTIONS){System.out.println(option);}
    }

    public void listen() throws NumberFormatException, FileNotFoundException, IOException, NoSuchFieldException,IllegalAccessException{
        System.out.printf("-> ");
        String input;
        int ind;

        String inputFile = "in.txt",
               outputFile = "out.txt";

        String exception = "Twoja baza jest pusta!";

        Baza db = new Baza();
        while(!(input=scanner.nextLine()).equals("-1")){
            switch(input){

                case "1":
                    db.init();
                    System.out.printf("Zainicjowano baze\n");
                    break;

                case "2":
                    db.read(inputFile);
                    break;

                case "-2":
                    show();
                    break;

                default:
                    if(db.size() == 0){System.out.println(exception); break;}
                    switch(input){
                        case "3":
                            db.display();
                            break;

                        case "4":
                            ind = Integer.valueOf(listenArgument()) - 1;
                            if(ind < 1){System.out.println("nieprawidlowy index"); break;}
                            db.displayAt(ind);
                            break;

                        case "5":
                            String nazwisko = listenArgument("nazwisko: "),
                                   imie = listenArgument("imie: "),
                                   data_urodzenia = listenArgument("data urodzenia: "),
                                   stanowisko = listenArgument("stanowisko: ");

                            Long PESEL = Long.valueOf(listenArgument("pesel: ")),
                                 pensja = Long.valueOf(listenArgument("pensja: ")),
                                 staz = Long.valueOf(listenArgument("staz: "));

                            Pracownik temp = new Pracownik(imie, nazwisko, data_urodzenia, stanowisko, PESEL, staz, pensja);

                            db.add(temp);
                            break;
                        
                        case "6":
                            ind = Integer.valueOf(listenArgument()) - 1;
                            if(ind < 1){System.out.println("nieprawidlowy index"); break;}
                            db.remove(ind);
                            System.out.printf("Usunieto pracownika");
                            break;

                        case "7":
                            ind = Integer.valueOf(listenArgument("ktorego pracownika chcesz zaktualizowac: ")) - 1;
                            if(ind < 1){System.out.println("nieprawidlowy index"); break;}
                            String param = listenArgument("ktory parametr pracownika chcesz zmienic: "),
                                   value = listenArgument("nowa wartosc: ");


                            db.update(ind, param, value);
                            break;

                        case "8":
                            System.out.printf("Srednia pensja w firmie:\n%.4f\n", db.avgPensja());
                            break;

                        case "9":
                            System.out.printf("Liczba zarabiajacych ponizej sredniej:\n%d\n", db.ilePonizej());
                            break;

                        case "10":
                            db.write(outputFile);
                            break;

                        default:
                            System.out.println("Nie ma takiej opcji!");


                     }
            }
            System.out.printf("-> ");
        }
        System.out.println("Opuszczanie...");
        System.exit(0);

    }

    public static String listenArgument(){
        System.out.printf("++ ");
        String input = scanner.nextLine();
        return input;
    }

    public static String listenArgument(String msg){
        System.out.printf(msg);
        String input = scanner.nextLine();
        return input;
    }
}
