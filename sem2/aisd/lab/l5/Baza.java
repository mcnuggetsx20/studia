import java.util.Iterator;
import java.util.ListIterator;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Baza{
    TwoWayCycledListWithSentinel<Pracownik> arr = new TwoWayCycledListWithSentinel<>();

    public void init(){arr = new TwoWayCycledListWithSentinel<>();}

    public void add(Pracownik p){
        if(arr.size()==0){
            arr.add(p);
            return;
        }

        ListIterator<Pracownik> iter = arr.listIterator();
        while(iter.hasNext()){
            if(iter.next().getPesel() > p.getPesel()){
                iter.previous();
                iter.add(p);
                iter.next();
                return;
            }
        }
        arr.add(p);
    }

    public void remove(int index){arr.remove(index);}

    public void displayAt(int index){
        System.out.println(arr.get(index));
    }

    public void update(int index, String param, String value) throws NoSuchFieldException, IllegalAccessException{
        Iterator<Pracownik> iter = arr.iterator();
        int counter = -1;
        while(iter.hasNext()){
            counter++;
            Pracownik temp = iter.next();
            if(counter == index){
                temp.setFieldValue(param, value);
                remove(index);
                add(temp);
                return;
            }
        }
    }

    public double avgPensja(){
        int p = 0,
            q = 0;
        
        Iterator<Pracownik> iter = arr.iterator();
        while(iter.hasNext()){
            Pracownik temp = iter.next();
            p += temp.getPensja() + temp.obliczPremie();
            ++q;
        }
        if(q==0) return 0.0;
        return p/q;
    }

    public int ilePonizej(){
        int ans = 0;
        double avg = avgPensja();
        Iterator<Pracownik> iter = arr.iterator();
        while(iter.hasNext()){
            Pracownik temp = iter.next();
            ans += (temp.getPensja() + temp.obliczPremie()) < avg ? 1 : 0;
        }
        return ans;
    }

    public void read(String filename) throws FileNotFoundException{
        init();
        File file = new File(filename);
        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            String[] elems = line.split(";");

            //System.out.println(elems.length);
            add(new Pracownik(elems[0], elems[1], elems[2], elems[3], Long.valueOf(elems[4]), Long.valueOf(elems[5]), Long.valueOf(elems[6])));
        }
        scanner.close();
        return;
    }

    public void write(String filename) throws FileNotFoundException, IOException{
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);

        Iterator<Pracownik> iter = arr.iterator();
        while(iter.hasNext()) bw.write(iter.next().toStringFile());

        bw.close();
        return;
    }
    public void display(){
        Iterator<Pracownik> iter = arr.iterator();

        System.out.println(
        "----------------------------------------------------------------------------------------------------------\n"+
        String.format("%-15s%-15s%-20s%-20s%15s%8s%8s%-15s\n", "imie", "nazwisko", "data_urodzenia", "stanowisko", "PESEL", "staz", "pensja", "premia")+
        "----------------------------------------------------------------------------------------------------------");

        while(iter.hasNext()) System.out.println(iter.next());
        System.out.println("----------------------------------------------------------------------------------------------------------");
    }
    public int size(){return arr.size();}
}
