.data
    instr:      .space 6
    arg1:       .space 4  
    arg2:       .space 4 
    arg3:       .space 4
    label:      .space 50

    newline:            .asciiz "\n"
    separator:          .asciiz "----------\n" 
    operator_prompt:    .asciiz "podaj operacje: "

    i1:         .asciiz "ADD"
    i2:         .asciiz "ADDI"
    i3:         .asciiz "J"
    i4:         .asciiz "NOOP"
    i5:         .asciiz "MULT"
    i6:         .asciiz "JR"
    i7:         .asciiz "JAL"

number_prompt:  .asciiz "podaj liczbe instrukcji do analizy: "

.text
main:
#pierwszy prompt
    li $v0, 4
    la $a0, number_prompt
    syscall

#wczytanie liczby instrukcji
    li $v0, 5
    syscall

#pod s0 mamy liczbe instrukcji, pod s1 licznik
    move $s0, $v0
    li $s1, 0
    li $t6, 1
    li $t7, '$'
    li $s3, 48
    li $s4, 57

    jal read_instructions

read_instructions:
    #li $v0, 1
    #move $a0, $s1
    #syscall 
#warunki petli
    beq $s1, $s0, kill_read_instructions
    li $t5, 0

#prompt operacji
    li $v0, 4
    la $a0, operator_prompt
    syscall

#wczytanie instrukcji
    li $v0, 8
    la $a0, instr
    li $a1, 6
    syscall

#sprawdzenie czy wprowadzona instrukcja jest obslugiwana
    la $t4, instr

    la $t3, i2
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_ADDI

    la $t4, instr
    la $t3, i1
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_ADD

    la $t4, instr
    la $t3, i7
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_J

    la $t4, instr
    la $t3, i4
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_NOOP

    la $t4, instr
    la $t3, i5
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_MULT

    la $t4, instr
    la $t3, i6
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_JR

    la $t4, instr
    la $t3, i3
    jal strcmp
    add $t5, $t5, $v0
    beq $v0, $t6, OP_J

    beq $t5, $zero, read_instructions
    addi $s1, $s1, 1

    j read_instructions

strcmp:
    lb $t0, 0($t3)  # Załaduj bieżący znak z pierwszego stringa do $t0
    lb $t1, 0($t4)  # Załaduj bieżący znak z drugiego stringa do $t1

    beq $t0, $zero, strcmp_end  # Jeśli dotarliśmy do końca pierwszego stringa, zakończ
    bne $t0, $t1, strings_not_equal  # Jeśli znaki są różne, stringi nie są równe

    # Przejdź do następnego znaku
    addi $t3, $t3, 1
    addi $t4, $t4, 1
    j strcmp  # Kontynuuj pętlę

strcmp_end:
    # Sprawdź, czy oba stringi zakończyły się na '\0'
    addi $t4, $t4, 1
    lb $t1, 0($t4)

    beq $t1, $zero, strings_equal  # Jeśli tak, stringi są równe
    bne $t1, $zero, strings_not_equal

strings_not_equal:
    li $v0, 0  # Stringi nie są równe
    jr $ra

strings_equal:

    li $v0, 1  # Stringi są równe
    jr $ra

kill_read_instructions:
    #jr $ra
    move $t3, $sp
    jal print_loop
    j exit

strlen_loop:
    lb $t2, 0($t0)
    beq $t2, $zero, strlen_done
    addi $t0, $t0, 1
    addi $t1, $t1, 1
    j strlen_loop
strlen_done:
    addi $t1, $t1, 1   # Długość stringa + null-terminator

    # Rezerwuj miejsce na stosie
    subu $sp, $sp, $t1

    #la $t0, instr

    # Skopiuj string na stos
    move $t3, $sp      # $t3 będzie wskaźnikiem stosu do zapisu stringa
    jr $ra

copy_loop:
    lb $t2, 0($t0)

    beq $t2, $zero, copy_done  # Kopiowanie kończy się na null-terminatorze
    sb $t2, 0($t3)
    addi $t0, $t0, 1
    addi $t3, $t3, 1
    j copy_loop

copy_done:
    li $t2, ' '
    sb $t2, 0($t3)

    move $t3, $sp
    jr $ra

print_loop:
    lb $a0, 0($t3)
    li $a1, ' '
    beq $a0, $zero, print_done  # Koniec stringa
    beq $a0, $a1, print_skip  # skipowanie spacji
    li $v0, 11        # Kod system call dla print_char
    syscall
    addi $t3, $t3, 1
    j print_loop
print_done:
    # Przywróć wskaźnik stosu
    #move $sp, $s0
    jr $ra

print_skip:
    add $t3, $t3, 1
    j print_loop

OP_ADDI:
#wczytywanie argumentow
    li $v0, 8
    la $a0, arg1
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    la $a0, arg1
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions

    lb $a1, 1($a0)

    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions


#wczytanie 2 argumentu
    li $v0, 8
    la $a0, arg2
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    la $a0, arg2
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions

    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#wczytywanie 3 argumentu
    li $v0, 8
    la $a0, arg3
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    lb $a1, 0($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#dodanie operacji na stos

    la $t0, arg3 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg3 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

OP_ADD:
#wczytywanie argumentow
    li $v0, 8
    la $a0, arg1
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow (czy jest $)
    la $a0, arg1
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions


#wczytanie 2 argumentu
    li $v0, 8
    la $a0, arg2
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    la $a0, arg2
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#wczytywanie 3 argumentu
    li $v0, 8
    la $a0, arg3
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    la $a0, arg3
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#dodanie operacji na stos
    la $t0, arg3 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg3 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

OP_J:
#wczytywanie argumentow
    li $v0, 8
    la $a0, label
    li $a1, 50
    syscall

#sprawdzenie poprawnosci argumentow (czy jest $)
    la $a0, label
    lb $a1, 0($a0)
    beq $a1, $t7, read_instructions #jezeli jest $ na poczatku to anulujemy

#dodanie operacji na stos
    la $t0, label #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, label #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

OP_MULT:
#wczytywanie argumentow
    li $v0, 8
    la $a0, arg1
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow (czy jest $)
    la $a0, arg1
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions


#wczytanie 2 argumentu
    li $v0, 8
    la $a0, arg2
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow
    la $a0, arg2
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#dodanie operacji na stos
    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg2 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

OP_JR:
#wczytywanie argumentow
    li $v0, 8
    la $a0, arg1
    li $a1, 4
    syscall

#sprawdzenie poprawnosci argumentow (czy jest $)
    la $a0, arg1
    lb $a1, 0($a0)
    bne $a1, $t7, read_instructions
    lb $a1, 1($a0)
    blt $a1, $s3, read_instructions
    bgt $a1, $s4, read_instructions

#dodanie operacji na stos
    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, arg1 #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

OP_NOOP:
#dodanie operacji na stos
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, instr #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    li $t1, 0 #t1 to nasz licznik
    jal strlen_loop
    la $t0, separator #do t0 dajemy wprowadzona instrukcje
    jal copy_loop

#powrot do petli
    addi $s1, $s1, 1
    j read_instructions

exit:
    li $v0, 10
    syscall
