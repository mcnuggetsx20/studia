.data
    str:                .space 50
    key:                .space 8
    inverted_key:       .space 16
    key_prompt:         .asciiz "podaj klucz: "
    str_normalized:     .space 50
    str_encrypted:      .space 50
    which_op:           .space 3
    op_prompt:          .asciiz "podaj typ operacji [S/D]: "
    str_prompt:         .asciiz "podaj kryptogram/tekst jawny: "
    newline:            .asciiz "\n"

.text
main:

#wypisanie prompta dla typu operacji
    li $v0, 4
    la $a0, op_prompt
    syscall

#wczytanie typu operacji
    li $v0, 8
    la $a0, which_op
    li $a1, 3
    syscall

#wypisanie prompta dla klucza
    li $v0, 4
    la $a0, key_prompt
    syscall

#wczytanie klucza
    li $v0, 8
    la $a0, key
    li $a1, 8
    syscall

#zebranie dlugosci klucza do t0
    li $t0, 0 #pod t0 bedziemy mieli dlugosc klucza
    la $t1, key
    jal get_length
    subi $t0, $t0, 2 #odejmujemy od policzonej dlugosci 2 bo za duzo sie liczy w petli

#zapamietujemy dlugosc klucza w s7 - przyda sie na pozniej
    move $s7, $t0

#sprawdzenie czy dlugosc klucza jest poprawna; jesli nie to program idzie od nowa
    li $t1, 3
    blt $s7, $t1, main
    li $t1, 8
    bgt $s7, $t1, main

#prompt dla stringa
    li $v0, 4
    la $a0, str_prompt
    syscall

#wczytanie stringa
    li $v0, 8
    la $a0, str
    li $a1, 50
    syscall

#zebranie dlugosci stringa do t0
    li $t0, 0 #pod t0 bedziemy mieli dlugosc stringa
    la $t1, str
    jal get_length
    subi $t0, $t0, 2 #odejmujemy od policzonej dlugosci 2 bo za duzo sie liczy w petli

#sprawdzenie czy dlugosc stringa jest poprawna; jesli nie to program idzie od nowa
    li $t1, 50
    bgt $t0, $t1, main

#zapamietujemy dlugosc stringa na pozniej
    move $s6, $t0

#normalizacja stringa do zaszyfrowania/odszyfrowania
    la $s0, str
    la $s1, str_normalized
    jal normalize_loop

#odwrocenie klucza
    la $t0, inverted_key
    la $t1, key
    li $t3, 1

    jal invert_key

#szyfrowanie 
    la $t0, str_normalized
    la $s0, str_normalized
    la $t1, key

    la $t2, which_op
    lb $t2, 0($t2)
    beq $t2, 'D', load_inverted

    j main_continue
    
    j exit

main_continue:
#kontynuacja szyfrowania
    la $t2, str_encrypted
    li $t3, 0 #licznik

    jal good_encrypt

    j exit
    
normalize_loop:

    lb $t0, 0($s0)

    li $t4, '?'
    beq $t0, $t4, skip

    li $t4, '!'
    beq $t0, $t4, skip

    li $t4, ','
    beq $t0, $t4, skip

    li $t4, '.'
    beq $t0, $t4, skip

    li $t4, '\n'
    beq $t0, $t4, skip

    li $t4, ' '
    beq $t0, $t4, skip

    sb $t0, 0($s1)

    addi $s1, $s1, 1
    addi $s0, $s0, 1

    bnez $t0, normalize_loop

    jr $ra

skip:
    addi $s0, $s0, 1
    j normalize_loop

get_length:
    addi $t0, $t0, 1
    lb $t2, 0($t1)
    addi $t1, $t1, 1
    bnez $t2, get_length
    jr $ra

good_encrypt:
    lb $t4, 0($s0) #kolejny element tekstu jawnego
    div $t3, $s7 #s7 to dlugosc klucza, w HI zapisana jest reszta z dzielenia
    mfhi $t5

    move $s5, $t5 #zapamietujemy reszte z dzielenia licznika przez dlugosc klucza

    add $t5, $t5, $t1 #dodajemy reszte z dzielenia do poczatkowego adresu klucza, czyli zbieramy indeks
    lb $t6, 0($t5) #dobry element klucza
    sub $t6, $t6, '0' #konwersja na int
    sub $t6, $t6, 1 #odjecie jeden bo to indeks ktorego uzyjemy do zebrania elementu z str_normalized

    mflo $t5
    mul $t5, $t5, $s7 #w ktorym interwale jestesmy

    add $t5, $t0, $t5 #dodajemy t5 czyli offset w ktorym interwale jestesmy

    add $t5, $t5, $t6 #dobry indeks w tekscie jawnym

    lb $t6, 0($t5) #dobry element z tekstu jawnego

    sb $t6, 0($t2) #zapisujemy t6 na nastepnym miejscu w str_encrypted

    li $v0, 11
    move $a0, $t6
    syscall

    addi $s0, $s0, 1
    addi $t2, $t2, 1
    addi $t3, $t3, 1

    add $s5, $s5, $t4 #reszta z dzielenia + aktualny znak

    bnez $s5, good_encrypt

    jr $ra

invert_key:
    lb $t4, 0($t1)

    sub $t4, $t4, '0' #string -> int
    sub $t4, $t4, 1

    add $t5, $t0, $t4 #index t4 w inverted key
    add $t6, $t3, '0'
    sb $t6, 0($t5) #zapisujemy licznik w inverted key

    addi $t3, $t3, 1
    addi $t1, $t1, 1

    ble $t3, $s7, invert_key

    jr $ra
    
load_inverted:
    la $t1, inverted_key
    j main_continue

exit:
    li $v0, 10
    syscall

