.data
    main_board:                 
        .asciiz " " 
        .asciiz " "
        .asciiz " " 
        .asciiz " "
        .asciiz " " 
        .asciiz " "
        .asciiz " " 
        .asciiz " "
        .asciiz " "

    score_player:               .word 0
    score_machine:              .word 0
    player_symbol:              .space 1
    machine_symbol:             .space 1
    main_prompt:                .asciiz "KOLKO I KRZYZYK\n"
    symbol_choice_prompt:       .asciiz "Wybierz swoj symbol\n1. O\n2. X\n-> "
    ingame_prompt:              .asciiz "-> "
    number_of_rounds_prompt:    .asciiz "Podaj liczbe rund: "    
    clear_screen_code:          .byte 0x1B,0x5B,0x33,0x3B,0x4A,0x1B,0x5B,0x48,0x1B,0x5B,0x32,0x4A
    newline:                    .asciiz "\n"
    round_lost:                 .asciiz "Przegrales runde!\n"
    round_won:                  .asciiz "Wygrales runde!\n"
    round_draw:                 .asciiz "Remis w tej rundzie\n"
    game_lost:                  .asciiz "Przegrales gre!\n"
    game_won:                   .asciiz "Wygrales gre!\n"
    game_draw:                  .asciiz "Remis\n"
    score_title:                .asciiz "Wynik: "
    final_score_title:          .asciiz "Ostateczny wynik: "

    board_horizontal_line:      .asciiz " - - -\n"
    board_vertical_separator:   .asciiz "|"

.text
main:
    li $v0, 4
    la $a0, main_prompt
    syscall

    la $a0, symbol_choice_prompt
    syscall

    #przyjecie wyboru uzytkownika
    li $v0, 5
    syscall
    move $s0, $v0

    #zabezpieczenie
    li $t0, 2
    bgt $s0, $t0, main
    li $t0, 1
    blt $s0, $t0, main


    li $v0, 4 
    la $a0, number_of_rounds_prompt
    syscall

    li $v0, 5
    syscall
    move $s4, $v0 #liczba rund
    blt $s4, $zero, main

    jal screen_clear

    li $s6, -1 #licznik rund
    #przypisanie znaku
    li $t0, 1
    beq $s0, $t0, assignOX
    li $t0, 2
    beq $s0, $t0, assignXO

main_continue:
    addi $s6, $s6, 1
    beq $s6, $s4, exit

    jal board_clear
    jal print_board

    la $s0, player_symbol
    la $s1, machine_symbol
    lb $s0, 0($s0)
    lb $s1, 0($s1)

    li $s3, 0 #main counter
    li $s5, 9
    jal game_loop

    j exit

game_loop:
    jal print_player_symbol

    #wypisanie prompta wyboru pola
    li $v0, 4
    la $a0, ingame_prompt
    syscall

    #przyjecie pola od gracza
    li $v0, 5
    syscall
    move $t3, $v0

    #zapisanie znaku gracza na odpowiednim polu
    addi $t3, $t3, -1
    sll $t3, $t3, 1 #mnozy5y razy 2 bo kazdy element planszy ma 2 bajty
    la $s2, main_board
    add $s2, $s2, $t3

    lb $t0, 0($s2)
    li $t1, ' '
    bne $t0, $t1, game_loop
    
    jal screen_clear

    sb $s0, 0($s2)
    addi $s3, $s3, 1

    jal machine_strategy

    jal print_board
    jal winner_check

    blt $s3, $s5, game_loop

    li $v0, 4
    la $a0, round_draw
    syscall
    j print_score

print_board:
    la $t2, main_board
    li $v0, 4

    la $a0, board_horizontal_line
    syscall

    li $t1, 3
    li $t0, 0 #counter

    addi $sp, $sp, -4
    sw $ra, 0($sp)

    jal print_board_content

    la $a0, board_horizontal_line
    syscall

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

#t0, t1, t2
print_board_content:
    la $a0, board_vertical_separator
    syscall
    move $a0, $t2
    syscall
    la $a0, board_vertical_separator
    syscall
    addi $t2, $t2, 2
    move $a0, $t2
    syscall
    la $a0, board_vertical_separator
    syscall
    addi $t2, $t2, 2
    move $a0, $t2
    syscall
    la $a0, board_vertical_separator
    syscall
    la $a0, newline
    syscall
    addi $t2, $t2, 2
    
    addi $t0, $t0, 1
    blt $t0, $t1, print_board_content
    jr $ra

winner_check:
    la $s2, main_board 

    addi $sp, $sp, -4
    sw $ra, 0($sp)

    #poziom
    lb $t0, 0($s2)
    lb $t1, 2($s2)
    lb $t2, 4($s2)
    jal winner_check_sup

    lb $t0, 6($s2)
    lb $t1, 8($s2)
    lb $t2, 10($s2)
    jal winner_check_sup

    lb $t0, 12($s2)
    lb $t1, 14($s2)
    lb $t2, 16($s2)
    jal winner_check_sup

    #pion
    lb $t0, 0($s2)
    lb $t1, 6($s2)
    lb $t2, 12($s2)
    jal winner_check_sup

    lb $t0, 2($s2)
    lb $t1, 8($s2)
    lb $t2, 14($s2)
    jal winner_check_sup

    lb $t0, 4($s2)
    lb $t1, 10($s2)
    lb $t2, 16($s2)
    jal winner_check_sup

    #skosy
    lb $t0, 0($s2)
    lb $t1, 8($s2)
    lb $t2, 16($s2)
    jal winner_check_sup

    lb $t0, 4($s2)
    lb $t1, 8($s2)
    lb $t2, 12($s2)
    jal winner_check_sup

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

winner_check_sup:
    li $t5, ' '
    bne $t0, $t5, winner_check_sup1
    jr $ra

winner_check_sup1:
    beq $t0, $t1, winner_check_sup2
    jr $ra

winner_check_sup2:
    beq $t1, $t2, winner_found
    jr $ra

winner_found:
    la $t1, player_symbol
    lb $t1, 0($t1)
    beq $t0, $t1, winner_found_player
    j winner_found_machine

winner_found_player:
    lw $t0, score_player
    addi $t0, $t0, 1
    sw $t0, score_player

    li $v0, 4
    la $a0, round_won
    syscall

    j print_score

winner_found_machine:
    lw $t0, score_machine
    addi $t0, $t0, 1
    sw $t0, score_machine

    li $v0, 4
    la $a0, round_lost
    syscall

    j print_score

print_score:
    #wypisanie wyniku

    li $v0, 4
    la $a0, score_title
    syscall

    li $v0, 1
    lw $a0, score_player
    syscall

    li $v0, 11
    li $a0, ':'
    syscall

    li $v0, 1
    lw $a0, score_machine
    syscall

    li $v0, 4
    la $a0, newline
    syscall

    j main_continue

print_final_score:

    j exit

end_winner_check:
    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

assignXO:
    la $t0, player_symbol
    li $t1, 'X'
    sb $t1, 0($t0)

    la $t0, machine_symbol
    li $t1, 'O'
    sb $t1, 0($t0)

    j main_continue

assignOX:
    la $t0, player_symbol
    li $t1, 'O'
    sb $t1, 0($t0)

    la $t0, machine_symbol
    li $t1, 'X'
    sb $t1, 0($t0)

    j main_continue

board_clear:
    la $s2, main_board
    li $t0, ' '
    sb $t0, 0($s2)
    sb $t0, 2($s2)
    sb $t0, 4($s2)
    sb $t0, 6($s2)
    sb $t0, 8($s2)
    sb $t0, 10($s2)
    sb $t0, 12($s2)
    sb $t0, 14($s2)
    sb $t0, 16($s2)
    jr $ra

machine_strategy:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $s2, main_board
    addi $s2, $s2, 8
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    li $t0, ' '
    addi $s2, $s2, 16
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 0
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 4
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 12
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 2
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 14
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 6
    lb $t1, 0($s2)
    jal machine_strategy_check

    la $s2, main_board
    addi $s2, $s2, 10
    lb $t1, 0($s2)
    jal machine_strategy_check

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

machine_strategy_check:
    beq $t1, $t0, machine_fill
    jr $ra

machine_fill:
    lb $t1, machine_symbol
    sb $t1, 0($s2)
    addi $s3, $s3, 1 #dodajemy do licznika z game_loop, zeby wiadomo bylo ile ruchow zostalo wykonanych
    j machine_strategy_end

machine_strategy_end:
    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

screen_clear:
    li $v0, 4
    la $a0, clear_screen_code
    syscall

    jr $ra

print_player_symbol:
    li $v0, 11
    lb $a0, player_symbol
    syscall
    jr $ra

print_game_player_won:
    li $v0, 4
    la $a0, game_won
    syscall
    li $v0, 10
    syscall

print_game_machine_won:
    li $v0, 4
    la $a0, game_lost
    syscall
    li $v0, 10
    syscall

print_game_draw:
    li $v0, 4
    la $a0, game_draw
    syscall
    li $v0, 10
    syscall

exit:
    #wypisanie wyniku

    li $v0, 4
    la $a0, final_score_title
    syscall

    li $v0, 1
    lw $a0, score_player
    syscall

    li $v0, 11
    li $a0, ':'
    syscall

    li $v0, 1
    lw $a0, score_machine
    syscall

    li $v0, 4
    la $a0, newline
    syscall

    lw $t0, score_player
    lw $t1, score_machine
    blt $t0, $t1, print_game_machine_won
    bgt $t0, $t1, print_game_player_won
    beq $t0, $t1, print_game_draw

    li $v0, 10
    syscall
