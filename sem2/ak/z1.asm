.data
    main_title: .asciiz "------ZADANIE-------\n"
    option_prompt: .asciiz "Podaj wyrazenie: "
    continue_prompt: .asciiz "Czy chcesz kontynuowac? [0/1]: "
    exit_prompt: .asciiz "Opuszczanie programu\n"

    prompt_a: .asciiz "a="
   	prompt_b: .asciiz "b="
   	prompt_c: .asciiz "c="
    equal_sign: .asciiz "="
    dot_sign: .asciiz ". "
    new_line: .asciiz "\n"

    #wyrownujemy kazdego stringa do dlugosci 16 bajtow czyli wyrazenia max 16 znakow
    finals:
        .align 4
        .asciiz "(a+c)*(a-b)/c"
        .align 4
        .asciiz "(a+b)/(b-c*a)"
        .align 4
        .asciiz "a*b/(a+b+c)"
        .align 4
        .asciiz "\0"
        

    number_of_expressions: .word 3
    expressions:
        .word proc1
        .word proc2
        .word proc3

.text

main:
    #wypisanie tytulu zadania
    li $v0, 4
    la $a0, main_title
    syscall

    #ustawienia pod print_options
    li $t5, 1 #licznik w t5
    la $t0, finals #zapisujemy nasza tablice w $t0
    jal print_options #wypisanie mozliwych opcji

    #wypisanie option prompt
    li $v0, 4
    la $a0, option_prompt
    syscall

    #wczytanie wyboru uzytkownika i przechowanie go w s4
    li $v0, 5
    syscall
    move $s4, $v0

    lw $t0, number_of_expressions #tymczasowo przechowujemy liczbe wyrazen w t0

    #rozpatrzenie opcji
    beq $s4, $zero, exit
    bgt $s4, $t0, main #jesli wybrano zla opcje to uruchamiamy main jeszcze raz

    #s0,s1,s2 --> a,b,c
    jal read_variables #wczytanie zmiennych
    jal etre

    #zaptytanie o kontynuacje programu
    li $v0, 4
    la $a0, continue_prompt
    syscall

    #czy chcemy kontynuowac
    li $v0, 5
    syscall
    beq $v0, $zero, exit
    
    j main

read_variables:
    #s0,s1,s2 --> a,b,c
    
    #tytul i wczytanie a
    li $v0, 4
    la $a0, prompt_a
    syscall

    li $v0, 5
    syscall
    move $s0, $v0

    #tytul i wczytanie b
    li $v0, 4
    la $a0, prompt_b
    syscall

    li $v0, 5
    syscall
    move $s1, $v0

    #tytul i wczytanie c
    li $v0, 4
    la $a0, prompt_c
    syscall

    li $v0, 5
    syscall
    move $s2, $v0

    jr $ra

print_options:
    #wypisywanie numeracji opcji
    li $v0, 1
    move $a0, $t5
    syscall
    li $v0, 4
    la $a0, dot_sign
    syscall

    #wypisanie aktualnego stringa z tablicy finals
    li $v0, 4
    move $a0, $t0
    syscall 

    #znak nowej linii
    la $a0, new_line
    syscall

    addi $t5, $t5, 1 #dodanie 1 do licznika
    addi $t0, $t0, 16 #nastepny string

    lb $t2, ($t0) #wczytujemy pierwszy bajt nastepnego stringa
    bne $t2, $zero, print_options #jesli nastepny string nie jest 0(null) to kontynuujemy petle

    jr $ra #return

etre: #execute the right expreesion - wywolanie wyrazenia o numerze $s4
    la $t0, expressions
    addi $s4, $s4, -1 #odjecie 1 od indeksu
    sll $t1, $s4, 2 #obliczenie indeksu wyrazenia
    add $t0, $t0, $t1 #t0 - odpowiedni element tablicy expressions
    lw $t0, 0($t0) #t0 - odpowiednie wyrazenie
    jr $t0

proc1: #(a+c)*(a-b)/c
    #konwersja liczb na float, zeby wynik dzielenia byl poprawny
    mtc1 $s0, $f0
    mtc1 $s1, $f1
    mtc1 $s2, $f2

    cvt.s.w $f0, $f0
    cvt.s.w $f1, $f1
    cvt.s.w $f2, $f2

    #obliczenie wyrazenia
    add.s $f3, $f0, $f2 #a+c
    sub.s $f4, $f0, $f1 #a-b
    mul.s $f12, $f3, $f4 #(a+c)*(a-b)
    div.s $f12, $f12, $f2 #(a+c)*(a-b)/c

    #wypisanie wyniku i prompta
    la $t2, finals
    li $v0, 4
    move $a0, $t2
    syscall
    la $a0, equal_sign
    syscall

    li $v0, 2
    syscall

    #znak nowej linii
    li $v0, 4
    la $a0, new_line
    syscall

    jr $ra #return

proc2: #(a+b)/(b-c*a)
    #konwersja liczb na float, zeby wynik dzielenia byl poprawny
    mtc1 $s0, $f0
    mtc1 $s1, $f1
    mtc1 $s2, $f2

    cvt.s.w $f0, $f0
    cvt.s.w $f1, $f1
    cvt.s.w $f2, $f2

    #obliczenie wyrazenia
    add.s $f3, $f0, $f1 #(a+b)
    mul.s $f4, $f2, $f0 #c*a
    sub.s $f4, $f1, $f4 #b-c*a
    div.s $f12, $f3, $f4 #(a+b) / (b-c*a)

    #wypisanie wyniku i prompta
    la $t2, finals
    addi $t2, $t2, 16 #odpowiedni prompt
    li $v0, 4
    move $a0, $t2
    syscall
    la $a0, equal_sign
    syscall

    li $v0, 2
    syscall

    #znak nowej linii
    li $v0, 4
    la $a0, new_line
    syscall

    jr $ra #return

proc3: #a*b/(a+b+c)
    #konwersja liczb na float, zeby wynik dzielenia byl poprawny
    mtc1 $s0, $f0
    mtc1 $s1, $f1
    mtc1 $s2, $f2

    cvt.s.w $f0, $f0
    cvt.s.w $f1, $f1
    cvt.s.w $f2, $f2

    #obliczanie wyrazenia
    add.s $f4, $f0, $f1
    add.s $f4, $f4, $f2

    div.s $f4, $f1, $f4
    mul.s $f12, $f4, $f0 #wynik zmiennoprzecinkowy trafia do f12 czyli do rejestru odpowiedzialnego za wypisywanie floatow

    #wypisanie wyniku i prompta
    la $t2, finals
    addi $t2, $t2, 32 #odpowiedni prompt
    li $v0, 4
    move $a0, $t2
    syscall
    la $a0, equal_sign
    syscall

    li $v0, 2
    syscall

    #znak nowej linii
    li $v0, 4
    la $a0, new_line
    syscall

    jr $ra

exit:
    li $v0, 4
    la $a0, exit_prompt
    syscall

    li $v0, 10
    syscall


