.data
main_prompt:    .asciiz "Podaj numer funkcji\n"
x_prompt:       .asciiz "x="
y_prompt:       .asciiz "y="
def_op1:        .asciiz "1. f(x, y)= {1 dla x=y lub y=0 i [f(x-1, y)+f(x-1, y-1)] dla x>y}\n"
def_op2:        .asciiz "2. g(x, y)= {1 dla x=y lub y=0 i [2*g(x-1, y)+2*g(x-1, y-1)] dla x>y}\n"
def_op3:        .asciiz "3. h(x, y)= {2 dla x=y lub y=0 i [2*h(x-1, y)+h(x-1, y-1)] dla x>y}\n"
newline:        .asciiz "\n"

operations:
    .word operation1
    .word operation2
    .word operation3

floats:      .float 1.0, 0.0, 2.0

.text
main:
    #przygotowanie zmiennych float 0 oraz 1
    lwc1 $f5, floats
    lwc1 $f6, floats + 4

    #wypisanie wszystkich promptow
    li $v0, 4
    la $a0, def_op1
    syscall
    la $a0, def_op2
    syscall
    la $a0, def_op3
    syscall

    #wybor funkcji
    li $v0, 5
    syscall
    move $t0, $v0 #do t0 dajemy numer funkcji

    li $t5, 3
    bgt $t0, $t5, exit
    li $t5, 1
    blt $t0, $t5, exit

    #przyjecie zmiennych typu float
    li $v0, 4
    la $a0, x_prompt
    syscall
    li $v0, 6
    syscall
    mov.s $f1, $f0

    li $v0, 4
    la $a0, y_prompt
    syscall
    li $v0, 6
    syscall
    mov.s $f2, $f0

    c.lt.s $f1, $f2
    bc1t exit

    #wywolanie funkcji rekurencyjnej
    jal etre

    #wypisanie wyniku
    li $v0, 2
    mov.s $f12, $f11
    syscall

    j exit

etre:
    subi $t0, $t0, 1
    sll $t0, $t0, 2 #mnozymy indeks przez 4 (bo word ma 4 bajty)
    la $t1, operations
    add $t1, $t1, $t0
    lw $t1, 0($t1)
    jr $t1

    
operation1:
    addi $sp, $sp, -16 #rozszerzamy stack o 8 bajtow: $ra, x, y, temp
    sw $ra, 0($sp)
    swc1 $f1, 4($sp)
    swc1 $f2, 8($sp)

    c.eq.s $f1, $f2 
    bc1t operation1_return1 #if x==y: return 1

    c.eq.s $f2, $f6 
    bc1t operation1_return1 #if y==0: return 1

    sub.s $f1, $f1, $f5 #odejmujemy 1
    jal operation1 #f(x-1, y)
    #mov.s $f3, $f11 #zapamietujemy pierwszy skladnik pod t3
    swc1 $f11, 12($sp) #zapamietujemy pierwszy skladnik na stosie

    lwc1 $f1, 4($sp)
    lwc1 $f2, 8($sp)

    sub.s $f1, $f1, $f5
    sub.s $f2, $f2, $f5

    jal operation1 #f(x-1, y-1)
    lwc1 $f3, 12($sp)
    add.s $f11, $f11, $f3 #dodajemy skladniki

    j operation_end

operation1_return1:
    lwc1 $f11, floats
    j operation_end

operation_end:
    lw $ra, 0($sp)
    addi $sp, $sp, 16
    jr $ra

operation2:
    addi $sp, $sp, -16 #rozszerzamy stack o 8 bajtow: $ra, x, y
    sw $ra, 0($sp)
    swc1 $f1, 4($sp)
    swc1 $f2, 8($sp)

    c.eq.s $f1, $f2 
    bc1t operation2_return1 #if x==y: return 1

    c.eq.s $f2, $f6 
    bc1t operation2_return1 #if y==0: return 1

    sub.s $f1, $f1, $f5 #odejmujemy 1
    jal operation2 #f(x-1, y)
    add.s $f11, $f11, $f11 #f11 *= 2
    swc1 $f11, 12($sp) #zapamietujemy pierwszy skladnik na stosie

    lwc1 $f1, 4($sp)
    lwc1 $f2, 8($sp)

    sub.s $f1, $f1, $f5
    sub.s $f2, $f2, $f5

    jal operation2 #f(x-1, y-1)
    lwc1 $f3, 12($sp)
    add.s $f11, $f11, $f11 #f11 *= 2
    add.s $f11, $f11, $f3 #dodajemy skladniki

    j operation_end

operation2_return1:
    lwc1 $f11, floats
    j operation_end

operation3:
    addi $sp, $sp, -16 #rozszerzamy stack o 8 bajtow: $ra, x, y
    sw $ra, 0($sp)
    swc1 $f1, 4($sp)
    swc1 $f2, 8($sp)

    c.eq.s $f1, $f2 
    bc1t operation3_return1 #if x==y: return 1

    c.eq.s $f2, $f6 
    bc1t operation3_return1 #if y==0: return 1

    sub.s $f1, $f1, $f5 #odejmujemy 1
    jal operation3 #f(x-1, y)
    add.s $f11, $f11, $f11 #f11 *= 2
    swc1 $f11, 12($sp) #zapamietujemy pierwszy skladnik na stosie

    lwc1 $f1, 4($sp)
    lwc1 $f2, 8($sp)

    sub.s $f1, $f1, $f5
    sub.s $f2, $f2, $f5

    jal operation3 #f(x-1, y-1)
    lwc1 $f3, 12($sp)
    add.s $f11, $f11, $f3 #dodajemy skladniki

    j operation_end

operation3_return1:
    lwc1 $f11, floats + 8
    j operation_end

exit:
    li $v0, 10
    syscall
