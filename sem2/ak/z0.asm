.data
    x: .asciiz "x="
   	y: .asciiz "y="
  	final: .asciiz "x+y="

.text
main:
    #tyutl dla x
    li $v0, 4
    la $a0, x
    syscall
    
    #wczytanie zmiennej x
    li $v0, 5
    syscall
    #skopiowanie wejscia do rejestru t0
    move $t0, $v0
    
    #tyutl dla y
    li $v0, 4
    la $a0, y
    syscall

    #to samo tutaj
    li $v0, 5
    syscall
    move $t1, $v0
    
    #dodanie dwoch liczb
    add $t2, $t0, $t1
    
    #wypisanie ostatecznego prompta
    li $v0, 4
    la $a0, final
 	syscall
 
 	#wypisanie sumy
    li $v0, 1
    move $a0, $t2
    syscall
    
    jal exit

exit:
    li $v0, 10
    syscall
 
    