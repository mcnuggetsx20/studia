.data
    plaintext:       .asciiz "abcd"       # Tekst jawny
    key:            .asciiz "2431"        # Klucz permutacyjny
    ciphertext:     .space 5              # Miejsce na zaszyfrowany tekst (4 znaki + '\0')
    newline:        .asciiz "\n"

.text
main:
    # Inicjalizacja wskaźników
    la $t0, plaintext      # $t0 = adres tekstu jawnego
    la $t1, key            # $t1 = adres klucza
    la $t2, ciphertext     # $t2 = adres zaszyfrowanego tekstu

encrypt_loop:
    lb $t4, 0($t1)         # Załaduj bieżący bajt z klucza
    beqz $t4, end_encrypt  # Jeśli koniec klucza, zakończ pętlę

    sub $t4, $t4, '0'      # Przekształć znak klucza na indeks (np. '2' -> 2)
    sub $t4, $t4, 1        # Przekształć indeks (np. 2 -> 1)
    #sll $t4, $t4, 0        # Przemnóż przez 1 (rozmiar elementu 8-bitowego, nie jest potrzebne)
    add $t5, $t0, $t4      # Oblicz adres `plaintext[v-1]`
    lb $t6, 0($t5)         # Załaduj bajt z tekstu jawnego

    sb $t6, 0($t2)         # Zapisz znak do zaszyfrowanego tekstu

    addi $t1, $t1, 1       # Przesuń wskaźnik klucza
    addi $t2, $t2, 1       # Przesuń wskaźnik zaszyfrowanego tekstu

    j encrypt_loop

end_encrypt:
    sb $zero, 0($t2)       # Dodaj znak końca łańcucha do zaszyfrowanego tekstu

    # Wypisz zaszyfrowany tekst
    li $v0, 4
    la $a0, ciphertext
    syscall

    # Wypisz nową linię
    li $v0, 4
    la $a0, newline
    syscall

    # Zakończenie programu
    li $v0, 10
    syscall
