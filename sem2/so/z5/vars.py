CPU_COUNT = 5
MAX_SINGLE_LOAD = 20 #ile moze maksymalnie zajmowac wygenerowany proces
MAX_PROC_GEN = 30 #ile procesow generuje sie w kazdym interwale
STEPS_COUNT = 8

STRAT1_SEND_THRESHOLD = 20 #dla strategii 1 prog wyslania procesu na losowy procesor
STRAT1_MAX_FAILURES = 3 #ile razy bedzie procesor x pytal o obciazenie losowego procesora y

STRAT2_SEND_THRESHOLD = 20 #dla strategii 2 prog wyslania procesu na losowy procesor

STRAT3_MIN_THRESHOLD = 20
STRAT3_SEND_THRESHOLD = 30 #dla strategii 3 prog wyslania procesu na losowy procesor
STRAT3_TRANSFER_PORTION = 0.1 #jaka porcja bedzie przejeta np 0.2 lub 0.5

