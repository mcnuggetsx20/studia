import strats

funcs = [strats.strat1, strats.strat2, strats.strat3]
for i in funcs: 
    temp = strats.main_loop(i)
    print(f'{i.__name__}:')
    print(f'avg_load: {temp[0]}')
    print(f'avg_deviation: {temp[1]}', end='\n\n')



