from random import randint, choice
from time import sleep
from statistics import fmean
from itertools import chain
import vars

def mean_absolute_deviation(data, avg_load):
    mean_value = fmean(data)
    return fmean([abs(avg_load - mean_value) for x in data])

migrations = [0,0,0]
requests = [0,0,0]

def main_loop():
    main_counter = 0
    CPUS1=   [list() for i in range(vars.CPU_COUNT)]
    CPUS2=   [list() for i in range(vars.CPU_COUNT)]
    CPUS3=   [list() for i in range(vars.CPU_COUNT)]
    p = 0
    CPU_LOADS1 = [list() for i in range(vars.CPU_COUNT)]
    CPU_LOADS2 = [list() for i in range(vars.CPU_COUNT)]
    CPU_LOADS3 = [list() for i in range(vars.CPU_COUNT)]

    while main_counter < vars.STEPS_COUNT:
        new_procs = list()
        for i in range(randint(0, vars.MAX_PROC_GEN)):
            random_cpu_index = randint(0, len(CPUS1)-1)
            random_proc = randint(1, vars.MAX_SINGLE_LOAD)
            new_procs.append((random_proc, random_cpu_index))

        #test_sample = strat1(CPUS.copy(), new_procs)
        strat1(CPUS1, new_procs)
        strat2(CPUS2, new_procs)
        strat3(CPUS3, new_procs)

        for i,v in enumerate(CPUS1): CPU_LOADS1[i].append(sum(v))
        for i,v in enumerate(CPUS2): CPU_LOADS2[i].append(sum(v))
        for i,v in enumerate(CPUS3): CPU_LOADS3[i].append(sum(v))

        #wylaczenie odpowiednich procesow
        for i in range(randint(0, 4)):
            try:
                random_row = randint(0, len(CPUS1)-1)
                random_col = randint(0, len(CPUS1[random_row])-1)
                del CPUS1[random_row][random_col]

                del CPUS2[random_row][random_col]

                del CPUS3[random_row][random_col]
            except (IndexError, ValueError) as e: continue

        main_counter += 1
    avg_loads1 = [round(fmean(i),2) for i in CPU_LOADS1]
    avg_loads2 = [round(fmean(i),2) for i in CPU_LOADS2]
    avg_loads3 = [round(fmean(i),2) for i in CPU_LOADS3]
    deviation1 = round(fmean([round(mean_absolute_deviation(i, fmean(avg_loads1)), 2) for i in CPU_LOADS1]), 2)
    deviation2 = round(fmean([round(mean_absolute_deviation(i, fmean(avg_loads2)), 2) for i in CPU_LOADS2]), 2)
    deviation3 = round(fmean([round(mean_absolute_deviation(i, fmean(avg_loads3)), 2) for i in CPU_LOADS3]), 2)
    return [[avg_loads1, avg_loads2, avg_loads3], [deviation1, deviation2, deviation3]]
    

def strat1(cpu_list, new_procs):
    global migrations, requests

    for proc in new_procs:
        current_proc = proc[0]
        current_cpu_index = proc[1]

        ok = False
        for i in range(vars.STRAT1_MAX_FAILURES):
            requests[0] += 1
            random_cpu_index = choice([i for i in range(0, len(cpu_list)) if i != current_cpu_index])
            if sum(cpu_list[random_cpu_index]) < vars.STRAT1_SEND_THRESHOLD:
                cpu_list[random_cpu_index].append(current_proc)
                ok=True
                migrations[0] += 1
                break

        if not ok: cpu_list[current_cpu_index].append(current_proc)

def strat2(cpu_list, new_procs):
    global migrations, requests

    for proc in new_procs:
        current_proc = proc[0]
        current_cpu_index = proc[1]

        if sum(cpu_list[current_cpu_index]) <= vars.STRAT2_SEND_THRESHOLD:
            cpu_list[current_cpu_index].append(current_proc)
            continue
        
        cpus_to_choose = [i for i, v in enumerate(cpu_list) if sum(v) <= vars.STRAT2_SEND_THRESHOLD]
        try: 
            random_cpu_index = choice(cpus_to_choose)
            migrations[1] += 1
            requests[1] += 1
        except IndexError: random_cpu_index = current_cpu_index

        cpu_list[random_cpu_index].append(current_proc)

    return

def strat3(cpu_list, new_procs):
    global migrations, requests

    for proc in new_procs:
        current_proc = proc[0]
        current_cpu_index = proc[1]

        if sum(cpu_list[current_cpu_index]) <= vars.STRAT3_MIN_THRESHOLD:
            cpus_to_choose = [i for i, v in enumerate(cpu_list) if sum(v) > vars.STRAT3_SEND_THRESHOLD]
            try: 
                random_cpu_index = choice(cpus_to_choose)
                bound = int(vars.STRAT3_TRANSFER_PORTION * len(cpu_list[random_cpu_index]))
                to_transfer = cpu_list[random_cpu_index][:bound]
                cpu_list[current_cpu_index] += to_transfer
                del cpu_list[random_cpu_index][:bound]
                migrations[2] += len(to_transfer)
                requests[2] += 1

            except IndexError: pass

        if sum(cpu_list[current_cpu_index]) <= vars.STRAT3_SEND_THRESHOLD:
            cpu_list[current_cpu_index].append(current_proc)
            continue
        
        cpus_to_choose = [i for i, v in enumerate(cpu_list) if sum(v) <= vars.STRAT3_SEND_THRESHOLD]
        try: 
            random_cpu_index = choice(cpus_to_choose)
            migrations[2] += 1
            requests[2] += 1
        except IndexError: random_cpu_index = current_cpu_index

        cpu_list[random_cpu_index].append(current_proc)

    return
    
