public class request{
    Integer cylinder,
        deadline;

    public request(int c, int d){
        this.cylinder = c;
        this.deadline = d;
    }

    public Integer getCylinder(){return this.cylinder;}
    public Integer getDeadline(){return this.deadline;}
    public String toString(){return String.format("%d : %d", cylinder, deadline);}
}
