import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class disk{
    int BOUND = 10000,
        INF = 1000000000;
    int START;

    ArrayList<request> N = new ArrayList<>();

    public disk(String inputFile) throws FileNotFoundException{
        this.START = new Random().nextInt(BOUND);
        read(inputFile);
    }

    public void read(String filename) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(filename));
        String temp = "";
        int c, d;
        while(scanner.hasNext()){
            temp = scanner.nextLine();
            c = Integer.valueOf(temp.split("[\\s]+")[0]);
            d = Integer.valueOf(temp.split("[\\s]+")[1]);
            N.add(new request(c, d));
        }

        System.out.printf("Zczytano %d zgloszen\n", N.size());

        scanner.close();
        return;
    }

    public int fcfs(){
        Integer last = START;
        int res = 0;
        for(request i: N){
            res += Math.abs(i.getCylinder() - last);
            last = i.getCylinder();
        }
        return res;
    }

    public int sstf(){
        int res = 0;
        ArrayList<request> arr = new ArrayList<>(N);
        Integer last = START, curr;

        for(int i = 0; i < arr.size(); ++i){
            curr = sstf_findNext(arr, last);
            res += Math.abs(last-curr);
            last = curr;
        }
        
        return res;
    }

    private Integer sstf_findNext(ArrayList<request> arr, Integer ref){
        Integer mn = INF;
        int ind = 0;
        for(int i =0; i < arr.size(); ++i){
            if(Integer.valueOf(Math.abs(ref - arr.get(i).getCylinder())).compareTo(mn) <= 0){
                mn = Math.abs(ref-arr.get(i).getCylinder());
                ind = i;
            }
        }
        Integer res = arr.get(ind).getCylinder();
        arr.set(ind, new request(INF, 0));
        return res;

    }
    
    private int binsrch(ArrayList<request> arr, Integer tar){
        int l = 0, r = arr.size(), m = 0;

        int ans = 0;
        while(l < r){
            m = (int)((l+r)/2);
            l = arr.get(m).getCylinder().compareTo(tar) <=0 ? m+1 : l;
            r = arr.get(m).getCylinder().compareTo(tar) >0 ? m : r;
            ++ans;
        }
        return m+1;
    }

    public int scan(){
        int res = 0, 
            last = START,
            it,
            step = 1;
        ArrayList<request> arr = new ArrayList<>(N);
        arr.sort((a,b) -> a.getCylinder().compareTo(b.getCylinder()));


        for(it = binsrch(arr,START)%arr.size(); it >-1; it += step){

            if(!arr.get(it).getCylinder().equals(0)){
                res += Math.abs(last - arr.get(it).getCylinder());
                last = arr.get(it).getCylinder();
            }

            //System.out.printf("%d, %d, %d\n", res, arr.get(it).getCylinder(), it);
            arr.set(it, new request(0, 0));
            if(it == arr.size()-1){
                res += BOUND - last;
                step = -1;
                last = BOUND;
            }
        }

        return res;

    }

    public int cscan(){
        int res = 0, last = START;
        ArrayList<request> arr = new ArrayList<>(N);
        arr.sort((a,b) -> a.getCylinder().compareTo(b.getCylinder()));

        int it = binsrch(arr, START)%arr.size();

        while(!arr.get(it).getCylinder().equals(INF)){
            res += Math.abs(last - arr.get(it).getCylinder());
            //System.out.printf("%d, %d\n", res, arr.get(it));
            last = arr.get(it).getCylinder();
            arr.set(it, new request(INF, 0));
            if(it == arr.size()-1){
                res+= BOUND - last + 1;
                last = 0;

            }
            it = (++it)%arr.size();
        }

        return res;
    }

    private int edfCompare(request a, request b){
        return a.getDeadline().compareTo(b.getDeadline());
    }

    public int edf(){
        int res = 0,
            last = START;

        ArrayList<request> arr = new ArrayList<>(N);
        arr.sort((a,b) -> edfCompare(a, b));

        for(request i: arr){
            res += Math.abs(last - i.getCylinder());
            last= i.getCylinder();
        }

        return res;
    }

    public int fdscan(){
        int res = 0,
            step,
            counter = 0;
        Integer temp,
                last = START;
        
        //<ddl, index>
        request mn = new request(0, INF);

        ArrayList<request> arr = new ArrayList<>(N);
        arr.sort((a,b) -> a.getCylinder().compareTo(b.getCylinder()));
        
        //find min
        for(int i = 0; i < N.size(); ++i){
            temp = N.get(i).getDeadline();
            if(mn.getDeadline().compareTo(temp) > 0) mn = N.get(i);
        }

        //okreslamy kierunek skanowania
        step = START - mn.getCylinder() >= 0 ? -1 : 1;

        //znajdujemy poczatkowy index skanowania
        for(int it = binsrch(arr, START) + (step ==1 ? 0 : -1); counter < arr.size(); it += step){
            if(!arr.get(it).getCylinder().equals(0)){
                res += Math.abs(last - arr.get(it).getCylinder());
                last = arr.get(it).getCylinder();
                ++counter;
            }

            if(counter == arr.size()) break;
            arr.set(it, new request(0, 0));
            if(it == arr.size()-1 || it == 0){
                int limit = it == 0 ? 0 : BOUND;
                res += Math.abs(limit - last);
                step *= -1;
                last = limit;
            }
        }

        return res;
    }

    public static void displayAll() throws FileNotFoundException{
        String[] filenames = {"in200", "in500"};

        for(String name: filenames){
            disk dsk = new disk(name);

            System.out.printf("%7s: %11d\n", "FCFS", dsk.fcfs());
            System.out.printf("%7s: %11d\n", "SSTF", dsk.sstf());
            System.out.printf("%7s: %11d\n", "SCAN", dsk.scan());
            System.out.printf("%7s: %11d\n", "CSCAN", dsk.cscan());
            System.out.printf("%7s: %11d\n", "EDF", dsk.edf());
            System.out.printf("%7s: %11d\n", "FDSCAN", dsk.fdscan());

            System.out.println("");
        }

        return;
    }
}
