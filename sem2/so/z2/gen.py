from random import randint

def generate(MAX_C, MAX_D, NUMBER_OF_REQUESTS, filename):
    tab = { randint(1, MAX_C) for i in range(NUMBER_OF_REQUESTS) }

    with open(filename, 'w') as file:
        for v in tab:
            file.write(f'{v}\t{randint(1, MAX_D)}\n')

generate(
        MAX_C = 10**4 -10,
        MAX_D = 10**3,
        NUMBER_OF_REQUESTS = 200,
        filename = 'in200',
)

generate(
        MAX_C = 10**4 -10,
        NUMBER_OF_REQUESTS = 5,
        MAX_D = 10**3,
        filename = 'in500',
)
