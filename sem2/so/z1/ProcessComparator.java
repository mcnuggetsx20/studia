import java.util.Comparator;
import java.util.PriorityQueue;

public class ProcessComparator implements Comparator<Process>{
    @Override
    public int compare(Process p1, Process p2){
        int end1 = Math.max(p1.getCallTime(), p1.getN()) + p1.getPhase(),
            end2 = Math.max(p2.getCallTime(), p2.getN()) + p2.getPhase();

        //System.out.printf("%d, %d\n", end1, end2);
        if(end1 > end2){
            return 1;
        }
        if(end1 < end2){
            return -1;
        }
        return 0;
    }
}


