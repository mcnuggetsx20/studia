public class Process{
    int ID,
        PHASE, 
        CALL_TIME,
        N = 0,
        PHASE2,
        first;

    public Process(int ID, int CALL_TIME, int PHASE){
        this.ID = ID;
        this.PHASE = PHASE;
        this.CALL_TIME = CALL_TIME;
        this.PHASE2 = PHASE;
    }

    public int getCallTime(){return CALL_TIME;}
    public int getPhase(){return PHASE;}
    public int getPhase2(){return PHASE2;}
    public int getN(){return N;}
    public int getId(){return ID;}
    public int getFirst(){return first;}

    public void addTime(int n){N = n;}
    public void minusPhase(){PHASE2--;}

    @Override
    public String toString(){
        return String.format("ID: %3d; CALL_TIME: %3d; PHASE: %3d", ID, CALL_TIME, PHASE);
    }

    public void reset(){PHASE2 = PHASE;}
    public void minusQuant(int q){PHASE2 -= q;}
    public void setFirst(int n){first = Math.min(n, first);}

}
