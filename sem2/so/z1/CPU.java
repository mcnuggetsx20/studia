import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Random;

public class CPU{
    static Process[] arr = new Process[10];

    static int tmr = 0,
               cnt = 0,
               done = 0;

    public static <T> void printQ(PriorityQueue<T> queue){
        Iterator iter = queue.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

    public static void init(){
        arr[0] = new Process(1, 0, 8);
        arr[1] = new Process(2, 1, 4);
        arr[2] = new Process(3, 2, 9);
        arr[3] = new Process(4, 3, 5);
        arr[4] = new Process(5, 7, 2);
        arr[5] = new Process(6, 9, 4);
        arr[6] = new Process(7, 0, 7);
        arr[7] = new Process(8, 2, 4);
        arr[8] = new Process(9, 4, 1);
        arr[9] = new Process(10, 5, 4);
        
        return;
    }

    public static Double FSCF(){
        int nextAvailable =0;
        Double await = 0.0;

        for(Process i : arr){
            //System.out.println(nextAvailable - i.getCallTime());
            await += nextAvailable - i.getCallTime();
            nextAvailable += i.getPhase();
        }

        return await/arr.length;
    }

    public static Double SJF(){
        Comparator<Process> comp = new ProcessComparator();
        PriorityQueue<Process> queue = new PriorityQueue<Process>(comp);

        int timer = 0;
        int counter =0;
        int total_time = arr[0].getPhase();
        int await = 0;
        int debug = 20;

        queue.add(arr[0]);

        while(!queue.isEmpty()){
            if(timer == Math.max(queue.peek().getCallTime(), queue.peek().getN()) +queue.peek().getPhase()){
                queue.poll();
                //System.out.println(queue.poll());
                if(queue.peek()==null){break;}
                total_time += queue.peek().getPhase();
                await += timer - queue.peek().getCallTime();
            }

            try{
                if(timer == arr[counter+1].getCallTime()){
                    arr[counter+1].addTime(total_time);
                    queue.add(arr[counter+1]);
                    ++counter;
                }
            } catch(ArrayIndexOutOfBoundsException e){;}
            //System.out.println(queue);
            ++timer;
            if(timer == debug){break;}
        }
        return (double)await/arr.length;
    }

    public static Double SRTF(){
        ArrayList<Process> tab = new ArrayList<>();

        Comparator<Process> comp = new Comparator<Process>(){
            @Override
            public int compare(Process p1, Process p2){
                int ph1 = p1.getPhase2();
                int ph2 = p2.getPhase2();
                if(ph1 > ph2){return 1;}
                if(ph1 < ph2){return -1;}
                return 0;
            }
        };

        int counter = 1,
            timer = 0,
            await = 0;

        tab.add(arr[0]);

        while(!tab.isEmpty()){
            if(tab.get(0).getPhase2() <= 0){
                await += - tab.get(0).getCallTime() + timer - tab.get(0).getPhase();
                //System.out.printf("%d %d %d\n", tab.get(0).getCallTime(), timer, tab.get(0).getPhase());
                tab.removeFirst();
            }
            if(tab.isEmpty()){break;}
            if(timer == arr[counter].getCallTime()){
                tab.add(arr[counter]);
                Collections.sort(tab, comp);
                counter = Math.min(counter +1, arr.length-1);
            }
            //System.out.println(tab.get(0).getPhase2());
            //System.out.println(tab);
            tab.get(0).minusPhase();
            ++timer;
        }

        return (double)await/arr.length;
    }

    public static Double RR(int quant){
        int counter = 1,
            timer = 0,
            await = 0;

        ArrayList<Process> tab = new ArrayList<>();

        for(int i =0; i < arr.length; ++i){
            arr[i].reset();
            tab.add(arr[i]);
        }

        while(!tab.isEmpty()){
            //System.out.println(tab);
            tab.get(0).minusQuant(quant);
            tab.get(0).setFirst(timer);
            if(tab.get(0).getPhase2() <=0){
                await += tab.get(0).getPhase2() + timer - tab.get(0).getFirst() - tab.get(0).getPhase();
                //System.out.println(tab.get(0).getPhase2() + timer - tab.get(0).getFirst() - tab.get(0).getPhase());
            }
            Process temp = tab.get(0);
            tab.removeFirst();
            if(temp.getPhase2() > 0){tab.add(temp);}

            timer += quant;
        }

        return (double)await/arr.length;
    }

    public static void main(String[] args){
        init();
        System.out.println("Dane wejsciowe:");
        for(Process i : arr){
            System.out.println(i);
        }
        System.out.println();

        System.out.printf("algorytm FSCF: %6.3f\n", FSCF());
        System.out.printf("algorytm SJF: %6.3f\n", SJF());
        System.out.printf("algorytm SRTF: %6.3f\n",SRTF());
        for(int i = 1; i < 30; ++i){
            System.out.printf("algorytm rotacyjny (%3d): %6.3f\n", i, RR(i));
        }

        return;
    }
}
