from vars import HIGH_BOUND, INTERVAL, LOW_BOUND, NUMBER_OF_PROCESSES, PAGES, NUMBER_OF_FRAMES, NUMBER_OF_REQUESTS
from random import Random, randint, choice, sample, randrange
from collections import defaultdict as dd
from collections import deque

processes = list()
prop = dict()
page_faults = dd(lambda: 0)
WSS = dd(lambda: set())

def genProcess():
    global processes, prop
    processes = list()
    current = 0
    for i in range(NUMBER_OF_PROCESSES):
        rg = randint(1, len(PAGES) // NUMBER_OF_PROCESSES)
        processes.append(PAGES[current:current+rg+1])
        current += rg+1
    
    #print(*processes, sep='\n')
    res = list()

    #generowanie kolejki odwolan
    for i in range(NUMBER_OF_REQUESTS):
        proc = choice(processes)
        call_size = randint(1, len(proc))
        call = sample(proc, call_size)
        res += call
        prop |= {j : i for j in call}

    return res

def lru(call_arr, frames):
    global page_faults, WSS
    call_times = dd(lambda: -1)
    #frames = []
    replacements = 0
    mx = 0

    for i, v in enumerate(call_arr):
        call_times[v] = i
        WSS[prop[v]].add(v)
        if v in frames: continue

        if len(frames) == NUMBER_OF_FRAMES:
            page_faults[prop[v]] += 1
            replacements +=1
            mx = max(frames, key= lambda x: call_times[x])
            frame_index = frames.index(mx)
            frames[frame_index] = v
            continue
        frames.append(v)

    return replacements

def equal_alloc(call_arr):
    global processes
    frames = []
    N = NUMBER_OF_FRAMES // NUMBER_OF_PROCESSES
    for proc in processes:
        temp = sample(proc, min(N, len(proc)))
        frames += temp

    return lru(call_arr, frames)

def prop_alloc(call_arr):
    global processes
    frames =[]
    for proc in processes:
        N = len(proc) // len(PAGES) * NUMBER_OF_FRAMES
        temp = sample(proc, min(N, len(proc)))
        frames += temp
    return lru(call_arr, frames)

def page_fault_freq(call_arr):
    global processes, prop,page_faults
    frames =[]
    for proc in processes:
        N = len(proc) // len(PAGES) * NUMBER_OF_FRAMES
        temp = sample(proc, min(N, len(proc)))
        frames += temp

    ans = 0
    for ind, call in enumerate(call_arr):
        if ind%INTERVAL: continue
        page_faults = dd(lambda: 0)
        ans += lru(call_arr[ind:ind+INTERVAL], frames)
        in_need = [i for i,j in page_faults.items() if j/INTERVAL > HIGH_BOUND ]
        donors = [i for i,j in page_faults.items() if j/INTERVAL < LOW_BOUND]

        if not in_need: continue
        for i in donors:
            if not in_need: break
            receiver = in_need.pop(0)
            to_replace = None
            for j in frames:
                if prop[j] == i: to_replace = j; break

            to_put = None
            for j in processes[receiver]:
                if j not in frames: to_put = j; break
            frames[ frames.index(to_replace) ] = to_put

    return ans

def zone_alloc(call_arr):
    global processes, prop,page_faults, INTERVAL, WSS
    INTERVAL += -2
    frames = []

    #zaczynamy od przydzialu proporcjonalnego
    for proc in processes:
        N = len(proc) // len(PAGES) * NUMBER_OF_FRAMES
        temp = sample(proc, min(N, len(proc)))
        frames += temp

    ans = 0
    for ind, call in enumerate(call_arr):
        if ind %INTERVAL: continue
        WSS = dd(lambda: set())
        ans += lru(call_arr[ind:ind+INTERVAL], frames)
        summed_wss = 0
        for i, j in WSS.items(): summed_wss += len(j)
        if summed_wss <= NUMBER_OF_FRAMES: continue
        to_deallocate = sorted(WSS, key=lambda x: len(WSS[x]), reverse=False)[-1] #wstrzymujemy proces o najwiekszym WSS
        for i, v in enumerate(frames):
            if prop[v] != to_deallocate: continue
            random_proc = to_deallocate
            while random_proc == to_deallocate: random_proc = choice(list(WSS.keys())) #wybieramy losowy proces, tylko nie ten ktory chcemy zdealokowac
            random_page = choice(processes[random_proc])
            while random_page in frames: random_page = choice(processes[random_proc]) #zabezpieczenie zeby jakims cudem nie dac tej samej strony dwa razy; potencjalne zapetlenie programu
            frames[i] = random_page

    return ans

