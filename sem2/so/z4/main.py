import wrapper

tab = wrapper.genProcess()
#print(len(tab))
functions = [wrapper.equal_alloc,
             wrapper.prop_alloc,
             wrapper.page_fault_freq,
             wrapper.zone_alloc,
]
for func in functions: print(f'{func.__name__:20}{func(tab):5}')
