PAGES = [i for i in range(1, 1000 + 1)]
NUMBER_OF_PROCESSES = 10
NUMBER_OF_FRAMES = 51
NUMBER_OF_REQUESTS = 100
INTERVAL=50
LOW_BOUND = 0.3
HIGH_BOUND = 0.7
