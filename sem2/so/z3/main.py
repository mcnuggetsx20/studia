import wrapper

tab = wrapper.genProcess()
#print(len(tab))
functions = [wrapper.fifo,
             wrapper.opt,
             wrapper.lru,
             wrapper.sca,
             wrapper.rand,
]
for func in functions: print(f'{func.__name__:5}{func(tab):5}')
