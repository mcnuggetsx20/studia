from vars import NUMBER_OF_PROCESSES, PAGES, NUMBER_OF_FRAMES, NUMBER_OF_REQUESTS
from random import randint, choice, sample, randrange
from collections import defaultdict as dd
from collections import deque

def genProcess():
    processes = list()
    current = 0
    for i in range(NUMBER_OF_PROCESSES):
        rg = randint(1, len(PAGES) // NUMBER_OF_PROCESSES)
        processes.append(PAGES[current:current+rg+1])
        current += rg+1
    
    #print(*processes, sep='\n')
    res = list()

    #generowanie kolejki odwolan
    for i in range(NUMBER_OF_REQUESTS):
        proc = choice(processes)
        call_size = randint(1, len(proc))
        print(proc, call_size)
        call = sample(proc, call_size)
        print(call)
        print()
        res += call

    return res

def fifo(call_arr):
    replacements = 0
    #call_arr= [1,2,3,4,1,2,5,1,2]
    frames = []

    for i in call_arr:
        if i in frames: continue

        if len(frames) == NUMBER_OF_FRAMES: 
            frames.pop(0)
            replacements += 1
        frames.append(i)

    return replacements

def opt(call_arr):

    #call_arr= [1,2,3,4,3,5,1,2,5,4]
    frames = []

    replacements = 0
    arr = dd(lambda: [10**9])
    visited = dd(lambda: False)
    mx = 0

    for i, v in enumerate(call_arr[::-1]):
        call_time = len(call_arr) - i - 1
        arr[v].append(call_time)

    for i in call_arr:
        arr[i].pop(-1)

        if i in frames: continue

        if len(frames) == NUMBER_OF_FRAMES: 
            replacements += 1
            mx = max(frames, key=lambda x: arr[x][-1])
            frame_index = frames.index(mx)
            frames[frame_index] = i
            continue
        
        frames.append(i)

    return replacements

def lru(call_arr):
    call_times = dd(lambda: -1)
    frames = []
    replacements = 0
    mx = 0

    for i, v in enumerate(call_arr):
        call_times[v] = i
        if v in frames: continue

        if len(frames) == NUMBER_OF_FRAMES:
            replacements +=1
            mx = max(frames, key= lambda x: call_times[x])
            frame_index = frames.index(mx)
            frames[frame_index] = v
            continue
        frames.append(v)

    return replacements

def sca(call_arr):
    frames = []
    replacements = 0

    que = deque()
    bits = dd(lambda: 1)
    for i in call_arr:
        if i in frames: 
            bits[i] = 1
            continue

        if len(frames) == NUMBER_OF_FRAMES:
            replacements+=1
            while bits[que[0]]:
                temp = que.popleft()
                que.append(temp)
                bits[temp] = 0
            frame_index = frames.index(que[0])
            frames[frame_index] = i
            que.popleft()
            que.append(i)
            continue

        frames.append(i)
        que.append(i)
        bits[i] = 1

    return replacements

def rand(call_arr):
    frames = []
    replacements = 0

    for i in call_arr:
        if i in frames: continue

        if len(frames) == NUMBER_OF_FRAMES:
            replacements+=1
            to_remove = randrange(len(frames))
            frames[to_remove] = i
            continue
        frames.append(i)

    return replacements
